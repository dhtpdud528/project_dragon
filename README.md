# 드래곤 브레스<br>DragonBreath

# 상점 페이지
[<img src="https://img.shields.io/badge/Google Play-414141?style=for-the-badge&logo=Google Play&logoColor=white">](https://play.google.com/store/apps/details?id=com.vg.dragonbreath&hl=ko)

# 개발 정보

### 개발 기간
2019/ 11/ 01 ~ 2020/ 09/ 03 출시

### 개발 툴, 언어
<img src="https://img.shields.io/badge/Unity-000000?style=for-the-badge&logo=Unity&logoColor=white">
<img src="https://img.shields.io/badge/C Sharp-239120?style=for-the-badge&logo=C Sharp&logoColor=white">

### 타겟 플랫폼
<img src="https://img.shields.io/badge/Android-3DDC84?style=for-the-badge&logo=Android&logoColor=white">

### 개발 인원
- 기획 1명
- 그래픽, 레벨 디자인 1명
- **클라이언트 개발 1명 (본인)**

---

# 게임 소개
### 장르
2D, 캐주얼 원터치 디펜스
### 스토리
- **플레이어** – 드래곤
- **게임 목표** – 드래곤의 알을 노리고 진격해오는 몬스터 무리들을 전부 저지 하는 것

# 게임 특징
- Spine을 이용한 애니메이션
- 원터치로 즐기는 캐주얼 디펜스게임
- 다양한 몬스터
  - 몬스터 마다 각각의 고유한 특징이 있음
  - 플레이에 도움을 주는 도우미 몬스터도 존재함
- 다양한 속성공격
  - 아이템을 이용해 속성공격이 가능
  - 불, 얼음, 전기, 바람 4종류의 속성 존재
- **샌드박스 모드**
  - 플레이어가 직접 레벨을 디자인해 볼 수 있음

# 플레이 영상
[![](http://img.youtube.com/vi/axj7NUjGFh0/0.jpg)](http://www.youtube.com/watch?v=axj7NUjGFh0 "")

<!--
# 컨텐츠 설명
## 적
## 동료
## 플레이어
-->
