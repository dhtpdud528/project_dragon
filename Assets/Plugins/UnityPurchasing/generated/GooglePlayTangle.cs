#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("5hs6XMZ0wNJTjHo5O5ofSkIO9pfETKA9h3+tSOeRIvlAfdSJf/AYp9V8jac6KsYTWlfoKJOuZSkrFns7fUtKwuF9mEqTXnLBqd5oLZ96b3BDjlKjd7E0MWrwLyL2AyTeP3LdRt2Rk7Za5v1w9b7d7Ra/gV9Yo4N6KJvPgwRX5OnqPXTUD6HT1yw+eBgLL3JZ3lc5EGi8RwteA/31b+x35lB9+/ns3aAcXgEzV2i4HNwS4/7x/XgmD+fqsoUKP4S7izP1XClh8INd3tDf713e1d1d3t7fW857cXCcO+9d3v3v0tnW9VmXWSjS3t7e2t/cmfq26zkZQ95eKHU/Rdr80Nyku2fjjqa3LxFDm2Ipj6GIZejbzQ7bo+HuXPzjwYLfft3c3t/e");
        private static int[] order = new int[] { 8,11,3,3,4,7,10,9,12,10,11,12,13,13,14 };
        private static int key = 223;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
