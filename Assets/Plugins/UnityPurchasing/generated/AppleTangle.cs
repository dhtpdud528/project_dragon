#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("qJ6hqK37s72vr1Gqq56tr69RnrOev6it+6qkvaTv3t7Cy47nwM2Any66hX7H6TrYp1BaxSOA7ghZ6ePR2sbB3Mfa15+4nrqorfuqrb2j794ZtRM97Iq8hGmhsxjjMvDNZuUuuR+e9kL0qpwixh0hs3DL3VHJ8MsShCjmKFmjr6+rq66ezJ+lnqeorfuBni9tqKaFqK+rq6msrJ4vGLQvHckhphqOWWUCgo7B3hiRr54iGe1h1J4sr9ieoKit+7Ohr69RqqqtrK87MNSiCukl9Xq4mZ1laqHjYLrHf7E/dbDp/kWrQ/DXKoNFmAz54vtC3M/N2sfNy47d2s/ay8PLwNrdgJ6bnJ+anp2Y9LmjnZuenJ6XnJ+antrHyMfNz9rLjszXjs/A147ez9za2dmAz97ewsuAzcHDgc/e3sLLzc+Oz8DKjs3L3NrHyMfNz9rHwcCO3t7Cy47ty9zax8jHzc/ax8HAju/bo6inhCjmKFmjr6+rq66tLK+vrvLXjs/d3dvDy92Oz83Ny97az8DNyyyvrqinhCjmKFnNyquvni9cnoSo/MvCx8/AzcuOwcCO2sbH3Y7Ny9zewsuO/MHB2o7t756wuaOemJ6anIpMRX8Z3nGh60+JZF/D1kNJG7m5ypuNu+W797MdOllYMjBh/hRv9v6xKy0rtTeT6ZlcBzXuIIJ6Hz68dhBa3TVAfMqhZdfhmnYMkFfWUcVmuJ66qK37qq29o+/e3sLLjvzBwdqeLKoVniytDQ6trK+srK+snqOop5g34oPWGUMiNXJd2TVc2HzZnuFvnZj0nsyfpZ6nqK37qqi9rPv9n73r0LHixf447ydq2sylvi3vKZ0kL8fIx83P2sfBwI7v29rGwdzH2tefbs2d2VmUqYL4RXShj6B0FN234RsGctCMm2SLe3eheMV6DIqNv1kPAoDuCFnp49Gm8J6xqK37s42qtp64k4jJjiSdxFmjLGFwRQ2BV/3E9crndtgxnbrLD9k6Z4Osra+urw0sr4KOzcvc2sfIx83P2suO3sHCx83XqK37s6CquKq6hX7H6TrYp1BaxSPMwsuO3drPwMrP3MqO2svcw92OzwUN3zzp/ftvAYHvHVZVTd5jSA3iZ7fcW/Oge9HxNVyLrRT7IePzo1+Inoqorfuqpb2z797ewsuO7cvc2qqovaz7/Z+9nr+orfuqpL2k797ewMqOzcHAysfax8HA3Y7ByI7b3cuO7e+eLK+MnqOop4Qo5ihZo6+vr3eY0W8p+3cJNxec7FV2e98w0A/8JbcncFflwlupBYyerEa2kFb+p32pQtOXLSX9jn2Wah8RNOGkxVGFUsLLjufAzYCfiJ6KqK37qqW9s+/eq66tLK+hrp4sr6SsLK+vrko/B6ch3S/OaLX1p4E8HFbq5l7OljC7W6bwniyvv6it+7OOqiyvpp4sr6qe9wmrp9K57vi/sNp9GSWNlekNe8GOwciO2sbLjtrGy8COz97ewsfNz9HvBjZXf2TIMorFv34NFUq1hG2xpoWor6urqayvuLDG2tre3ZSBgdkblANaoaCuPKUfj7iA2nuSo3XMuKEzk12F54a0ZlBgGxegd/CyeGWT/gQke3RKUn6nqZke29uP");
        private static int[] order = new int[] { 29,9,32,5,59,26,9,37,21,28,18,15,46,37,39,36,45,59,47,40,34,38,28,53,34,49,50,46,29,48,38,49,36,57,54,49,56,46,44,44,42,54,42,47,56,47,46,58,58,51,50,56,55,57,55,55,59,59,59,59,60 };
        private static int key = 174;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
