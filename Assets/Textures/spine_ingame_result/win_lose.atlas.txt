
win_lose.png
size: 2048,2048
format: RGBA8888
filter: Linear,Linear
repeat: none
result_egg_back
  rotate: false
  xy: 0, 1468
  size: 1330, 482
  orig: 1330, 482
  offset: 0, 0
  index: -1
result_lose_effect
  rotate: false
  xy: 0, 61
  size: 778, 422
  orig: 778, 627
  offset: 0, 71
  index: -1
result_lose_egg_a
  rotate: false
  xy: 1085, 845
  size: 308, 245
  orig: 339, 376
  offset: 31, 0
  index: -1
result_lose_egg_b
  rotate: false
  xy: 1425, 1047
  size: 309, 236
  orig: 309, 236
  offset: 0, 0
  index: -1
result_lose_egg_c
  rotate: false
  xy: 1085, 599
  size: 308, 245
  orig: 339, 376
  offset: 31, 0
  index: -1
result_lose_egg_d
  rotate: false
  xy: 1394, 810
  size: 309, 236
  orig: 309, 236
  offset: 0, 0
  index: -1
result_lose_egg_f
  rotate: false
  xy: 1394, 810
  size: 309, 236
  orig: 309, 236
  offset: 0, 0
  index: -1
result_lose_egg_e
  rotate: false
  xy: 1425, 1284
  size: 308, 245
  orig: 339, 376
  offset: 31, 0
  index: -1
result_lose_title
  rotate: false
  xy: 0, 1140
  size: 1084, 327
  orig: 1144, 327
  offset: 30, 0
  index: -1
result_score_title
  rotate: false
  xy: 0, 812
  size: 1084, 327
  orig: 1144, 327
  offset: 30, 0
  index: -1
result_win_effect
  rotate: false
  xy: 1331, 1530
  size: 620, 420
  orig: 646, 566
  offset: 13, 95
  index: -1
result_win_egg_a
  rotate: false
  xy: 1085, 1091
  size: 339, 376
  orig: 339, 376
  offset: 0, 0
  index: -1
result_win_egg_b
  rotate: false
  xy: 1085, 1091
  size: 339, 376
  orig: 339, 376
  offset: 0, 0
  index: -1
result_win_egg_d
  rotate: false
  xy: 1085, 1091
  size: 339, 376
  orig: 339, 376
  offset: 0, 0
  index: -1
result_win_egg_effect_a
  rotate: false
  xy: 779, 25
  size: 422, 458
  orig: 576, 614
  offset: 77, 78
  index: -1
result_win_egg_effect_b
  rotate: false
  xy: 779, 25
  size: 422, 458
  orig: 576, 614
  offset: 77, 78
  index: -1
result_win_egg_effect_c
  rotate: false
  xy: 779, 25
  size: 422, 458
  orig: 576, 614
  offset: 77, 78
  index: -1
result_win_title
  rotate: false
  xy: 0, 484
  size: 1084, 327
  orig: 1144, 327
  offset: 30, 0
  index: -1
