﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class DesignerSideMenuController : UIController
{
    public Image selectedObjIcon;
    public Text selectedObjName;

    public Toggle toggleLock;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public override void UpdateInfo(GameObject lastSelected)
    {
        toggleLock.isOn = MapDesigner.instance.lockObjs.Contains(lastSelected);
        selectedObjName.text = lastSelected.GetComponent<IName>().Name;
    }
}
