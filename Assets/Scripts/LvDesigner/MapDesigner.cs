﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;
using System.IO;

public class MapDesigner : MonoBehaviour
{
    public static MapDesigner instance;
    [HideInInspector]
    
    public Dropdown dropdownTheme;
    public Transform gamePlayComponentsTransform;
    public Image selectedOBJIcon;
    public InputField inputFieldSize;
    public DesignerSideMenuController uiSettingMenu;
    public DesignerSpawnerTapController infoSpawnerTap;
    public GameObject[] blockInfos;
    public GameObject waveEditWindow;
    public GameObject[] brushInfos;
    public float zoomOutMin = 1f;
    public float zoomOutMax = 20f;
    public Tilemap targetTile;
    public GameObject[] objsForPlaying;

    public GameObject adPopupSavemap;

    [SerializeField]
    public enum ToolState { Brush, Erase, Select , None}
    ToolState toolState;
    public ToolState CurrentToolState
    {
        get
        {
            return toolState;
        }
        set
        {
            toolState = value;
            if (toolState != ToolState.Erase)
            {
                eraser.gameObject.SetActive(false);
                eraser.enabled = false;
                eraser.transform.position = Vector2.zero;
            }
            switch (toolState)
            {
                case ToolState.Brush:
                    infoSpawnerTap.gameObject.SetActive(false);
                    for (int i =0; i < brushInfos.Length;i++)
                    {
                        brushInfos[i].SetActive(true);
                    }
                    for(int i =0; i < blockInfos.Length;i++)
                    {
                        blockInfos[i].SetActive(false);
                    }
                    uiSettingMenu.switchSettingMenu(true);
                    break;
                case ToolState.Erase:
                    eraser.enabled = false;
                    eraser.transform.position = Vector2.zero;
                    eraser.transform.position = new Vector2(Camera.main.transform.position.x, Camera.main.transform.position.y);
                    break;
                case ToolState.Select:
                    if(lastSelected != null && lastSelected.GetComponent<Block_Spawner>())
                        infoSpawnerTap.gameObject.SetActive(true);
                    for (int i = 0; i < brushInfos.Length; i++)
                    {
                        brushInfos[i].SetActive(false);
                    }
                    for (int i = 0; i < blockInfos.Length; i++)
                    {
                        blockInfos[i].SetActive(true);
                    }
                    break;
            }
        }
    }
    public void DoSelectTool(int i)
    {
        CurrentToolState = (ToolState)i;
        //GameManager.instance.gamePlayComponetsTrasform.gameObject.SetActive(false);
    }

    public BrushComponent currentBrush;
    public Collider2D eraser;

    public Transform selectCursor;
    public GameObject currentSelected;
    [SerializeField]
    GameObject lastSelected;
    public GameObject LastSelected
    {
        get
        {
            return lastSelected;
        }
        set
        {
            if (lastSelected != null)
            {
                if (value.Equals(lastSelected))
                    return;
                else if (lastSelected.GetComponent<Block_Spawner>())
                {
                    infoSpawnerTap.SaveSpawner();
                    infoSpawnerTap.WaveViewClear();
                }
            }
            lastSelected = value;
            inputFieldSize.text = lastSelected.transform.localScale.y.ToString();
            selectedOBJIcon.sprite = icons.Find((val) => lastSelected.GetComponent<IName>().Name.Equals(val.name));
            if (lastSelected.GetComponent<Block_Spawner>())
            {
                infoSpawnerTap.gameObject.SetActive(true);
                infoSpawnerTap.targetSpawner = lastSelected.GetComponent<Block_Spawner>();
                infoSpawnerTap.LoadWaves();
                infoSpawnerTap.UpdateWaveView();
            }
            else
                infoSpawnerTap.gameObject.SetActive(false);
            uiSettingMenu.UpdateInfo(lastSelected);
        }
    }
    Vector2 touchStart = Vector2.zero;

    public List<GameObject> lockObjs = new List<GameObject>();
    private bool isCanUseTool;
    public List<Sprite> icons = new List<Sprite>();

    

    // Start is called before the first frame update
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Debug.LogWarning("Scene에 2개 이상의 맵디자이너가 존재합니다!");
            Destroy(gameObject);
        }
        
    }
    void Start()
    {
        CurrentToolState = ToolState.Select;
    }

    public void SwitchTool()
    {
        gamePlayComponentsTransform.gameObject.SetActive(!gamePlayComponentsTransform.gameObject.activeSelf);
        var canvasGroup = GetComponent<CanvasGroup>();
        canvasGroup.blocksRaycasts = !canvasGroup.blocksRaycasts;
        canvasGroup.DOFade(canvasGroup.blocksRaycasts ? 1 : 0, 0.5f);
        Camera.main.transform.position = new Vector3(0, -2, -1);
        Camera.main.orthographicSize = zoomOutMax;
        GameManager.instance.breathController.curBreath = (float)GameManager.instance.breathController.profile.maxBreath;
    }

    // Update is called once per frame
    void Update()
    {
        if (!GetComponent<CanvasGroup>().blocksRaycasts || SimpleFileBrowser.FileBrowser.IsOpen) return;
        if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor)
        {
            zoom(Input.GetAxis("Mouse ScrollWheel") * 5);
            if (Input.GetMouseButtonDown(2))
                touchStart = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            else if (Input.GetMouseButton(2))
            {
                Vector3 direction = touchStart - (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
                Camera.main.transform.position += direction;
            }
        }

        if (Input.touchCount >= 2)
        {
            Touch touch1 = Input.GetTouch(0);
            Touch touch2 = Input.GetTouch(1);

            Vector2 touch1PrevPos = touch1.position - touch1.deltaPosition;
            Vector2 touch2PrevPos = touch2.position - touch2.deltaPosition;

            Vector2 touchPrevVector = touch1PrevPos - touch2PrevPos;
            Vector2 touchVector = touch1.position - touch2.position;

            float prevMagnitude = touchPrevVector.magnitude;
            float currentMagnitude = touchVector.magnitude;

            float difference = currentMagnitude - prevMagnitude;

            zoom(difference * 0.01f);

            var point1 = (Vector2)Camera.main.ScreenToWorldPoint(touch1.position);
            var point2 = (Vector2)Camera.main.ScreenToWorldPoint(touch2.position);

            //Vector3 viewPosition = (point1 + point2) / 2;
            //Camera.main.transform.position = viewPosition;
        }
        else if (!GameManager.IsPointerOverGameObject())
        {
            if (Input.GetMouseButtonDown(0))
                isCanUseTool = true;
            if (isCanUseTool)
                switch (CurrentToolState)
                {
                    case ToolState.Brush: DoDraw(); break;
                    case ToolState.Erase: DoErase(); break;
                    case ToolState.Select: DoSelect(); break;
                }
        }
        if (Input.GetMouseButtonUp(0))
            isCanUseTool = false;
        if (toolState == ToolState.Select && lastSelected != null)
            selectCursor.position = lastSelected.transform.position;
    }
    public void DoDraw()
    {
        if (currentBrush != null)
        {
            if (Input.GetMouseButton(0))
            {
                Vector2 touchPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                touchPoint = new Vector2(setPositionTile(touchPoint.x), setPositionTile(touchPoint.y));

                Collider2D collider;
                if (Input.GetMouseButtonDown(0) && currentBrush.target.GetComponent<IName>().Name.Contains("Egg") && GameManager.instance.eggs.Count >= 3)
                {
                    PopUpBoxManager.instance.PopConfirmBox(LocalizationManager.instance.GetMessage(0));
                    return;
                }
                if ((collider = Physics2D.OverlapCircle(touchPoint, .45f)) != null || currentBrush.target.GetComponent<IName>().Name.Contains("Egg") && GameManager.instance.eggs.Count >= 3)
                {//Presuming the object you are testing also has a collider 0 otherwise
                    return;
                }
                

                var objTransfom = Instantiate(currentBrush.target, touchPoint, Quaternion.identity).transform;
                objTransfom.SetParent(MapDesigner.instance.targetTile.transform);
            }
        }
    }
    public void setEraserScale(Slider slider)
    {
        setEraserScale(slider.value);
    }
    public void setEraserScale(float value)
    {
        eraser.transform.localScale = new Vector2(value, value);
    }
    public void DoErase()
    {
        if (Input.GetMouseButton(0))
        {
            eraser.enabled = true;
            Vector2 touchPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            eraser.transform.position = touchPoint;
        }
        else
            eraser.enabled = false;
    }
    public void DoSelect()
    {

        if (Input.GetMouseButtonDown(0))
        {
            touchStart = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Collider2D collider;
            if ((collider = Physics2D.OverlapCircle(touchStart, .1f)) != null)
            {
                if (collider.GetComponent<Object_Deadable>() || collider.GetComponent<Object_Static>())
                {
                    LastSelected = currentSelected = collider.gameObject;
                    SoundManager.instance.PlayOneShot_SFX(SoundManager.instance.cardOnSound);
                }
            }
            else
                touchStart = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }
        else if (Input.GetMouseButton(0))
        {
            if (currentSelected != null)
            {
                Vector2 touchPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                touchPoint = new Vector2(setPositionTile(touchPoint.x), setPositionTile(touchPoint.y));
                currentSelected.transform.position = touchPoint;
                if (!selectCursor.gameObject.activeSelf)
                    selectCursor.gameObject.SetActive(true);
            }
            else if (touchStart != Vector2.zero)
            {
                Vector3 direction = touchStart - (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
                Camera.main.transform.position += direction;
            }

        }
        else if (Input.GetMouseButtonUp(0))
        {
            if(currentSelected != null)
                SoundManager.instance.PlayOneShot_SFX(SoundManager.instance.cardOffSound);
            currentSelected = null;
        }
    }

    public void OnClickButtonTestPlay()
    {
        for (int i = 0; i < objsForPlaying.Length; i++)
        {
            objsForPlaying[i].SetActive(true);
        }
    }
    public float setPositionTile(float i)
    {
        return (Mathf.Ceil((i - 0.25f) * 2) / 2);
    }
    public void zoom(float increment)
    {
        Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize - increment, zoomOutMin, zoomOutMax);
    }
    public void SetLockObj(bool val)
    {
        if (val)
        {
            if (!lockObjs.Contains(lastSelected))
                lockObjs.Add(lastSelected);
        }
        else
            lockObjs.Remove(lastSelected);
    }
    public void DeleteOBJ()
    {
        if (LastSelected.GetComponent<IName>() == null || LastSelected.GetComponent<IName>().Name.Contains("RainbowEnd") && GameManager.instance.spawners.Count <= 1) return;
        if (!MapDesigner.instance.lockObjs.Contains(LastSelected.gameObject) && !LastSelected.GetComponent<IName>().Name.Contains("Egg"))
            Destroy(LastSelected.gameObject);
    }
    public void FocusLastOBj()
    {
        Camera.main.transform.position = new Vector3(LastSelected.transform.position.x, LastSelected.transform.position.y,-1);
    }
    public void setScale()
    {
        lastSelected.transform.localScale = new Vector3(float.Parse(inputFieldSize.text), float.Parse(inputFieldSize.text),1);
    }

}
