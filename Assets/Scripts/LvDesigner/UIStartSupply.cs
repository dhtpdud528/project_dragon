﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIStartSupply : MonoBehaviour
{
    public InputField inputFieldFireCount;
    public InputField inputFieldIceCount;
    public InputField inputFieldLightningCount;
    public InputField inputFieldWindCount;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnEnable()
    {
        inputFieldFireCount.text = GameManager.instance.startSupply.fireCount.ToString();
        inputFieldIceCount.text = GameManager.instance.startSupply.iceCount.ToString();
        inputFieldLightningCount.text = GameManager.instance.startSupply.lightningCount.ToString();
        inputFieldWindCount.text = GameManager.instance.startSupply.windCount.ToString();
    }
    public void ApplySupply()
    {
        GameManager.instance.startSupply.fireCount = int.Parse(inputFieldFireCount.text);
        GameManager.instance.startSupply.iceCount = int.Parse(inputFieldIceCount.text);
        GameManager.instance.startSupply.lightningCount = int.Parse(inputFieldLightningCount.text);
        GameManager.instance.startSupply.windCount = int.Parse(inputFieldWindCount.text);
    }
}
