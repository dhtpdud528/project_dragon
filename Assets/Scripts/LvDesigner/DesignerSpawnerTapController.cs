﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using DG.Tweening;

public class DesignerSpawnerTapController : MonoBehaviour
{
    public Block_Spawner targetSpawner;
    public enum SelectState { Select,Remove, Copy }
    public SelectState selectState;

    public GameObject wave;
    public Transform wavesTransform;

    [Header("- 선택한 웨이브 정보 UI")]
    public InputField m_name;
    public RegisterGratingController gratingRegister;
    public InputField m_countdown;
    public Transform m_monsterList;
    public GameObject monsterComponet;
    public InputField m_count;
    public InputField m_rate;

    public MonsterComponent dragedMonsterComponent;
    public WaveComponent selectedWaveComponent;
    public WaveComponent dragedWaveComponent;
    public bool isCanDrop;

    Block_Spawner.Wave selectedWave;
    public Block_Spawner.Wave SelectedWave
    {
        get
        {
            return selectedWave;
        }
        set
        {
            selectedWave = value;
            m_name.text = SelectedWave.name;
            m_countdown.text = SelectedWave.timeBetweenWaves.ToString();
            for(int i=0; i< m_monsterList.childCount;i++)
                Destroy(m_monsterList.GetChild(i).gameObject);
            for (int i =0; i < SelectedWave.spawnTargetsOrigin.Count;i++)
            {
                var component = Instantiate(monsterComponet, m_monsterList).GetComponent<MonsterComponent>();
                component.target = SelectedWave.spawnTargetsOrigin[i];
            }
            m_count.text = SelectedWave.spawnCount.ToString();
            m_rate.text = SelectedWave.rate.ToString();
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void LoadWaves()
    {
        if (targetSpawner != null)
        {
            WaveViewClear();
            for (int i = 0; i < targetSpawner.waves.Count; i++)
            {
                var placedWave = Instantiate(wave, wavesTransform).GetComponent<WaveComponent>();
                placedWave.wave = targetSpawner.waves[i];
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (dragedMonsterComponent != null)
        {
            if (Input.GetMouseButton(0))
                dragedMonsterComponent.transform.position = (Vector2)Camera.main.ScreenToWorldPoint(Input.mousePosition);
            else if (Input.GetMouseButtonUp(0) && !isCanDrop)
                Destroy(dragedMonsterComponent.gameObject);
        }
        if (dragedWaveComponent != null)
        {
            var point = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            if (Input.GetMouseButton(0))
                dragedWaveComponent.transform.position = new Vector2(point.x, point.y);
            else if (Input.GetMouseButtonUp(0) && !isCanDrop)
                dragedWaveComponent.transform.SetParent(wavesTransform);
        }
        //if(EventSystem.current.IsPointerOverGameObject())
        //{
        //    PointerEventData pointerEventData = new PointerEventData(EventSystem.current);
        //    pointerEventData.position = Input.mousePosition;

        //    List<RaycastResult> result = new List<RaycastResult>();
        //    EventSystem.current.RaycastAll(pointerEventData, result);
        //    for(int i=0; i< result.Count;i++)
        //    {
        //        Debug.Log(result[i].gameObject.name);
        //    }
        //}
        
    }

    public void SetSelectState(Dropdown dropdown)
    {
        selectState = (SelectState)dropdown.value;
        SaveSpawner();
    }
    public void IsOnDropPlace(bool val)
    {
        isCanDrop = val;
    }

    public void UpdateMonsterList()
    {
        UpdateSpacing(m_monsterList.parent.GetComponent<VerticalLayoutGroup>());
    }
    public void UpdateWaveView()
    {
        UpdateSpacing(wavesTransform.parent.GetComponent<VerticalLayoutGroup>());
    }
    public void UpdateSpacing(VerticalLayoutGroup verticalLayoutGroup)
    {
        StartCoroutine(UpdateSpacingCoroutine(verticalLayoutGroup));
    }
    IEnumerator UpdateSpacingCoroutine(VerticalLayoutGroup verticalLayoutGroup)
    {
        verticalLayoutGroup.spacing += 1;
        yield return new WaitForEndOfFrame();
        verticalLayoutGroup.spacing -= 1;
    }
    public void AddWave()
    {
        var defaultMonsterNames = new List<string>();
        defaultMonsterNames.Add("Forest_Slime");
        targetSpawner.waves.Add(new Block_Spawner.Wave("추가된 웨이브", null, MapDataManager.instance.FindMonsters(defaultMonsterNames), 1, 1, 5, null));
        LoadWaves();
    }
    public void CopyWave()
    {
        targetSpawner.waves.Add(selectedWave);
        LoadWaves();
    }
    public void CopySpawner()
    {
        MapDesigner.instance.LastSelected = Instantiate(targetSpawner.gameObject, MapDataManager.instance.target.transform);
        MapDesigner.instance.LastSelected.transform.DOMoveX(MapDesigner.instance.LastSelected.transform.position.x + 0.25f, 0.5f);
    }
    public void SaveWave()
    {
        List<GameObject> targets = new List<GameObject>();
        for (int i = 0; i < m_monsterList.childCount; i++)
            targets.Add(m_monsterList.GetChild(i).GetComponent<MonsterComponent>().target);

        selectedWave.name = m_name.text;

        //시작 이벤트
        selectedWaveComponent.textName.text = selectedWave.name;
        selectedWave.spawnTargetsOrigin = targets;
        selectedWave.spawnCount = int.Parse(m_count.text);
        selectedWave.rate = float.Parse(m_rate.text);
        selectedWave.timeBetweenWaves = float.Parse(m_countdown.text);
        //끝 이벤트
    }
    public void SaveSpawner()
    {
        targetSpawner.waves.Clear();
        for (int i =0;i< wavesTransform.childCount;i++)
            targetSpawner.waves.Add(wavesTransform.GetChild(i).GetComponent<WaveComponent>().wave);
    }
    public void WaveViewClear()
    {
        for(int i=0;i<wavesTransform.childCount;i++)
            Destroy(wavesTransform.GetChild(i).gameObject);
        UpdateWaveView();
    }
    public void EditGratingEvent(bool isStartEvent)
    {
        if (isStartEvent)
            gratingRegister.eventType = RegisterGratingController.EventType.Start;
        else
            gratingRegister.eventType = RegisterGratingController.EventType.End;

        gratingRegister.gameObject.SetActive(true);
    }

    public void OnDropMonsterList(Transform parent)
    {
        if (!isCanDrop) return;

        dragedMonsterComponent.transform.SetParent(parent);

        for (int i = 0; i < parent.childCount; i++)
            if (dragedMonsterComponent.transform.position.y > parent.GetChild(i).position.y)
            {
                dragedMonsterComponent.transform.SetSiblingIndex(parent.GetChild(i).GetSiblingIndex());
                break;
            }

        dragedMonsterComponent.icon.raycastTarget = true;
        dragedMonsterComponent = null;
        UpdateMonsterList();
    }
    public void OnDropWaveList(Transform parent)
    {
        if (!isCanDrop) return;

        dragedWaveComponent.transform.SetParent(parent);

        for (int i = 0; i < parent.childCount; i++)
            if (dragedWaveComponent.transform.position.y > parent.GetChild(i).position.y)
            {
                dragedWaveComponent.transform.SetSiblingIndex(parent.GetChild(i).GetSiblingIndex());
                break;
            }

        dragedWaveComponent.button.interactable = true;
        dragedWaveComponent = null;
        UpdateWaveView();
    }
}
