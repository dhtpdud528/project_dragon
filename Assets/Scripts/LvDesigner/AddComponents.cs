﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddComponents : MonoBehaviour
{
    public GameObject targetComponent;
    // Start is called before the first frame update
    void Start()
    {
        Instantiate(targetComponent, transform).GetComponent<MonsterComponent>().target = ObjectPactory.instance.breathController;
        for (int i = 0; i < ObjectPactory.instance.blocks.Length; i++)
            if (ObjectPactory.instance.blocks[i].GetComponent<Object_Deadable>())
                Instantiate(targetComponent, transform).GetComponent<MonsterComponent>().target = ObjectPactory.instance.blocks[i];
            else if (ObjectPactory.instance.blocks[i].GetComponent<Floor_Trap>() && ObjectPactory.instance.blocks[i].GetComponent<Floor_Trap>().Name.Equals("Trap_Fire"))
                Instantiate(targetComponent, transform).GetComponent<MonsterComponent>().target = ObjectPactory.instance.blocks[i];
        for (int i = 0; i < ObjectPactory.instance.monsters.Length; i++)
            Instantiate(targetComponent, transform).GetComponent<MonsterComponent>().target = ObjectPactory.instance.monsters[i];


    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
