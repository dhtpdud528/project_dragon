﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIDropBoxController : MonoBehaviour
{
    public Dropdown dropdown;
    public Image background;
    CanvasGroup dropdownList;
    public void ClickDropDown()
    {
        for (int i = 0; i < transform.childCount; i++)
            if (transform.GetChild(i).name.Equals("Dropdown List"))
                dropdownList = transform.GetChild(i).GetComponent<CanvasGroup>();
        
    }
    private void Update()
    {
        if(dropdownList != null)
            background.DOFade(dropdownList.alpha*255, 0);
        else
            background.DOFade(0, 0);
    }
}
