﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIEditGameBalance : MonoBehaviour
{
    public static UIEditGameBalance instance;
    [SerializeField]
    ContentSizeFitter[] csf;
    [SerializeField]
    GameObject target;
    public GameObject Target
    {
        get
        {
            return target;
        }
        set
        {
            target = value;

            if (target.GetComponent<IName>() == null && target.GetComponent<BreathController>())
            {
                textTargetName.text = LocalizationManager.instance.GetMessage(7)+"\r\nDragon";
                trapComponents.gameObject.SetActive(false);
                deadableComponents.gameObject.SetActive(false);
                dragonComponents.gameObject.SetActive(true);

                inputFieldBreathDamage.text = target.GetComponent<BreathController>().profile.breathDamage.ToString();
                inputFieldFireDamage.text = target.GetComponent<BreathController>().profile.fireTickDamage.ToString();
                inputFieldBreathCharginTime.text = target.GetComponent<BreathController>().profile.breathChargingTime.ToString();
                inputFieldBreathPushPower.text = target.GetComponent<BreathController>().profile.breathPushPower.ToString();
                inputFieldCollisionDamage.text = target.GetComponent<BreathController>().profile.collisionDamage.ToString();
                sliderCircleMaxSize.value = (float)target.GetComponent<BreathController>().profile.circleMaxSize;
                inputFieldMaxBreath.text = target.GetComponent<BreathController>().profile.maxBreath.ToString();
                inputFieldRecoverBreath.text = target.GetComponent<BreathController>().profile.recoverBreath.ToString();
                inputFieldRecoverBreathItem.text = target.GetComponent<BreathController>().profile.itemRecoverValue.ToString();
                inputFieldExitTime.text = target.GetComponent<BreathController>().profile.exitTime.ToString();
                inputConsumptionRate.text = target.GetComponent<BreathController>().profile.consumptionRate.ToString();


                for (int i = 0; i < csf.Length; i++)
                    LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)csf[i].transform);
                return;
            }
            dragonComponents.gameObject.SetActive(false);
            textTargetName.text = LocalizationManager.instance.GetMessage(7) + "\r\n" + (target.GetComponent<IName>() != null ? target.GetComponent<IName>().Name : target.name);
            if (target.GetComponent<Object_Deadable>())
            {
                deadableComponents.gameObject.SetActive(true);
                inputFieldMaxHP.text = target.GetComponent<Object_Deadable>().maxHP.ToString();
                if (target.GetComponent<AI_Moveable>())
                {
                    moveableComponents.gameObject.SetActive(true);
                    if (target.GetComponent<AI_Attackable>())
                    {
                        attackableComponents.gameObject.SetActive(true);
                        inputFieldAtk.text = target.GetComponent<AI_Attackable>().mobProfile.attackPower.ToString();
                        inputFieldAtkSpeed.text = target.GetComponent<AI_Attackable>().mobProfile.attackSpeed.ToString();
                        inputFieldAtkRange.text = target.GetComponent<AI_Attackable>().mobProfile.attackRange.ToString();
                        inputFieldSpeed.text = target.GetComponent<AI_Attackable>().mobProfile.moveSpeed.ToString();
                        inputFieldMass.text = target.GetComponent<AI_Attackable>().mobProfile.Mass.ToString();
                        toggleIsDestroyerAI.isOn = target.GetComponent<AI_Attackable>().mobProfile.isDestroyAI;
                    }
                    else
                        attackableComponents.gameObject.SetActive(false);

                    if (target.GetComponent<AI_Burfable>())
                    {
                        inputFieldSpeed.text = target.GetComponent<AI_Burfable>().mobProfile.moveSpeed.ToString();
                        inputFieldMass.text = target.GetComponent<AI_Burfable>().mobProfile.Mass.ToString();
                    }
                }
                else
                    moveableComponents.gameObject.SetActive(false);
            }
            else
                deadableComponents.gameObject.SetActive(false);

            if (target.GetComponent<Floor_Trap>())
            {
                inputFieldDeBurfDamage.text = target.GetComponent<Floor_Trap>().profile.deBurfDamage.ToString();
                trapComponents.gameObject.SetActive(true);
            }
            else
                trapComponents.gameObject.SetActive(false);

            for (int i = 0; i < csf.Length; i++)
                LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)csf[i].transform);
        }
    }
    public Text textTargetName;

    public RectTransform itemComponents;
    public RectTransform trapComponents;
    public RectTransform deadableComponents;
    public RectTransform moveableComponents;
    public RectTransform attackableComponents;
    public RectTransform dragonComponents;

    public InputField inputFieldDeBurfDamage;

    public InputField inputFieldBreathDamage;
    public InputField inputFieldFireDamage;
    public InputField inputFieldBreathCharginTime;
    public InputField inputFieldBreathPushPower;
    public InputField inputFieldCollisionDamage;
    public Slider sliderCircleMaxSize;
    public InputField inputFieldCircleMaxSize;
    public InputField inputFieldMaxBreath;
    public InputField inputFieldRecoverBreath;
    public InputField inputFieldRecoverBreathItem;
    public InputField inputFieldExitTime;
    public InputField inputConsumptionRate;
    public Toggle toggleIsDestroyerAI;

    public InputField inputFieldMaxHP;
    public InputField inputFieldAtk;
    public InputField inputFieldAtkRange;
    public InputField inputFieldAtkSpeed;
    public InputField inputFieldSpeed;
    public InputField inputFieldMass;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Debug.LogWarning("Scene에 2개 이상의 게임밸런싱 창이 존재합니다!");
            Destroy(gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SetDeBurfDamage(InputField inputField)
    {
        Target.GetComponent<Floor_Trap>().profile.deBurfDamage = int.Parse(inputField.text);
    }
    public void SetMaxHP(InputField inputField)
    {
        Target.GetComponent<Object_Deadable>().maxHP = int.Parse(inputField.text);
    }
    public void SetAttackPower(InputField inputField)
    {
        Target.GetComponent<AI_Attackable>().mobProfile.attackPower = int.Parse(inputField.text);
    }
    public void SetAttackSpeed(InputField inputField)
    {
        Target.GetComponent<AI_Attackable>().mobProfile.attackSpeed = float.Parse(inputField.text);
    }
    public void SetAttackRange(InputField inputField)
    {
        Target.GetComponent<AI_Attackable>().mobProfile.attackRange = float.Parse(inputField.text);
    }
    public void SetMoveSpeed(InputField inputField)
    {
        if (Target.GetComponent<AI_Attackable>())
            Target.GetComponent<AI_Attackable>().mobProfile.moveSpeed = float.Parse(inputField.text);
        else if (Target.GetComponent<AI_Burfable>())
            Target.GetComponent<AI_Burfable>().mobProfile.moveSpeed = float.Parse(inputField.text);
    }
    public void SetMass(InputField inputField)
    {
        if (Target.GetComponent<AI_Attackable>())
            Target.GetComponent<AI_Attackable>().mobProfile.Mass = float.Parse(inputField.text);
        else if (Target.GetComponent<AI_Burfable>())
            Target.GetComponent<AI_Burfable>().mobProfile.Mass = float.Parse(inputField.text);
    }
    public void SetBreathDamage(InputField inputField)
    {
        Target.GetComponent<BreathController>().profile.breathDamage = int.Parse(inputField.text);
    }
    public void SetBreathChargingTime(InputField inputField)
    {
        Target.GetComponent<BreathController>().profile.breathChargingTime = float.Parse(inputField.text);
    }
    public void SetFireTickDamage(InputField inputField)
    {
        Target.GetComponent<BreathController>().profile.fireTickDamage = int.Parse(inputField.text);
    }
    public void SetBreathPushPower(InputField inputField)
    {
        Target.GetComponent<BreathController>().profile.breathPushPower = float.Parse(inputField.text);
    }
    public void SetCollisionDamage(InputField inputField)
    {
        Target.GetComponent<BreathController>().profile.collisionDamage = int.Parse(inputField.text);
    }
    public void SetCircleMaxSize(Slider slider)
    {
        Target.GetComponent<BreathController>().profile.circleMaxSize = slider.value;
        GameManager.instance.breathController.GetComponent<BreathController>().breathCircle.transform.localScale = new Vector2(slider.value / 100, slider.value / 100);
        setInputFieldValue(inputFieldCircleMaxSize, slider.value.ToString());
    }
    public void SetCircleMaxSize(InputField inputField)
    {
        Target.GetComponent<BreathController>().profile.circleMaxSize = float.Parse(inputField.text) > 200 ? 200 : float.Parse(inputField.text);
        GameManager.instance.breathController.GetComponent<BreathController>().breathCircle.transform.localScale = new Vector2(float.Parse(inputField.text) / 100, float.Parse(inputField.text) / 100);
        setSliderValue(sliderCircleMaxSize, float.Parse(inputField.text));
    }
    public void setInputFieldValue(InputField inputField, string value)
    {
        inputField.text = value;
    }
    public void setSliderValue(Slider slider, float value)
    {
        slider.value = value;
    }
    public void SetMaxBreath(InputField inputField)
    {
        Target.GetComponent<BreathController>().profile.maxBreath = float.Parse(inputField.text);
        Target.GetComponent<BreathController>().curBreath = float.Parse(inputField.text);
    }
    public void SetRecoverBreath(InputField inputField)
    {
        Target.GetComponent<BreathController>().profile.recoverBreath = float.Parse(inputField.text);
    }
    public void SetRecoverBreathItem(InputField inputField)
    {
        Target.GetComponent<BreathController>().profile.itemRecoverValue = float.Parse(inputField.text);
    }
    public void SetExitTime(InputField inputField)
    {
        Target.GetComponent<BreathController>().profile.exitTime = float.Parse(inputField.text);
    }
    public void SetConsumptionRate(InputField inputField)
    {
        Target.GetComponent<BreathController>().profile.consumptionRate = float.Parse(inputField.text);
    }
    public void SetDestroyerAI(Toggle toggle)
    {
        Target.GetComponent<AI_Attackable>().mobProfile.isDestroyAI = toggle.isOn;
    }
}
