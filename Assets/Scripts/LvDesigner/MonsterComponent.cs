﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MonsterComponent : MonoBehaviour
{
    public GameObject target;
    [HideInInspector]
    public Image icon;
    private void Awake()
    {
        icon = GetComponent<Image>();
    }
    // Start is called before the first frame update
    void Start()
    {
        icon.preserveAspect = true;
        if (target.GetComponent<IName>() == null && target.GetComponent<BreathController>())
        {
            icon.sprite = MapDesigner.instance.icons.Find((val) => val.name.Contains("dragon_normal"));
            return;
        }

        icon.sprite = MapDesigner.instance.icons.Find((val) => target.GetComponent<IName>().Name.Equals(val.name));
        switch (target.GetComponent<IName>().Name)
        {
            case "": //아이콘 스프라이트 변경
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
    }
    public void PickUpComponent()
    {
        if (transform.parent.gameObject.name.Contains("isCanDrop"))
            MapDesigner.instance.infoSpawnerTap.isCanDrop = true;
        else
            MapDesigner.instance.infoSpawnerTap.isCanDrop = false;

        if (!MapDesigner.instance.infoSpawnerTap.isCanDrop)
        {
            var copy = Instantiate(gameObject, MapDesigner.instance.waveEditWindow.transform);
            MapDesigner.instance.infoSpawnerTap.dragedMonsterComponent = copy.GetComponent<MonsterComponent>();
            MapDesigner.instance.infoSpawnerTap.dragedMonsterComponent.icon.raycastTarget = false;
        }
        else
        {
            if (MapDesigner.instance.infoSpawnerTap.m_monsterList.childCount <= 1) return;
            MapDesigner.instance.infoSpawnerTap.dragedMonsterComponent = this;
            MapDesigner.instance.infoSpawnerTap.dragedMonsterComponent.transform.SetParent(MapDesigner.instance.waveEditWindow.transform);
            MapDesigner.instance.infoSpawnerTap.dragedMonsterComponent.icon.raycastTarget = false;
            MapDesigner.instance.infoSpawnerTap.UpdateMonsterList();
        }
    }
    public void OnDrop()
    {
        if (transform.parent.gameObject.name.Contains("isCanDrop"))
            MapDesigner.instance.infoSpawnerTap.isCanDrop = true;
        else
            MapDesigner.instance.infoSpawnerTap.isCanDrop = false;

        if (!MapDesigner.instance.infoSpawnerTap.isCanDrop) return;

        MapDesigner.instance.infoSpawnerTap.dragedMonsterComponent.transform.SetParent(transform.parent);

        if (MapDesigner.instance.infoSpawnerTap.dragedMonsterComponent.transform.position.y > transform.position.y)
            MapDesigner.instance.infoSpawnerTap.dragedMonsterComponent.transform.SetSiblingIndex(transform.GetSiblingIndex());
        else
            MapDesigner.instance.infoSpawnerTap.dragedMonsterComponent.transform.SetSiblingIndex(transform.GetSiblingIndex() + 1);

        MapDesigner.instance.infoSpawnerTap.dragedMonsterComponent.icon.raycastTarget = true;
        MapDesigner.instance.infoSpawnerTap.dragedMonsterComponent = null;
        MapDesigner.instance.infoSpawnerTap.UpdateMonsterList();
    }
    
    public void OnClick()
    {
        UIEditGameBalance.instance.Target = target;
    }
}
