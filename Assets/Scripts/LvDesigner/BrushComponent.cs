﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BrushComponent : MonoBehaviour
{
    public GameObject target;
    [HideInInspector]
    public Image icon;

    // Start is called before the first frame update
    void Start()
    {
        icon = GetComponent<Image>();
        icon.sprite = MapDesigner.instance.icons.Find((val) => target.GetComponent<IName>().Name.Equals(val.name));
        icon.preserveAspect = true;
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void OnSelectBrush()
    {
        if (MapDesigner.instance.currentBrush != this)
            MapDesigner.instance.currentBrush = this;
        MapDesigner.instance.DoSelectTool(0);
    }
   
}
