﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class GratingComponent : MonoBehaviour
{
    public GameObject target;
    Button button;
    Text textID;
    bool isSelected;
    public bool IsSelected
    {
        get
        {
            return isSelected;
        }
        set
        {
            isSelected = value;
            MapDesigner.instance.selectCursor.position = (Vector2)target.transform.position;
            Camera.main.transform.DOMove(new Vector3(target.transform.position.x+5, target.transform.position.y, Camera.main.transform.position.z),0.5f) ;
            if (isSelected)
                button.image.color = Color.blue;
            else
                button.image.color = Color.white;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        button = GetComponent<Button>();
        textID = transform.GetChild(0).GetComponent<Text>();
        textID.text = "ID: " + target.GetComponent<IName>().ID;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SwitchSelected()
    {
        IsSelected = !isSelected;
    }
}
