﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StageComponent : MonoBehaviour
{
    public ClearDataInfo clearData;
    public Text textStageNumber;
    public Image[] imagesEggs;
    public Image imageLock;
    // Start is called before the first frame update
    void Start()
    {
        for(int i =0; i < clearData.eggCount;i++)
            imagesEggs[i].sprite = UIMainManager.instance.spriteEnableEgg;
        GetComponent<Button>().onClick.AddListener(() => { DataManager.instance.StartGame(this); });
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
