﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaveComponent : MonoBehaviour
{
    public Button button;
    public Text textName;
    public Block_Spawner.Wave wave;
    // Start is called before the first frame update
    private void Awake()
    {
        button = GetComponent<Button>();
    }
    void Start()
    {
        textName.text = wave.name;
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void ClickWave()
    {
        if(MapDesigner.instance.infoSpawnerTap.dragedMonsterComponent == null)
        switch (MapDesigner.instance.infoSpawnerTap.selectState)
        {
            case DesignerSpawnerTapController.SelectState.Select:
                MapDesigner.instance.infoSpawnerTap.SelectedWave = wave;
                MapDesigner.instance.infoSpawnerTap.selectedWaveComponent = this;
                MapDesigner.instance.waveEditWindow.SetActive(true);
                break;
            case DesignerSpawnerTapController.SelectState.Remove:
                if (transform.parent.childCount <= 1) return;
                Destroy(gameObject);
                break;
            case DesignerSpawnerTapController.SelectState.Copy:
                MapDesigner.instance.infoSpawnerTap.SelectedWave = wave;
                MapDesigner.instance.infoSpawnerTap.selectedWaveComponent = this;
                MapDesigner.instance.infoSpawnerTap.CopyWave();
                break;
        }
    }
    public void PickUpComponent()
    {
        if (MapDesigner.instance.infoSpawnerTap.wavesTransform.childCount <= 1 || MapDesigner.instance.infoSpawnerTap.selectState != DesignerSpawnerTapController.SelectState.Select) return;
        MapDesigner.instance.infoSpawnerTap.dragedWaveComponent = this;
        MapDesigner.instance.infoSpawnerTap.dragedWaveComponent.transform.SetParent(null
            //MapDesigner.instance.infoSpawnerTap.transform.parent
            );
        
        //MapDesigner.instance.infoSpawnerTap.dragedWaveComponent.transform.SetSiblingIndex(MapDesigner.instance.infoSpawnerTap.transform.GetSiblingIndex());
        MapDesigner.instance.infoSpawnerTap.dragedWaveComponent.button.interactable = false;
        MapDesigner.instance.infoSpawnerTap.UpdateWaveView();
    }
    public void OnDrop()
    {
        if (!MapDesigner.instance.infoSpawnerTap.isCanDrop || MapDesigner.instance.infoSpawnerTap.selectState != DesignerSpawnerTapController.SelectState.Select) return;

        MapDesigner.instance.infoSpawnerTap.dragedWaveComponent.transform.SetParent(transform.parent);

        if (MapDesigner.instance.infoSpawnerTap.dragedWaveComponent.transform.position.y > transform.position.y)
            MapDesigner.instance.infoSpawnerTap.dragedWaveComponent.transform.SetSiblingIndex(transform.GetSiblingIndex());
        else
            MapDesigner.instance.infoSpawnerTap.dragedWaveComponent.transform.SetSiblingIndex(transform.GetSiblingIndex() + 1);

        MapDesigner.instance.infoSpawnerTap.targetSpawner.waves.Clear();
        for(int i=0; i < transform.parent.childCount;i++)
            MapDesigner.instance.infoSpawnerTap.targetSpawner.waves.Add(transform.parent.GetChild(i).GetComponent<WaveComponent>().wave);
        MapDesigner.instance.infoSpawnerTap.dragedWaveComponent.button.interactable = true;
        MapDesigner.instance.infoSpawnerTap.dragedWaveComponent = null;
        MapDesigner.instance.infoSpawnerTap.UpdateWaveView();
    }
}
