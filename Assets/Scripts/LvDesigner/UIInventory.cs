﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIInventory : MonoBehaviour
{
    //0 불, 1 얼음, 2 바람, 3 전기
    public EntitySkillStone[] slots;
    // Start is called before the first frame update
    public Transform[] slotPositions;
}
