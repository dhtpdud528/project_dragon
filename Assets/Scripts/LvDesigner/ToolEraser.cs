﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToolEraser : MonoBehaviour
{
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.GetComponent<IName>() == null || collision.GetComponent<IName>().Name.Contains("RainbowEnd")) return;
        if (!MapDesigner.instance.lockObjs.Contains(collision.gameObject))
            Destroy(collision.gameObject);
    }
}
