﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitEvent : MonoBehaviour
{
    public GameObject tut;
    // Start is called before the first frame update
    void Start()
    {
        GameManager.instance.Resume();
        StartCoroutine(WaitAttack());
    }
    IEnumerator WaitAttack()
    {
        yield return new WaitUntil(() => GameManager.instance.dragonController.state == DragonController.DragonState.Attack);
        yield return new WaitForSeconds(2);
        GameManager.instance.Pause();
        tut.SetActive(true);
        gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
