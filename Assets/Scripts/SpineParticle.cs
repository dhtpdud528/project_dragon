﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public class SpineParticle : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<SkeletonAnimation>().AnimationState.GetCurrent(0).IsComplete)
            Destroy(gameObject);
    }
}
