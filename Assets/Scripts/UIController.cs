﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UIController : MonoBehaviour
{
    public Transform contentsTranform;
    public float slideValue;
    public bool openX;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }
    public virtual void switchSettingMenu()
    {
        if (openX)
            if (contentsTranform.localPosition.x > -10)
                contentsTranform.DOLocalMoveX(-slideValue, .2f);
            else
                contentsTranform.DOLocalMoveX(0, .2f);
        else
        {
            if (contentsTranform.localPosition.y > -10)
                contentsTranform.DOLocalMoveY(-slideValue, .2f);
            else
                contentsTranform.DOLocalMoveY(0, .2f);
        }
    }
    public virtual void switchSettingMenu(bool val)
    {
        if (openX)
            if (val)
                contentsTranform.DOLocalMoveX(-slideValue, .2f);
            else
                contentsTranform.DOLocalMoveX(0, .2f);
        else
        {
            if (val)
                contentsTranform.DOLocalMoveY(-slideValue, .2f);
            else
                contentsTranform.DOLocalMoveY(0, .2f);
        }
    }
    public virtual void UpdateInfo(GameObject lastSelected)
    {
    }
}
