﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopUpBoxManager : MonoBehaviour
{
    public static PopUpBoxManager instance;
    public GameObject currentPopupBox;
    public UI_PopUpBox confirmBox;
    public UI_PopUpBox rewardADBox;
    public GameObject quitBox;
    public void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Debug.LogWarning("Scene에 2개 이상의 팝업 매니저가 존재합니다!");
            Destroy(gameObject);
        }

        
    }
    public void PopConfirmBox(string val)
    {
        confirmBox.gameObject.SetActive(true);
        confirmBox.textBox.text = val;
    }
    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (currentPopupBox != null && currentPopupBox.activeSelf)
            {
                if (GameManager.instance != null)
                    GameManager.instance.Resume();
                currentPopupBox.SetActive(false);
            }
            else if (GameManager.instance == null)
                if (!quitBox.activeSelf)
                    quitBox.SetActive(true);
                else
                    Application.Quit();
        }
    }
}
