﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.IO;


public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public Text textLog;
    public Transform gameCanvas;
    StageTheme stageTheme;
    public StageTheme m_StageTheme
    {
        get
        {
            return stageTheme;
        }
        set
        {
            stageTheme = value;
            mapBackground.sprite = MapDataManager.instance.backgrounds[(int)stageTheme];
        }
    }
    public void SetStageTheme(Dropdown dropdown)
    {
        m_StageTheme = (StageTheme)dropdown.value;
    }
    [Header("- 스테이지 정보")]
    public string stageName;
    public Text stageNameText;
    public SpriteRenderer mapBackground;
    [SerializeField]
    bool isGamePlay;
    public bool IsGamePlay
    {
        get
        {
            return isGamePlay;
        }
        set
        {
            isGamePlay = value;
            time = 0;
            if (MapDesigner.instance != null)
                StartCoroutine(RefreshGame());
            else if (isGamePlay)
            {
                string path = //Application.persistentDataPath + 
                    "MapData/";
                MapDataManager.instance.LoadFromResources(path + stageName//+".json"
                    );

                SupplyItem(BreathType.fire, startSupply.fireCount);
                SupplyItem(BreathType.ice, startSupply.iceCount);
                SupplyItem(BreathType.lightning, startSupply.lightningCount);
                SupplyItem(BreathType.wind, startSupply.windCount);
            }
        }
    }
    IEnumerator RefreshGame()
    {
        ObjectPactory.instance.Save();
        MapDataManager.instance.Save(true);
        yield return new WaitUntil(() => MapDataManager.instance.path != null);
        yield return new WaitForEndOfFrame();

        if (MapDesigner.instance.infoSpawnerTap.gameObject.activeSelf)
            MapDesigner.instance.infoSpawnerTap.WaveViewClear();
        MapDataManager.instance.SearchAndLoad();
        yield return new WaitForEndOfFrame();
        if (IsGamePlay)
        {
            SupplyItem(BreathType.fire, startSupply.fireCount);
            SupplyItem(BreathType.ice, startSupply.iceCount);
            SupplyItem(BreathType.lightning, startSupply.lightningCount);
            SupplyItem(BreathType.wind, startSupply.windCount);
            var breathColliders = FindObjectsOfType<BreathCollider>();
            for (int i = 0; i < breathColliders.Length; i++)
                Destroy(breathColliders[i]);
            dragonController.breathProfiles.Clear();

        }
    }
    public void SupplyItem(BreathType type, int count)
    {
        StartCoroutine(SupplyItemCoroutine(type, count));
    }
    IEnumerator SupplyItemCoroutine(BreathType type, int count)
    {
        float rate = 0.01f;
        EntitySkillStone skill;
        switch (type)
        {
            case BreathType.fire:
                for (int i = 0; i < count; i++)
                {
                    skill = Instantiate(GameManager.instance.item, transform.position, Quaternion.identity).GetComponent<EntitySkillStone>();
                    skill.Type = BreathType.fire;
                    skill.anim.CrossFadeInFixedTime("slot", 0);
                    skill.MoveToSlot();
                    yield return new WaitForSeconds(rate);
                }
                break;
            case BreathType.ice:
                for (int i = 0; i < count; i++)
                {
                    skill = Instantiate(GameManager.instance.item, transform.position, Quaternion.identity).GetComponent<EntitySkillStone>();
                    skill.Type = BreathType.ice;
                    skill.anim.CrossFadeInFixedTime("slot", 0);
                    skill.MoveToSlot();
                    yield return new WaitForSeconds(rate);
                }
                break;
            case BreathType.lightning:
                for (int i = 0; i < count; i++)
                {
                    skill = Instantiate(GameManager.instance.item, transform.position, Quaternion.identity).GetComponent<EntitySkillStone>();
                    skill.Type = BreathType.lightning;
                    skill.anim.CrossFadeInFixedTime("slot", 0);
                    skill.MoveToSlot();
                    yield return new WaitForSeconds(rate);
                }
                break;
            case BreathType.wind:
                for (int i = 0; i < count; i++)
                {
                    skill = Instantiate(GameManager.instance.item, transform.position, Quaternion.identity).GetComponent<EntitySkillStone>();
                    skill.Type = BreathType.wind;
                    skill.anim.CrossFadeInFixedTime("slot", 0);
                    skill.MoveToSlot();
                    yield return new WaitForSeconds(rate);
                }
                break;
        }
    }
    public float time;
    public int score;
    public Text timer;
    public enum BreathType { normal, fire, ice, wind, lightning }
    public GameObject hoverObject;
    public GameObject item;

    [Header("- 게임 파티클")]
    public GameObject[] firePlaceParticle;
    public GameObject[] fireDeburfParticle;
    public GameObject[] icePlaceParticle;
    public GameObject[] iceDeburfParticle;
    public GameObject[] windPlaceParticle;
    public GameObject[] electricPlaceParticle;
    public GameObject[] electricDeburfParticle;

    [Header("- 플레이어")]
    public Image breathGauge;
    public BreathController breathController;
    public DragonController dragonController;
    public UIInventory inventory;
    public AudioSource audioSource_Default;


    [Header("- 게임 오브젝트")]
    public List<Object_Deadable> eggs = new List<Object_Deadable>();
    public List<Block_Spawner> spawners = new List<Block_Spawner>();
    public List<AI_Attackable> monsters = new List<AI_Attackable>();

    [Header("- 게임 결과")]
    public bool isAllWavesEnd;
    public GameObject result;
    public Animator dragonSpine;
    public Animator resultSpine;
    public Button nextStageButton;

    [Header("- 튜토리얼 컨텐츠")]
    public GameObject tutContents;

    [Header("-")]
    public AstarPath astarPath;
    public List<int> ids = new List<int>();
    public StartSupplyInfo startSupply = new StartSupplyInfo(0, 0, 0, 0);


    // Start is called before the first frame update
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Debug.LogWarning("Scene에 2개 이상의 게임 매니저가 존재합니다!");
            Destroy(gameObject);
        }
        astarPath = FindObjectOfType<AstarPath>();
        inventory = FindObjectOfType<UIInventory>();
    }
    void Start()
    {
        StartCoroutine(initGame());
    }
    IEnumerator initGame()
    {
        yield return new WaitForEndOfFrame();
        if (stageName != "")
        {
            SoundManager.instance.audioSourceBGM.clip = SoundManager.instance.gameBGM;
            SoundManager.instance.audioSourceBGM.Play();
            if (stageName.Equals("Forest_1") && !PlayerPrefs.HasKey("TutComplete") && tutContents != null)
            {
                tutContents.SetActive(true);
                PlayerPrefs.SetInt("TutComplete", 1);
            }
        }
        else
        {
            SoundManager.instance.audioSourceBGM.clip = SoundManager.instance.lvDesignerBGM;
            SoundManager.instance.audioSourceBGM.Play();
        }
    }
    public void AstarScan()
    {
        if (!astarPath.isScanning)
        {
            astarPath.Scan();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!IsGamePlay || result.activeSelf) return;
        time += Time.deltaTime;
        timer.text = LocalizationManager.instance.GetMessage(8) + ": " + Mathf.Ceil(time).ToString();
    }
    public void Pause()
    {
        Time.timeScale = 0;
    }
    public void Resume()
    {
        Time.timeScale = 1;
    }
    public bool IsAllMonsterDead()
    {
        if (monsters.Count > 0)
            return false;
        else
            return true;
    }
    public bool IsAllWavesEnd()
    {
        for (int i = 0; i < spawners.Count; i++)
        {
            if (spawners[i].state != Block_Spawner.SpawnState.END)
                return false;
        }
        return true;
    }
    public int getAliveEggsCount()
    {
        int eggCount = 3;
        for (int i = 0; i < GameManager.instance.eggs.Count; i++)
        {
            if (GameManager.instance.eggs[i].HP <= 0)
                eggCount--;
        }
        return eggCount;
    }
    public static bool IsPointerOverGameObject()
    {
        // Check mouse
        if (EventSystem.current.IsPointerOverGameObject())
        {
            return true;
        }

        // Check touches
        for (int i = 0; i < Input.touchCount; i++)
        {
            var touch = Input.GetTouch(i);
            if (touch.phase == TouchPhase.Began)
            {
                if (EventSystem.current.IsPointerOverGameObject(touch.fingerId))
                {
                    return true;
                }
            }
        }
        return false;
    }
    public void StageClear()
    {
        SoundManager.instance.PlayOneShot_SFX(SoundManager.instance.winSound);
        Time.timeScale = 1;
        result.SetActive(true);

        var split = stageName.Split('_');
        if (!Resources.Load("MapData/" + split[0] + "_" + (int.Parse(split[1]) + 1))
            //!File.Exists(Application.persistentDataPath + "/SaveData/MapData/" + split[0] + "_" + (int.Parse(split[1]) + 1)+".json")
            )
            nextStageButton.interactable = false;

        int eggCount = GameManager.instance.getAliveEggsCount();
        resultSpine.SetInteger("Eggs", eggCount);
        DataManager.instance.SaveClearData(stageName, time, score, eggCount);

    }
    public void StageRestart()
    {
        DataManager.instance.StartGame(stageName);
    }
    public void BackToTheMain()
    {
        Time.timeScale = 1;
        SceneTransformManager.instance.MoveToScene("Main");
    }
    public void LoadNextStage()
    {
        Time.timeScale = 1;
        var split = stageName.Split('_');
        DataManager.instance.StartGame(split[0] + "_" + (int.Parse(split[1]) + 1));
    }
}
public class BreathProfile
{
    public Vector2 position;
    public float size;
    public Animator circle;

    public BreathProfile(Vector2 position, float size, Animator circle)
    {
        this.position = position;
        this.size = size;
        this.circle = circle;
    }
}

public interface IName
{
    string Name { get; set; }
    int ID { get; set; }
}
