﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTransformManager : MonoBehaviour
{
    public static SceneTransformManager instance;
    public GameObject loadingOBJ;
    public Scene currentScene;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Debug.LogWarning("Scene에 2개 이상의 씬전환 매니저가 존재합니다!");
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
        currentScene = SceneManager.GetActiveScene();
    }
    public void MoveToScene(string sceneName)
    {
        loadingOBJ.SetActive(true);
        SceneManager.LoadScene(sceneName);
    }
    public void Update()
    {
        if(currentScene != SceneManager.GetActiveScene())
        {
            loadingOBJ.SetActive(false);
            currentScene = SceneManager.GetActiveScene();
        }
    }
}
