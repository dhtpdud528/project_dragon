﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Extension;
using UnityEngine.Purchasing.Security;
using UnityEngine.Analytics;
using UnityEngine.UI;
using System.Threading.Tasks;
using UnityEngine.Networking;
using System;

public class IAPManager : MonoBehaviour, IStoreListener
{
    public static IAPManager instance;
    // 영수증
    public class Receipt
    {
        public string index;
        public string uid;
        public string transaction_id;
        public string product_id;
        public string platform;
        public string price;
        public string date;
    }

    static IStoreController storeController = null;
    private IExtensionProvider m_StoreExtensionProvider;
    //public bool isPurchaseUnderProcess = true;

    // 결재 품목 목록
    static string[] sProductIds;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Debug.LogWarning("Scene에 2개 이상의 IAP 매니저가 존재합니다!");
            Destroy(gameObject);
        }

        if (storeController == null)
        {
            // 인앱상품 - 관리되는 제품의 ID
            sProductIds = new string[] { "noads", "heart5", "icetheme" };
            InitStore();
        }
    }

    private bool IsInitialized()
    {
        // Only say we are initialized if both the Purchasing references are set.
        return storeController != null && m_StoreExtensionProvider != null;
    }
    // 결제 상품 초기화
    void InitStore()
    {
        if (IsInitialized()) return;

        ConfigurationBuilder builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
        builder.AddProduct(sProductIds[0], ProductType.NonConsumable, new IDs { { sProductIds[0], GooglePlay.Name } });
        builder.AddProduct(sProductIds[1], ProductType.Consumable, new IDs { { sProductIds[1], GooglePlay.Name } });
        builder.AddProduct(sProductIds[2], ProductType.NonConsumable, new IDs { { sProductIds[2], GooglePlay.Name } });

        // 초기화
        UnityPurchasing.Initialize(this, builder);
    }
    void IStoreListener.OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        storeController = controller;
        Debug.Log("결제 기능 초기화 완료");

        Product productNoADs = storeController.products.WithID(sProductIds[0]);
        if (productNoADs != null && productNoADs.hasReceipt)
        {
            MyGoogleAdsMobs.instance.isNoAds = true;
            Debug.Log(productNoADs.receipt);
        }

        Product productIceTheme = storeController.products.WithID(sProductIds[2]);
        if (productIceTheme != null && productIceTheme.hasReceipt)
        {
            DataManager.instance.isCanPlayIce = true;
            Debug.Log(productIceTheme.receipt);
        }

    }

    void IStoreListener.OnInitializeFailed(InitializationFailureReason error)
    {
        Debug.Log("OnInitializeFailed" + error);
    }

    public void OnBtnPurchaseClicked(int index)
    {
        if (storeController == null)
        {
            Debug.Log("구매 실패 : 결제 기능 초기화 실패");
        }
        else
            storeController.InitiatePurchase(sProductIds[index]);
    }

    PurchaseProcessingResult IStoreListener.ProcessPurchase(PurchaseEventArgs e)
    {
        Debug.Log("구매 시도");
        bool isSuccess = true;
#if UNITY_ANDROID && !UNITY_EDITOR
		CrossPlatformValidator validator = new CrossPlatformValidator(GooglePlayTangle.Data(), AppleTangle.Data(), Application.identifier);
		try
		{
			IPurchaseReceipt[] result = validator.Validate(e.purchasedProduct.receipt);
			for(int i = 0; i < result.Length; i++)
				Analytics.Transaction(result[i].productID, e.purchasedProduct.metadata.localizedPrice, e.purchasedProduct.metadata.isoCurrencyCode, result[i].transactionID, null);
		}
		catch (IAPSecurityException)
		{

			isSuccess = false;
		}
#endif
        if (isSuccess)
        {
            if (e.purchasedProduct.definition.id.Equals(sProductIds[0]))
            {
                BuyNoADs();
            }
            else if (e.purchasedProduct.definition.id.Equals(sProductIds[1]))
            {
                Buy5Heart();
            }
            else if (e.purchasedProduct.definition.id.Equals(sProductIds[2]))
            {
                BuyIceTheme();
            }
            Debug.Log("지급 성공");
        }
        else
            Debug.Log("구매 실패 : 비정상 결제");

        // 결제 완료
        Debug.Log("결제 완료");
        return PurchaseProcessingResult.Complete;
    }

    void IStoreListener.OnPurchaseFailed(Product i, PurchaseFailureReason error)
    {
        if (!error.Equals(PurchaseFailureReason.UserCancelled))
        {
            Debug.Log("구매 실패 : " + error);
        }
    }
    public void Buy5Heart()
    {
        DataManager.instance.HeartRewardStack += 5;
    }
    public void BuyNoADs()
    {
        MyGoogleAdsMobs.instance.isNoAds = true;
        PopUpBoxManager.instance.PopConfirmBox("구매해주셔서 감사합니다!");
        SoundManager.instance.audioSourceSFX.PlayOneShot(SoundManager.instance.buyNoADsSound);
    }
    public void BuyIceTheme()
    {
        DataManager.instance.isCanPlayIce = true;
        UIStageManager.instance.iceTheme.interactable = true;
        PopUpBoxManager.instance.PopConfirmBox("구매해주셔서 감사합니다!");
        UIMainManager.instance.iceEggCountObjs.SetActive(false);
        SoundManager.instance.audioSourceSFX.PlayOneShot(SoundManager.instance.buyNoADsSound);
    }
    public void FailedPay()
    {
        SoundManager.instance.audioSourceSFX.PlayOneShot(SoundManager.instance.failedPay);
    }
}
