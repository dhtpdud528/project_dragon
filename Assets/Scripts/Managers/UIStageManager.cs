﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using JetBrains.Annotations;

public enum StageTheme { Forest, Ice, Infinity }

public class UIStageManager : MonoBehaviour
{
    public static UIStageManager instance;
    [SerializeField]
    private bool isTest;
    [HideInInspector]
    public Animator anim;
    public AnimatorStateInfo animInfo;

    [SerializeField]
    StageTheme stageTheme;
    public StageTheme m_StageTheme
    {
        get
        {
            return stageTheme;
        }
        set
        {
            stageTheme = value;
            switch (stageTheme)
            {
                case StageTheme.Forest:
                    stageForestLayout.gameObject.SetActive(true);
                    stageIceLayout.gameObject.SetActive(false);
                    stageInfinityLayout.gameObject.SetActive(false);
                    if (!IsStageToTheme())
                        anim.CrossFadeInFixedTime("IceToForest", 0);
                    break;
                case StageTheme.Ice:
                    stageForestLayout.gameObject.SetActive(false);
                    stageIceLayout.gameObject.SetActive(true);
                    stageInfinityLayout.gameObject.SetActive(false);
                    if (!IsStageToTheme())
                        anim.CrossFadeInFixedTime("ForestToIce", 0);
                    break;
                case StageTheme.Infinity:
                    stageForestLayout.gameObject.SetActive(false);
                    stageIceLayout.gameObject.SetActive(false);
                    stageInfinityLayout.gameObject.SetActive(true);
                    break;
            }
        }
    }
    StageTheme tempTheme;
    public Button forestTheme;
    public Button iceTheme;
    public Button lvDesigner;
    public GameObject stageComponent;
    public Transform stageForestLayout;
    public Transform stageIceLayout;
    public Transform stageInfinityLayout;
    public CanvasGroup StageUI;
    public int forestStageCount;
    public int iceStageCount;
    public int infinityStageCount;
    List<string> forestStages = new List<string>();
    List<string> iceStages = new List<string>();
    List<string> infinityStages = new List<string>();

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Debug.LogWarning("Scene에 2개 이상의 스테이지 매니저가 존재합니다!");
            Destroy(gameObject);
        }
        anim = GetComponent<Animator>();
    }
    // Start is called before the first frame update
    void Start()
    {
        InitStage(StageTheme.Forest);
        InitStage(StageTheme.Ice);
        //InitStage(StageTheme.Infinity);
        stageForestLayout.gameObject.SetActive(true);
        stageIceLayout.gameObject.SetActive(false);
        stageInfinityLayout.gameObject.SetActive(false);

        if (UIMainManager.instance.EggsCount >= 50 || DataManager.instance.isCanPlayIce)
        {
            iceTheme.interactable = true;
            UIMainManager.instance.iceEggCountObjs.SetActive(false);
        }
        else
        {
            iceTheme.interactable = false;
            UIMainManager.instance.iceEggCountObjs.SetActive(true);
        }
        //if (UIMainManager.instance.EggsCount >= 80)
        //{
        //    lvDesigner.interactable = true;
        //    UIMainManager.instance.lvDesignerEggCountObjs.SetActive(false);
        //}
        //else
        //{
        //    lvDesigner.interactable = false;
        //    UIMainManager.instance.lvDesignerEggCountObjs.SetActive(true);
        //}

    }
    public void InitStage(StageTheme stageTheme)
    {
        List<string> themeStages = null;
        Transform layout = null;
        string themeName = "Forest_";
        int count = 0;
        switch (stageTheme)
        {
            case StageTheme.Forest:
                themeStages = forestStages;
                themeName = "Forest_";
                layout = stageForestLayout;
                count = forestStageCount;
                break;
            case StageTheme.Ice:
                themeStages = iceStages;
                themeName = "Ice_";
                layout = stageIceLayout;
                count = iceStageCount;
                break;
            case StageTheme.Infinity:
                themeStages = infinityStages;
                themeName = "Infinity_";
                layout = stageInfinityLayout;
                count = infinityStageCount;
                break;
        }

        for (int i = 0; i < count; i++)
            themeStages.Add(themeName + (i + 1));

        for (int i = 0; i < themeStages.Count; i++)
        {
            var component = Instantiate(stageComponent, layout).GetComponent<StageComponent>();
            if (File.Exists(Application.persistentDataPath + "/SaveData/ClearData/" + themeName + (i + 1) + ".json") && (component.clearData = DataManager.instance.LoadClearData(themeName + (i + 1))) != null)
            {
                UIMainManager.instance.EggsCount += component.clearData.eggCount;
                component.imageLock.gameObject.SetActive(false);
                component.GetComponent<Button>().interactable = true;
            }
            else
            {
                component.clearData = new ClearDataInfo(themeStages[i], false, 0, 0, 0);
                if (i > 0 && File.Exists(Application.persistentDataPath + "/SaveData/ClearData/" + themeName + i + ".json") && DataManager.instance.LoadClearData(themeName + i) != null)
                {
                    component.imageLock.gameObject.SetActive(false);
                    component.GetComponent<Button>().interactable = true;
                }
            }

            if (i < 9)
            {
                if (i == 0)
                {
                    component.imageLock.gameObject.SetActive(false);
                    component.GetComponent<Button>().interactable = true;
                }
                component.textStageNumber.text = "0" + (i + 1);
            }
            else
                component.textStageNumber.text = (i + 1).ToString();

            if(isTest)
            {
                component.imageLock.gameObject.SetActive(false);
                component.GetComponent<Button>().interactable = true;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        animInfo = anim.GetCurrentAnimatorStateInfo(0);
    }
    public void ChangeSlot(int i)
    {
        if (!IsStageSelectUI())
        {
            m_StageTheme += i;
            if ((int)stageTheme > 1)
                m_StageTheme = (StageTheme)0;
            else if ((int)stageTheme < 0)
                m_StageTheme = (StageTheme)1;
        }
    }
    public void SelectInfinityMode()
    {
        tempTheme = m_StageTheme;
        m_StageTheme = (StageTheme)2;
    }
    public void ThemeToStage()
    {
        anim.CrossFadeInFixedTime("ThemeToStage", 0);
    }
    public void StageToTheme()
    {
        if (m_StageTheme == StageTheme.Infinity)
            StartCoroutine(OutInfinityMode());
        anim.CrossFadeInFixedTime("StageToTheme", 0);
    }
    IEnumerator OutInfinityMode()
    {
        yield return new WaitUntil(() => StageUI.alpha <= 0);
        m_StageTheme = tempTheme;
    }
    public bool IsStageSelectUI()
    {
        for (int i = 0; i < anim.GetCurrentAnimatorClipInfo(0).Length; i++)
            return anim.GetCurrentAnimatorClipInfo(0)[i].clip.name.Equals("ThemeToStage");
        return false;
    }
    public bool IsStageToTheme()
    {
        for (int i = 0; i < anim.GetCurrentAnimatorClipInfo(0).Length; i++)
            return anim.GetCurrentAnimatorClipInfo(0)[i].clip.name.Equals("StageToTheme");

        return false;
    }
}
