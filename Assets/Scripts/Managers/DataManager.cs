﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using LitJson;
using UnityEngine.Networking;
using DG.Tweening;

[Serializable]
public class PlayerProfile
{
    [SerializeField]
    int heartCount;
    public int HeartCount
    {
        get
        {
            return heartCount;
        }
        set
        {
            heartCount = value;
            if (heartCount < 0)
                heartCount = 0;
            if (UIMainManager.instance != null)
            {
                UIMainManager.instance.heartOverCountText.text = "";
                for (int i = 0; i < UIMainManager.instance.heartImages.childCount; i++)
                    UIMainManager.instance.heartImages.GetChild(i).gameObject.SetActive(false);

                for (int i = 0; i < UIMainManager.instance.heartImages.childCount && i < heartCount; i++)
                    UIMainManager.instance.heartImages.GetChild(i).gameObject.SetActive(true);
                
                if (heartCount > 5)
                    UIMainManager.instance.heartOverCountText.text = "+" + (heartCount - 5);
            }
            //Debug.Log("하트갯수: "+heartCount);
        }
    }
    public int remainHeartSupplyTime;
    public int eggCount;
    public int highScore_Infinitymode;

    public bool isSound_ON;
    public bool isBGM_ON;

    public PlayerProfile(int heartCount, int supplyTime, int eggCount, int highScore_Infinitymode, bool isSound_ON, bool isBGM_ON)
    {
        this.heartCount = heartCount;
        this.remainHeartSupplyTime = supplyTime;
        this.eggCount = eggCount;
        this.highScore_Infinitymode = highScore_Infinitymode;
        this.isSound_ON = isSound_ON;
        this.isBGM_ON = isBGM_ON;
    }
}
public class DataManager : MonoBehaviour
{
    public static DataManager instance;
    [Header("- 이미지")]
    public Sprite spriteHeart;

    [Header("- 데이터")]
    public string saveKey;
    private DateTime m_AppQuitTime = new DateTime(1970, 1, 1).ToLocalTime();
    private const int MAX_HEART = 5;
    public int HeartSupplyInterval = 30;
    private Coroutine supplyTimerCoroutine = null;

    public PlayerProfile playerProfile;

    public GameObject sendPackage;
    private Queue<IEnumerator> giveRewardCoroutine = new Queue<IEnumerator>();
    private IEnumerator currentGiveRewardCoroutine;
    public bool isCanPlayIce;

    [Header("- 보상")]
    public GameObject RewardDirection;
    public Image RewardIcon;
    public Text RewardText;

    public int heartRewardStack;

    public int HeartRewardStack
    {
        get
        {
            return heartRewardStack;
        }
        set
        {
            heartRewardStack = value;
            GiveRewardDirection(spriteHeart, heartRewardStack);
            SaveHeartInfo();
        }
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
            Init();
        }
        else
        {
            //Debug.LogWarning("Scene에 2개 이상의 데이터 매니저가 존재합니다!");
            Destroy(gameObject);
        }
        
    }
    private void Start()
    {
        MainInit();
        Application.targetFrameRate = 300;
        QualitySettings.vSyncCount = 0;
        List<DirectoryInfo> di = new List<DirectoryInfo>();
        di.Add(new DirectoryInfo(Application.persistentDataPath + "/SaveData/MapDesigner/"));
        di.Add(new DirectoryInfo(Application.persistentDataPath + "/BackupData/MapDesigner/"));
        di.Add(new DirectoryInfo(Application.persistentDataPath + "/SaveData/ClearData/"));
        //di.Add(new DirectoryInfo(Application.persistentDataPath + "/SaveData/MapData/"));
        di.Add(new DirectoryInfo(Application.persistentDataPath + "/SaveData/MapDesigner/"));
        for (int i = 0; i < di.Count; i++)
            if (di[i].Exists == false)
                di[i].Create();
    }
    public void Update()
    {
        if (giveRewardCoroutine.Count > 0 && currentGiveRewardCoroutine == null)
            StartCoroutine(currentGiveRewardCoroutine = giveRewardCoroutine.Dequeue());
    }
    public void SaveLanguageInfo()
    {
        try
        {
            PlayerPrefs.SetInt("LANGUAGE", (int)LocalizationManager.instance.language);
            PlayerPrefs.Save();
        }
        catch (System.Exception e)
        {
            //Debug.LogError("SaveHeartInfo Failed (" + e.Message + ")");
        }
    }
    //게임 초기화, 중간 이탈, 중간 복귀 시 실행되는 함수
    public void OnApplicationFocus(bool value)
    {
        if (value)
        {
            LoadHeartInfo();
            LoadAppQuitTime();
            SetRechargeScheduler();
        }
        else
        {
            SaveHeartInfo();
            SaveLanguageInfo();
            SaveAppQuitTime();
            if (supplyTimerCoroutine != null)
            {
                StopCoroutine(supplyTimerCoroutine);
            }
        }
    }
    //게임 종료 시 실행되는 함수
    public void OnApplicationQuit()
    {
        //Debug.Log("GoodsRechargeTester: OnApplicationQuit()");
        SaveHeartInfo();
        SaveLanguageInfo();
        SaveAppQuitTime();
    }
    //버튼 이벤트에 이 함수를 연동한다.
    public void OnClickUseHeart()
    {
        //Debug.Log("OnClickUseHeart");
        UseHeart();
    }

    public void Init()
    {
        playerProfile.HeartCount = 5;
        playerProfile.remainHeartSupplyTime = HeartSupplyInterval;
        m_AppQuitTime = new DateTime(1970, 1, 1).ToLocalTime();
        ////Debug.Log("heartRechargeTimer : " + playerProfile.remainHeartSupplyTime + "s");
    }
    public void MainInit()
    {
        playerProfile.HeartCount = playerProfile.HeartCount;
        SystemLanguage language = Application.systemLanguage;
        if (PlayerPrefs.HasKey("LANGUAGE"))
            language = (SystemLanguage)PlayerPrefs.GetInt("LANGUAGE");

        LocalizationManager.instance.language = language;
        LocalizationManager.instance.ChangeLanguage(language);
        if (UIMainManager.instance != null)
            switch (language)
            {
                case SystemLanguage.Korean:
                    //playerProfile.language = GameLanguage.KR;
                    UIMainManager.instance.korButton.onClick.Invoke();
                    break;
                case SystemLanguage.English:
                    //playerProfile.language = GameLanguage.EN;
                    UIMainManager.instance.engButton.onClick.Invoke();
                    break;
                default:
                    //playerProfile.language = GameLanguage.EN;
                    UIMainManager.instance.engButton.onClick.Invoke();
                    break;
            }


    }
    public bool LoadHeartInfo()
    {
        ////Debug.Log("LoadHeartInfo");
        bool result = false;
        try
        {
            if (PlayerPrefs.HasKey("HeartAmount"))
            {
                ////Debug.Log("PlayerPrefs has key : HeartAmount");
                playerProfile.HeartCount = PlayerPrefs.GetInt("HeartAmount");
                if (playerProfile.HeartCount < 0)
                    playerProfile.HeartCount = 0;
                else if (playerProfile.HeartCount >=MAX_HEART)
                {
                    playerProfile.HeartCount = MAX_HEART;
                }
            }
            else
                playerProfile.HeartCount = 5;
            //heartAmountLabel.text = playerProfile.heartCount.ToString();
            ////Debug.Log("Loaded HeartAmount : " + playerProfile.HeartCount);
            result = true;
        }
        catch (System.Exception e)
        {
            //Debug.LogError("LoadHeartInfo Failed (" + e.Message + ")");
        }
        return result;
    }
    public bool SaveHeartInfo()
    {
        ////Debug.Log("SaveHeartInfo");
        bool result = false;
        try
        {
            playerProfile.HeartCount += heartRewardStack;
            heartRewardStack = 0;
            PlayerPrefs.SetInt("HeartAmount", playerProfile.HeartCount);
            PlayerPrefs.Save();
            ////Debug.Log("Saved HeartAmount : " + playerProfile.HeartCount);
            result = true;
        }
        catch (System.Exception e)
        {
            //Debug.LogError("SaveHeartInfo Failed (" + e.Message + ")");
        }
        return result;
    }
    public bool LoadAppQuitTime()
    {
        //Debug.Log("LoadAppQuitTime");
        bool result = false;
        try
        {
            if (PlayerPrefs.HasKey("AppQuitTime"))
            {
                //Debug.Log("PlayerPrefs has key : AppQuitTime");
                var appQuitTime = string.Empty;
                appQuitTime = PlayerPrefs.GetString("AppQuitTime");
                m_AppQuitTime = DateTime.FromBinary(Convert.ToInt64(appQuitTime));
            }
            else
            {
                //Debug.Log("현재시간 불러옴");
                m_AppQuitTime = DateTime.Now.ToLocalTime();
            }
            //Debug.Log(string.Format("Loaded AppQuitTime : {0}", m_AppQuitTime.ToString()));
            //appQuitTimeLabel.text = string.Format("AppQuitTime : {0}", m_AppQuitTime.ToString());
            result = true;
        }
        catch (System.Exception e)
        {
            //Debug.LogError("LoadAppQuitTime Failed (" + e.Message + ")");
        }
        return result;
    }
    public bool SaveAppQuitTime()
    {
        //Debug.Log("SaveAppQuitTime");
        bool result = false;
        try
        {
            var appQuitTime = DateTime.Now.ToLocalTime().ToBinary().ToString();
            PlayerPrefs.SetString("AppQuitTime", appQuitTime);
            PlayerPrefs.SetInt("RemainTime", playerProfile.remainHeartSupplyTime);
            PlayerPrefs.Save();
            //Debug.Log("Saved AppQuitTime : " + DateTime.Now.ToLocalTime().ToString());
            result = true;
        }
        catch (System.Exception e)
        {
            //Debug.LogError("SaveAppQuitTime Failed (" + e.Message + ")");
        }
        return result;
    }
    public void SetRechargeScheduler(Action onFinish = null)
    {
        if (supplyTimerCoroutine != null)
        {
            StopCoroutine(supplyTimerCoroutine);
        }
        var timeDifferenceInSec = (int)((DateTime.Now.ToLocalTime() - m_AppQuitTime).TotalSeconds);

        //Debug.Log("TimeDifference In Sec :" + timeDifferenceInSec + "s");

        int origintimeDifferenceInSec = timeDifferenceInSec;

        int remainTime = 0;
        int heartToAdd;
        if (timeDifferenceInSec > 0)
        {

            timeDifferenceInSec = PlayerPrefs.GetInt("RemainTime") - timeDifferenceInSec;
            //58

            if (timeDifferenceInSec <= 0)
            {
                playerProfile.HeartCount++;
                timeDifferenceInSec = Mathf.Abs(timeDifferenceInSec);
                heartToAdd = timeDifferenceInSec / HeartSupplyInterval;
                //Debug.Log("Heart to add : " + heartToAdd);
                if (heartToAdd == 0)
                    remainTime = HeartSupplyInterval - timeDifferenceInSec;
                else
                    remainTime = HeartSupplyInterval - (timeDifferenceInSec % HeartSupplyInterval);
            }
            else
            {
                heartToAdd = timeDifferenceInSec / HeartSupplyInterval;
                //Debug.Log("Heart to add : " + heartToAdd);
                if (heartToAdd == 0)
                    remainTime = PlayerPrefs.GetInt("RemainTime") - origintimeDifferenceInSec;
            }
            //Debug.Log("RemainTime : " + remainTime);
            playerProfile.HeartCount += heartToAdd;
        }
        else if (timeDifferenceInSec < 0)
            Debug.Log("선생님? 시간여행자이십니까?");

        if (playerProfile.HeartCount >= MAX_HEART)
        {
            playerProfile.HeartCount = MAX_HEART;
        }
        else
        {
            supplyTimerCoroutine = StartCoroutine(DoRechargeTimer(remainTime, onFinish));
        }
        //heartAmountLabel.text = string.Format("Hearts : {0}", playerProfile.heartCount.ToString());
        ////Debug.Log("HeartAmount : " + playerProfile.HeartCount);
    }
    public void UseHeart(Action onFinish = null)
    {
        if (playerProfile.HeartCount <= 0)
        {
            PopUpBoxManager.instance.rewardADBox.gameObject.SetActive(true);
            return;
        }

        playerProfile.HeartCount--;
        //heartAmountLabel.text = string.Format("Hearts : {0}", playerProfile.heartCount.ToString());
        if (supplyTimerCoroutine == null)
        {
            supplyTimerCoroutine = StartCoroutine(DoRechargeTimer(HeartSupplyInterval));
        }
        if (onFinish != null)
        {
            onFinish();
        }
    }
    private IEnumerator DoRechargeTimer(int remainTime, Action onFinish = null)
    {
        //Debug.Log("DoRechargeTimer");
        if (remainTime <= 0)
        {
            playerProfile.remainHeartSupplyTime = HeartSupplyInterval;
        }
        else
        {
            playerProfile.remainHeartSupplyTime = remainTime;
        }
        //Debug.Log("heartRechargeTimer : " + playerProfile.remainHeartSupplyTime + "s");
        //heartRechargeTimer.text = string.Format("Timer : {0} s",  playerProfile.remainHeartSupplyTime);

        while (playerProfile.remainHeartSupplyTime > 0)
        {
            //heartRechargeTimer.text = string.Format("Timer : {0} s",  playerProfile.remainHeartSupplyTime);
            playerProfile.remainHeartSupplyTime -= 1;
            //Debug.Log("heartRechargeTimer : " + playerProfile.remainHeartSupplyTime + "s");
            yield return new WaitForSecondsRealtime(1f);
        }
        playerProfile.HeartCount++;
        if (playerProfile.HeartCount >= MAX_HEART)
        {
            playerProfile.HeartCount = MAX_HEART;
            playerProfile.remainHeartSupplyTime = 0;
            //heartRechargeTimer.text = string.Format("Timer : {0} s",  playerProfile.remainHeartSupplyTime);
            //Debug.Log("HeartAmount reached max amount");
            supplyTimerCoroutine = null;
        }
        else
        {
            supplyTimerCoroutine = StartCoroutine(DoRechargeTimer(HeartSupplyInterval, onFinish));
        }
        //heartAmountLabel.text = string.Format("Hearts : {0}", playerProfile.heartCount.ToString());
        //Debug.Log("HeartAmount : " + playerProfile.HeartCount);
    }

    public void SaveClearData(string stageName, float clearTime, int score, int eggCount)
    {
        string path = Application.persistentDataPath + "/SaveData/ClearData/" + stageName + ".json";
        ClearDataInfo clearData = new ClearDataInfo(stageName, true, clearTime, score, eggCount);
        string dataJsonString = JsonMapper.ToJson(clearData);
        dataJsonString = MapDataManager.Encrypt(dataJsonString, saveKey);

        //if (File.Exists(path))
        //{
        //    return;
        //}

        File.WriteAllText(path, dataJsonString);
    }
    public ClearDataInfo LoadClearData(string stageName)
    {
        string path = Application.persistentDataPath + "/SaveData/ClearData/" + stageName + ".json";
        string jsonString = "";
        try
        {
            jsonString = MapDataManager.Decrypt(File.ReadAllText(path), saveKey);
        }
        catch (System.Security.Cryptography.CryptographicException e)
        {
            return null;
        }
        JSONObject clearDataJson = new JSONObject(jsonString);
        ClearDataInfo resultData = new ClearDataInfo(stageName, clearDataJson["isClear"].b, clearDataJson["clearTIme"].f, (int)clearDataJson["score"].i, (int)clearDataJson["eggCount"].i);
        return resultData;
    }
    public void StartGame(StageComponent seletedStage)
    {
        DataManager.instance.UseHeart(() => LoadGame(seletedStage));

    }
    public void StartGame(string stageName)
    {
        GameManager.instance.Resume();
        DataManager.instance.UseHeart(() => LoadGame(stageName));

    }
    void LoadGame(string stageName)
    {
        var package = Instantiate(sendPackage).GetComponent<GameInfoPackage>();
        package.stageName = stageName;
        SceneTransformManager.instance.MoveToScene("GameBase");
    }
    void LoadGame(StageComponent seletedStage)
    {
        var package = Instantiate(sendPackage).GetComponent<GameInfoPackage>();
        package.stageName = seletedStage.clearData.name;
        SceneTransformManager.instance.MoveToScene("GameBase");
    }
    public void GiveRewardDirection(Sprite icon, int amount)
    {
        giveRewardCoroutine.Enqueue(GiveRewardDirectionCoroutine(icon, amount));
    }
    IEnumerator GiveRewardDirectionCoroutine(Sprite icon, int amount)
    {
        SoundManager.instance.audioSourceSFX.PlayOneShot(SoundManager.instance.rewardSound);
        RewardDirection.SetActive(true);
        RewardIcon.sprite = icon;
        RewardText.text = amount.ToString();
        RewardDirection.transform.DOScale(new Vector2(1, 1), 0.5f);
        yield return new WaitForSeconds(2);
        RewardDirection.transform.DOScale(new Vector2(0, 0), 0.5f);
        yield return new WaitForSeconds(0.5f);
        RewardDirection.SetActive(false);
        currentGiveRewardCoroutine = null;
    }
    public void DeleteHeart()
    {
        playerProfile.HeartCount = 0;
        playerProfile.remainHeartSupplyTime = HeartSupplyInterval;
        PlayerPrefs.SetInt("HeartAmount", playerProfile.HeartCount);
        PlayerPrefs.Save();
        if (supplyTimerCoroutine == null)
            supplyTimerCoroutine = StartCoroutine(DoRechargeTimer(HeartSupplyInterval));
    }
    public void SetQuality(int i)
    {
        QualitySettings.SetQualityLevel(i, true);
    }
}
