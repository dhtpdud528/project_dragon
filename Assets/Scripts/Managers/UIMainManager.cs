﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;

public class UIMainManager : MonoBehaviour
{
    public static UIMainManager instance;
    public Text heartSupplyTimeText;
    public Text heartOverCountText;
    public Transform heartImages;
    public Image soundIcon;
    public Image bgmIcon;
    public Button korButton;
    public Button engButton;

    public Sprite spriteEnableEgg;
    int eggsCount;
    public int EggsCount
    {
        get
        {
            return eggsCount;
        }
        set
        {
            eggsCount = value;
            for(int i=0; i < textEggsCount.Length;i++)
                textEggsCount[i].text = eggsCount.ToString();
        }
    }
    public Text[] textEggsCount;
    public GameObject iceEggCountObjs;
    public GameObject lvDesignerEggCountObjs;
    public RectTransform layoutForest;
    public RectTransform layoutIce;

    public List<Sprite> soundIconSprites = new List<Sprite>();
    public List<Sprite> bgmIconSprites = new List<Sprite>();

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Debug.LogWarning("Scene에 2개 이상의 UI 매니저가 존재합니다!");
            Destroy(gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        DataManager.instance.MainInit();
        // StartCoroutine(MainInit());
        if (SoundManager.instance.audioSourceBGM.clip != SoundManager.instance.mainBGM)
        {
            SoundManager.instance.audioSourceBGM.clip = SoundManager.instance.mainBGM;
            SoundManager.instance.audioSourceBGM.Play();
        }
    }
    public void LayoutInit()
    {
        layoutForest.DOMoveY(0, 0.1f);
        layoutIce.DOMoveY(0, 0.1f);
    }
    IEnumerator MainInit()
    {
        yield return new WaitForEndOfFrame();
        DataManager.instance.MainInit();
    }

    // Update is called once per frame
    void Update()
    {
        //heartSupplyTimeText.text = DataManager.instance.playerProfile.remainHeartSupplyTime.ToString();
        int totalTime = DataManager.instance.playerProfile.remainHeartSupplyTime;
        int minTime = totalTime / 60;
        int secTime = totalTime - minTime * 60;
        if (totalTime >= 60)
            heartSupplyTimeText.text = string.Format("{0} : {1}", minTime.ToString("D2"), secTime.ToString("D2"));
        else
            heartSupplyTimeText.text = string.Format("{0}", secTime.ToString("D2"));
    }
    public void ClickButtonSound()
    {
        DataManager.instance.playerProfile.isSound_ON = !DataManager.instance.playerProfile.isSound_ON;
        switch (DataManager.instance.playerProfile.isSound_ON)
        {
            case true: soundIcon.sprite = soundIconSprites.Find((val) => val.name.Contains("on")); break;
            case false: soundIcon.sprite = soundIconSprites.Find((val) => val.name.Contains("off")); break;
        }
    }
    public void ClickButtonBGM()
    {
        DataManager.instance.playerProfile.isBGM_ON = !DataManager.instance.playerProfile.isBGM_ON;
        switch (DataManager.instance.playerProfile.isBGM_ON)
        {
            case true: bgmIcon.sprite = bgmIconSprites.Find((val) => val.name.Contains("on")); break;
            case false: bgmIcon.sprite = bgmIconSprites.Find((val) => val.name.Contains("off")); break;
        }
    }
}
