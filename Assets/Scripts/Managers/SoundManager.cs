﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour
{
    public static SoundManager instance;
    public AudioSource audioSourceSFX;
    public AudioSource audioSourceBGM;

    [Header("- 오디오 UI")]
    public AudioClip tileBGM;
    public AudioClip mainBGM;
    public AudioClip lvDesignerBGM;
    public AudioClip gameBGM;

    public AudioClip clickSound;
    public AudioClip cardOnSound;
    public AudioClip cardOffSound;
    public AudioClip rewardSound;
    public AudioClip onSound;
    public AudioClip offSound;
    public AudioClip winSound;
    public AudioClip loseSound;

    public AudioClip buyNoADsSound;
    public AudioClip failedPay;

    [Header("- 오디오 몬스터")]
    public AudioClip zombieDieSound;
    public AudioClip orgeDieSound;
    public AudioClip slimeDieSound;
    public AudioClip wolfDieSound;
    public AudioClip golemDieSound;

    public AudioClip deadHoleSound;
    public AudioClip boundSound;

    public AudioClip[] allysClickMeSound;

    [Header("- 오디오 드래곤")]
    public AudioClip breathSound;
    public AudioClip transformSound;
    public AudioClip explosionNormalSound;
    public AudioClip explosionFireSound;
    public AudioClip explosionIceSound;
    public AudioClip explosionLightningSound;
    public AudioClip explosionWindSound;

    [Header("- 오디오 블록")]
    public AudioClip eggHit;
    public AudioClip LogHit;
    public AudioClip eggBreak;
    public AudioClip LogBreak;

    public AudioClip useItem;

    [Header("- 환경 설정")]
    public Image buttonSoundIcon;
    public List<Sprite> buttonSoundSprites = new List<Sprite>();
    bool isSoundOn = true;
    public bool IsSoundOn
    {
        get
        {
            return isSoundOn;
        }
        set
        {
            isSoundOn = value;
            audioSourceSFX.mute = !isSoundOn;
            switch (isSoundOn)
            {
                case true: buttonSoundIcon.sprite = buttonSoundSprites.Find((val) => val.name.Contains("on")); break;
                case false: buttonSoundIcon.sprite = buttonSoundSprites.Find((val) => val.name.Contains("off")); break;
            }
        }
    }

    public Image buttonBGMIcon;
    public List<Sprite> buttonBGMSprites = new List<Sprite>();
    bool isBGMOn = true;
    

    public bool IsBGMOn
    {
        get
        {
            return isBGMOn;
        }
        set
        {
            isBGMOn = value;
            audioSourceBGM.mute = !isBGMOn;
            switch (isBGMOn)
            {
                case true: buttonBGMIcon.sprite = buttonBGMSprites.Find((val) => val.name.Contains("on")); break;
                case false: buttonBGMIcon.sprite = buttonBGMSprites.Find((val) => val.name.Contains("off")); break;
            }
        }
    }

    // Start is called before the first frame update
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Debug.LogWarning("Scene에 2개 이상의 사운드 매니저가 존재합니다!");
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void IconUpdate(Image buttonImageSFX, Image buttonImageBGM)
    {
        buttonSoundIcon = buttonImageSFX;
        buttonBGMIcon = buttonImageBGM;
        IsSoundOn = IsSoundOn;
        IsBGMOn = IsBGMOn;
    }
    public void SwitchSound(Image buttonImageSFX)
    {
        buttonSoundIcon = buttonImageSFX;
        IsSoundOn = !IsSoundOn;
    }
    public void SwitchBGM(Image buttonImageBGM)
    {
        buttonBGMIcon = buttonImageBGM;
        IsBGMOn = !IsBGMOn;
    }

    public void PlayOneShotRandomAudio_BGM(AudioClip[] audios)
    {
        // pick & play a random footstep sound from the array,
        // excluding sound at index 0
        if (audios == null) return;
        int n = UnityEngine.Random.Range(1, audios.Length);
        if (audios[n] == null) return;
        audioSourceBGM.clip = audios[n];
        audioSourceBGM.PlayOneShot(audioSourceBGM.clip);
        // move picked sound to index 0 so it's not picked next time
        audios[n] = audios[0];
        audios[0] = audioSourceBGM.clip;
    }
    public void PlayRandomAudio_BGM(AudioClip[] audios)
    {
        // pick & play a random footstep sound from the array,
        // excluding sound at index 0
        if (audios == null) return;
        int n = UnityEngine.Random.Range(1, audios.Length);
        if (audios[n] == null) return;
        audioSourceBGM.clip = audios[n];
        audioSourceBGM.Play();
        // move picked sound to index 0 so it's not picked next time
        audios[n] = audios[0];
        audios[0] = audioSourceBGM.clip;
    }
    public void PlayRandomAudio_SFX(AudioClip[] audios)
    {
        // pick & play a random footstep sound from the array,
        // excluding sound at index 0
        if (audios == null) return;
        int n = UnityEngine.Random.Range(1, audios.Length);
        if (audios[n] == null) return;
        audioSourceSFX.clip = audios[n];
        audioSourceSFX.Play();
        // move picked sound to index 0 so it's not picked next time
        audios[n] = audios[0];
        audios[0] = audioSourceSFX.clip;
    }
    public void PlayOneShotRandomAudio_SFX(AudioClip[] audios)
    {
        // pick & play a random footstep sound from the array,
        // excluding sound at index 0
        if (audios == null) return;
        int n = UnityEngine.Random.Range(1, audios.Length);
        if (audios[n] == null) return;
        audioSourceSFX.clip = audios[n];
        audioSourceSFX.PlayOneShot(audioSourceSFX.clip);
        // move picked sound to index 0 so it's not picked next time
        audios[n] = audios[0];
        audios[0] = audioSourceSFX.clip;
    }

    public void PlayOneShot_SFX(AudioClip audioClip)
    {
        if (audioClip == null) return;
        audioSourceSFX.PlayOneShot(audioClip);
    }

}
