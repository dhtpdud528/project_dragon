﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameToolkit.Localization;

public class LocalizationManager : MonoBehaviour
{
    public static LocalizationManager instance;
    public SystemLanguage language;

    private Message[] messages;
    public struct Message
    {
        public string stringKR;
        public string stringEN;
        public Message(string stringKR, string stringEN)
        {
            this.stringKR = stringKR;
            this.stringEN = stringEN;
        }
    }

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Debug.LogWarning("Scene에 2개 이상의 지역화 매니저가 존재합니다!");
            Destroy(gameObject);
        }
        messages = new Message[9];
        messages[0] = new Message("알은 3개이상 배치 할 수 없습니다!", "You cannot place more than three eggs!");
        messages[1] = new Message("저장 되었습니다!", "Saved successfully!");
        messages[2] = new Message("레벨 파일", "Level file");
        messages[3] = new Message("레벨 저장", "Save Level");
        messages[4] = new Message("저장", "Save");
        messages[5] = new Message("레벨 불러오기", "Load Level");
        messages[6] = new Message("불러오기", "Load");
        messages[7] = new Message("선택된 객체", "Selected Content");
        messages[8] = new Message("시간", "Time");
    }
    public string GetMessage(int i)
    {
        if (language == SystemLanguage.Korean)
            return messages[i].stringKR;
        else
            return messages[i].stringEN;
    }
    public void ChangeLanguage(SystemLanguage language)
    {
        this.language = Localization.Instance.CurrentLanguage = LocalizationSettings.Instance.AvailableLanguages.Find((val)=>val.Equals(language));
        PlayerPrefs.SetInt("LANGUAGE", (int)language);
    }
}