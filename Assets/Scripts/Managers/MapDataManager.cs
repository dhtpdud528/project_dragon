using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using LitJson;
using System;
using System.IO;
using UnityEngine.Events;
using SimpleFileBrowser;
using System.Text;
using System.Security.Cryptography;

[Serializable]
public class BlockTransform
{
    public double positionX;
    public double positionY;
    public double scaleX;
    public double scaleY;

    public BlockTransform(double positionX, double positionY, double scaleX, double scaleY)
    {
        this.positionX = positionX;
        this.positionY = positionY;
        this.scaleX = scaleX;
        this.scaleY = scaleY;
    }
    public BlockTransform(Transform transform)
    {
        this.positionX = transform.position.x;
        this.positionY = transform.position.y;
        this.scaleX = transform.localScale.x;
        this.scaleY = transform.localScale.y;
    }
}

[Serializable]
public class BlockInfo
{
    public string name;
    public int id;
    public BlockTransform transform;

    public BlockInfo(string name,int id, double positionX, double positionY, double scaleX, double scaleY)
    {
        this.name = name;
        this.id = id;
        transform = new BlockTransform(positionX, positionY, scaleX, scaleY);
    }
}
[Serializable]
public class SpawnerInfo : BlockInfo
{
    [Serializable]
    public class Wave
    {
        public string name;
        public List<EventMessage> onWaveStart = new List<EventMessage>();
        public List<string> spawnTargetNames = new List<string>();
        public int spawnCount;
        public double rate;
        public double timeBetweenWaves;
        public List<EventMessage> onWaveEnd = new List<EventMessage>();

        public Wave(string name, List<EventMessage> onWaveStart, List<GameObject> spawnTagets, int spawnCount, float rate, float timeBetweenWaves, List<EventMessage> onWaveEnd)
        {
            this.name = name;
            if (onWaveStart != null)
                this.onWaveStart = onWaveStart;
            for (int i = 0; i < spawnTagets.Count; i++)
                this.spawnTargetNames.Add(spawnTagets[i].GetComponent<IName>().Name);
            this.spawnCount = spawnCount;
            this.rate = rate;
            this.timeBetweenWaves = timeBetweenWaves;
            if (onWaveEnd != null)
                this.onWaveEnd = onWaveEnd;
        }
    }
    public Wave[] waves;

    public SpawnerInfo(string name, int id, float positionX, float positionY, double scaleX, double scaleY, Block_Spawner.Wave[] waves) : base(name, id, positionX, positionY, scaleX, scaleY)
    {
        this.waves = new Wave[waves.Length];
        for (int i = 0; i < waves.Length; i++)
            this.waves[i] = new Wave(waves[i].name, waves[i].onWaveStart, waves[i].spawnTargetsOrigin, waves[i].spawnCount, waves[i].rate, waves[i].timeBetweenWaves, waves[i].onWaveEnd);

    }
}
[Serializable]
public class RainbowInfo : SpawnerInfo
{
    public BlockTransform endRainbowTransform;

    public RainbowInfo(string name, int id, float positionX, float positionY, double scaleX, double scaleY, Block_Spawner.Wave[] waves, BlockTransform endRainbowTransform) : base(name, id, positionX, positionY, scaleX, scaleY, waves)
    {
        this.endRainbowTransform = endRainbowTransform;
    }
}
[Serializable]
public class ClearDataInfo
{
    public string name;
    public bool isClear;
    public double clearTIme;
    public int score;
    public int eggCount;

    public ClearDataInfo(string name, bool isClear, double clearTIme, int score, int eggCount)
    {
        this.name = name;
        this.isClear = isClear;
        this.clearTIme = clearTIme;
        this.score = score;
        this.eggCount = eggCount;
    }
}
[Serializable]
public class StartSupplyInfo
{
    public int fireCount;
    public int iceCount;
    public int lightningCount;
    public int windCount;

    public StartSupplyInfo(int fireCount, int iceCount, int lightningCount, int windCount)
    {
        this.fireCount = fireCount;
        this.iceCount = iceCount;
        this.lightningCount = lightningCount;
        this.windCount = windCount;
    }
}
[Serializable]
public class MapDataInfo
{
    public int stageTheme;
    public StartSupplyInfo startSupplyInfo;
    public List<BlockInfo> blockInfos = new List<BlockInfo>();
    public List<BaseProfile> balanceProfiles = new List<BaseProfile>();
    public MapDataInfo()
    {
        startSupplyInfo = new StartSupplyInfo(0,0,0,0);
        stageTheme = (int)StageTheme.Forest;
    }
}


public class MapDataManager : MonoBehaviour
{
    public static MapDataManager instance;
    bool isEncrypt;
    public bool IsEncrypt
    {
        get
        {
            return isEncrypt;
        }
        set
        {
            isEncrypt = value;
        }
    }
    
    public string path;
    private string mapFileName;
    public string MapKey = "key";

    public Sprite[] backgrounds;
    public List<GameObject> spawnedBlocks = new List<GameObject>();
    public Tilemap target;
    public MapDataInfo mapDataInfo;

    public bool doSave;
    public IEnumerator saveCoroutine;

    //public List<ClearDataInfo> clearDataInfos = new List<ClearDataInfo>();


    //public List<BlockInfo> blockInfos = new List<BlockInfo>();
    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Debug.LogWarning("Scene에 2개 이상의 파일 매니저가 존재합니다!");
            Destroy(gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        this.path = null;
        if (MapDesigner.instance != null)
        {
            if (FileBrowser.CheckPermission() != FileBrowser.Permission.Granted)
                FileBrowser.RequestPermission();
            StartCoroutine(InitFileBrowser());
        }
    }
    IEnumerator InitFileBrowser()
    {
        yield return new WaitUntil(()=> FileBrowser.CheckPermission() == FileBrowser.Permission.Granted);
        //Debug.Log(FileBrowserHelpers.GetEntriesInDirectory(Application.persistentDataPath + "/SaveData/MapDesigner/")[0].Path);
        FileBrowser.AddQuickLink("MapsFolder", Application.persistentDataPath + "/SaveData/MapDesigner/");
        FileBrowser.SetFilters(true, new FileBrowser.Filter(LocalizationManager.instance.GetMessage(2)+"(.json)", ".json"));
        FileBrowser.SetDefaultFilter(".json");
        //FileBrowser.AskPermissions = true;
    }
    // Update is called once per frame
    void Update()
    {

    }
    public void ClearStage()
    {
        int tempCount;

        List<AI_Moveable> monsters = new List<AI_Moveable>(FindObjectsOfType<AI_Moveable>());
        if(monsters.Count > 0)
        {
            tempCount = monsters.Count;
            for (int i = 0; i < tempCount; i++)
                Destroy(monsters[i].gameObject);
        }
        
        tempCount = target.transform.childCount;
        for (int i = 0; i < tempCount; i++)
            DestroyImmediate(target.transform.GetChild(0).gameObject);

        List<EntitySkillStone> skills = new List<EntitySkillStone>(FindObjectsOfType<EntitySkillStone>());
        if(skills.Count > 0)
        {
            tempCount = skills.Count;
            for (int i = 0; i < tempCount; i++)
                Destroy(skills[i].gameObject);
        }

        List<ParticleSystem> particles = new List<ParticleSystem>(FindObjectsOfType<ParticleSystem>());
        if (particles.Count > 0)
        {
            tempCount = particles.Count;
            for (int i = 0; i < tempCount; i++)
                Destroy(particles[i].gameObject);
        }

        GameManager.instance.monsters.Clear();
        GameManager.instance.spawners.Clear();
        GameManager.instance.eggs.Clear();
        GameManager.instance.ids.Clear();
    }

    public void NewMapSave()
    {
        this.path = null;
        Save(false);
    }
    public void Save(bool isTestPlay)
    {
        Debug.Log("게임 저장중");
        mapDataInfo = new MapDataInfo();
        mapDataInfo.stageTheme = (int)GameManager.instance.m_StageTheme;
        mapDataInfo.startSupplyInfo = GameManager.instance.startSupply;
        mapDataInfo.balanceProfiles = ObjectPactory.instance.blanaceProfiles;
        spawnedBlocks.Clear();
        for (int i = 0; i < target.transform.childCount; i++)
        {
            Transform block;
            if (target.transform.GetChild(i).gameObject.activeSelf && target.transform.GetChild(i).GetComponent<IName>() != null)
            {
                block = target.transform.GetChild(i);
                string name = block.GetComponent<IName>().Name;
                if (block.GetComponent<Block_Rainbow>())
                {
                    RainbowInfo rainbow
                        = new RainbowInfo(name, block.GetComponent<IName>().ID, block.position.x, block.position.y, block.localScale.x, block.localScale.y, block.GetComponent<Block_Spawner>().waves.ToArray(),
                        new BlockTransform(block.GetComponent<Block_Rainbow>().EndRainbow.transform));
                    mapDataInfo.blockInfos.Add(rainbow);
                }
                else if (block.GetComponent<Block_Spawner>())
                {
                    SpawnerInfo spawner = new SpawnerInfo(name, block.GetComponent<IName>().ID, block.position.x, block.position.y, block.localScale.x, block.localScale.y, block.GetComponent<Block_Spawner>().waves.ToArray());

                    for (int test = 0; test < spawner.waves.Length; test++)
                    {
                        for (int test2 = 0; test2 < spawner.waves[test].onWaveEnd.Count; test2++)
                        {
                            spawner.waves[test].onWaveEnd[test2].target = null;
                            Debug.Log(spawner.waves[test].onWaveEnd[test2].message + ", " + spawner.waves[test].onWaveEnd[test2].target);
                        }
                        for (int test2 = 0; test2 < spawner.waves[test].onWaveStart.Count; test2++)
                        {
                            spawner.waves[test].onWaveStart[test2].target = null;
                            Debug.Log(spawner.waves[test].onWaveStart[test2].message + ", " + spawner.waves[test].onWaveStart[test2].target);
                        }
                    }
                    mapDataInfo.blockInfos.Add(spawner);
                }
                else
                    mapDataInfo.blockInfos.Add(new BlockInfo(name, block.GetComponent<IName>().ID, block.position.x, block.position.y, block.localScale.x, block.localScale.y));
            }
        }
        string MapJsonString = "";
        //for (int i = 0; i < mapDataInfo.blockInfos.Count; i++)
        //{
        //    if (MapJsonString != "")
        //        MapJsonString += ",";
        //    MapJsonString += JsonUtility.ToJson(mapDataInfo.blockInfos[i]);
        //}
        //MapJsonString = "["+MapJsonString+"]";
        Debug.Log("json 패킹준비");
        MapJsonString = JsonMapper.ToJson(mapDataInfo);
        Debug.Log("json 패킹완료");

        if (isTestPlay)
            this.path = Application.persistentDataPath + "/BackupData/MapDesigner/Backup.json";
        if (this.path == null)
        {
            if (saveCoroutine != null)
                StopCoroutine(saveCoroutine);
            StartCoroutine(saveCoroutine = ShowSaveDialogCoroutine(MapJsonString));
            return;
        }
        if (IsEncrypt)
            MapJsonString = Encrypt(MapJsonString, MapKey);
        Debug.Log("암호화 완료");
        File.WriteAllText(this.path, MapJsonString);
        Debug.Log("저장성공");
        //mapDataInfo.blockInfos.Clear();
    }
    IEnumerator ShowSaveDialogCoroutine(string MapJsonString)
    {
        // Show a load file dialog and wait for a response from user
        // Load file/folder: file, Initial path: default (Documents), Title: "Load File", submit button text: "Load"


        yield return FileBrowser.WaitForSaveDialog(false, false, Application.persistentDataPath + "/SaveData/MapDesigner/", LocalizationManager.instance.GetMessage(3), LocalizationManager.instance.GetMessage(4));

        // Dialog is closed
        // Print whether a file is chosen (FileBrowser.Success)
        // and the path to the selected file (FileBrowser.Result) (null, if FileBrowser.Success is false)
        //Debug.Log(FileBrowser.Success + " " + FileBrowser.Result);

        if (FileBrowser.Success)
        {
            //임시 저장
            this.path = Application.persistentDataPath + "/BackupData/MapDesigner/Backup.json";
            File.WriteAllText(this.path, MapJsonString);

            // If a file was chosen, read its bytes via FileBrowserHelpers
            // Contrary to File.ReadAllBytes, this function works on Android 10+, as well
            //byte[] bytes = FileBrowserHelpers.ReadBytesFromFile(FileBrowser.Result);
            if (IsEncrypt)
                MapJsonString = Encrypt(MapJsonString, MapKey);
            if (Application.platform == RuntimePlatform.WindowsEditor || MyGoogleAdsMobs.instance.isNoAds)
                doSave = true;
            else
                MapDesigner.instance.adPopupSavemap.SetActive(true);
                //MyGoogleAdsMobs.instance.OnViewSaveMapAdClicked();

            
            //↓↓↓↓↓정식 빌드시 지울것!!
            //doSave = true;
            //↑↑↑↑↑

            yield return new WaitUntil(() => doSave);
            this.path = FileBrowser.Result[0];
            doSave = false;
            FileBrowserHelpers.WriteTextToFile(this.path, MapJsonString);
            //File.WriteAllText(this.path, MapJsonString);
            //mapDataInfo.blockInfos.Clear();
        }
        saveCoroutine = null;
    }
    public void NewMapLoad()
    {
        mapDataInfo.blockInfos.Clear();
        spawnedBlocks.Clear();
        StartCoroutine(ShowLoadDialogCoroutine());
    }
    public void SearchAndLoad()
    {
        //Application.dataPath + "/Resources/mapData.json"
        mapDataInfo.blockInfos.Clear();
        spawnedBlocks.Clear();
        if (this.path == null)
        {
            StartCoroutine(ShowLoadDialogCoroutine());
            return;
        }
        ClearStage();
        Debug.Log(this.path);
        LoadFromFilePath(this.path);
    }
    public void LoadFromResources(string path)
    {
        var textData = Resources.Load(path) as TextAsset;
        Load(textData.ToString());
    }
    public void LoadFromFilePath(string path)
    {
        string jsonString = "";
        if (path.Contains("content://"))
            jsonString = FileBrowserHelpers.ReadTextFromFile(path);
        else
            jsonString = File.ReadAllText(path);
        Load(jsonString);
    }
    public void Load(string jsonString)
    {
        Debug.Log("불러오는 중");
        JsonData mapDataLitJson;
        try
        {
            mapDataLitJson = JsonMapper.ToObject(jsonString);
        }
        catch (JsonException e)
        {
            jsonString = Decrypt(jsonString, MapKey);
            mapDataLitJson = JsonMapper.ToObject(jsonString);
        }
        Debug.Log("해독 완료");
        JSONObject mapData = new JSONObject(jsonString);


        GameManager.instance.m_StageTheme = (StageTheme)mapData["stageTheme"].i;
        if (MapDesigner.instance != null)
            MapDesigner.instance.dropdownTheme.value = (int)GameManager.instance.m_StageTheme;

        GameManager.instance.startSupply = new StartSupplyInfo(
            (int)mapData["startSupplyInfo"]["fireCount"].i,
            (int)mapData["startSupplyInfo"]["iceCount"].i,
            (int)mapData["startSupplyInfo"]["lightningCount"].i,
            (int)mapData["startSupplyInfo"]["windCount"].i);

        for (int i = 0; i < mapData["blockInfos"].Count; i++)
        {
            GameObject block;
            string name = mapData["blockInfos"][i]["name"].str;
            if (block = FindBlock(name))
            {
                block = Instantiate(block, new Vector2(mapData["blockInfos"][i]["transform"]["positionX"].f, mapData["blockInfos"][i]["transform"]["positionY"].f), Quaternion.identity);
                block.transform.localScale = new Vector2(mapData["blockInfos"][i]["transform"]["scaleX"].f, mapData["blockInfos"][i]["transform"]["scaleY"].f);
                block.transform.SetParent(target.transform);
                block.GetComponent<IName>().ID = (int)mapData["blockInfos"][i]["id"].i;
                block.GetComponent<IName>().Name = name;

                if (block.GetComponent<IName>().Name == "Rainbow")
                {
                    block.GetComponent<Block_Rainbow>().EndRainbow.transform.position = new Vector2(mapData["blockInfos"][i]["endRainbowTransform"]["positionX"].f, mapData["blockInfos"][i]["endRainbowTransform"]["positionY"].f);
                    block.GetComponent<Block_Rainbow>().EndRainbow.transform.localScale = new Vector2(mapData["blockInfos"][i]["endRainbowTransform"]["scaleX"].f, mapData["blockInfos"][i]["endRainbowTransform"]["scaleY"].f);
                }

                spawnedBlocks.Add(block);
            }
            //block.GetComponent<IName>().ID = (int) mapData["blockInfos"][i]["id"].i;
        }
        for (int i = 0; i < spawnedBlocks.Count; i++)
        {
            if (spawnedBlocks[i].GetComponent<IName>().Name.Contains("Spawner") || spawnedBlocks[i].GetComponent<IName>().Name == "Rainbow")
                SpawnerInit(spawnedBlocks[i], mapData, mapDataLitJson, i);
        }
        GameManager.instance.AstarScan();
        if (MapDesigner.instance != null)
            GameManager.instance.stageNameText.text = this.mapFileName;

        ObjectPactory.instance.Load(mapData["balanceProfiles"]);
        //불러오기
    }
    IEnumerator ShowLoadDialogCoroutine()
    {
        // Show a load file dialog and wait for a response from user
        // Load file/folder: file, Initial path: default (Documents), Title: "Load File", submit button text: "Load"
        yield return FileBrowser.WaitForLoadDialog(false, false, Application.persistentDataPath + "/SaveData/MapDesigner/", LocalizationManager.instance.GetMessage(5), LocalizationManager.instance.GetMessage(6));

        // Dialog is closed
        // Print whether a file is chosen (FileBrowser.Success)
        // and the path to the selected file (FileBrowser.Result) (null, if FileBrowser.Success is false)
        //Debug.Log(FileBrowser.Success + " " + FileBrowser.Result);

        if (FileBrowser.Success)
        {
            // If a file was chosen, read its bytes via FileBrowserHelpers
            // Contrary to File.ReadAllBytes, this function works on Android 10+, as well
            this.path = FileBrowser.Result[0];
            this.mapFileName = Path.GetFileNameWithoutExtension(this.path);
            SearchAndLoad();
        }
    }
    public void SpawnerInit(GameObject blockSpawner, JSONObject mapData, JsonData mapDataLitJson, int i)
    {
        SpawnerInfo.Wave[] loadedWaves = new SpawnerInfo.Wave[mapData["blockInfos"][i]["waves"].Count];

        for (int waveIndex = 0; waveIndex < loadedWaves.Length; waveIndex++)
        {
            //Object를 GameObject로 바꾸는 방법: 두 id값이 같을 경우, 아래에서 json파싱할 때 instance id값을 대입한다.
            List<EventMessage> tempOnWaveStart = new List<EventMessage>();
            List<EventMessage> tempOnWaveEnd = new List<EventMessage>();

            for (int eventIndex = 0; eventIndex <  mapData["blockInfos"][i]["waves"][waveIndex]["onWaveStart"].Count; eventIndex++)
            {
                tempOnWaveStart.Add(
                    new EventMessage((int) mapData["blockInfos"][i]["waves"][waveIndex]["onWaveStart"][eventIndex]["targetID"].i,  mapData["blockInfos"][i]["waves"][waveIndex]["onWaveStart"][eventIndex]["message"].str));
            }
            
            for (int eventIndex = 0; eventIndex <  mapData["blockInfos"][i]["waves"][waveIndex]["onWaveEnd"].Count; eventIndex++)
                tempOnWaveEnd.Add(
                    new EventMessage((int) mapData["blockInfos"][i]["waves"][waveIndex]["onWaveEnd"][eventIndex]["targetID"].i,  mapData["blockInfos"][i]["waves"][waveIndex]["onWaveEnd"][eventIndex]["message"].str));

            var targetNames = new List<string>();
            for (int tartgetIndex = 0; tartgetIndex <  mapData["blockInfos"][i]["waves"][waveIndex]["spawnTargetNames"].Count; tartgetIndex++)
                targetNames.Add( mapData["blockInfos"][i]["waves"][waveIndex]["spawnTargetNames"][tartgetIndex].str);

            loadedWaves[waveIndex] = new SpawnerInfo.Wave(
                 mapDataLitJson["blockInfos"][i]["waves"][waveIndex]["name"].ToString(),
                tempOnWaveStart,
                FindMonsters(targetNames),
                (int) mapData["blockInfos"][i]["waves"][waveIndex]["spawnCount"].i,
                 mapData["blockInfos"][i]["waves"][waveIndex]["rate"].f,
                 mapData["blockInfos"][i]["waves"][waveIndex]["timeBetweenWaves"].f,
                tempOnWaveEnd);
        }
        blockSpawner.GetComponent<Block_Spawner>().LoadWaves(loadedWaves);
    }
    public GameObject FindSpawnedBlock(int id)
    {
        GameObject obj = null;
        for (int i = 0; i < spawnedBlocks.Count; i++)
        {
            if (spawnedBlocks[i].GetComponent<IName>().ID == id)
            {
                obj = spawnedBlocks[i];
                Debug.Log(spawnedBlocks[i].GetComponent<IName>().Name + ": " + spawnedBlocks[i].GetComponent<IName>().ID);
                break;
            }
        }
        return obj;
    }
    public GameObject FindBlock(string name)
    {
        GameObject obj = null;
        for(int i =0; i < ObjectPactory.instance.blocks.Length;i++)
        {
            if (ObjectPactory.instance.blocks[i].GetComponent<IName>().Name == name)
            {
                obj = ObjectPactory.instance.blocks[i];
                break;
            }
        }
        return obj;
    }
    public GameObject FindMonster(string name)
    {
        GameObject obj = null;
        for (int i = 0; i < ObjectPactory.instance.monsters.Length; i++)
        {
            if (ObjectPactory.instance.monsters[i].GetComponent<IName>().Name == name)
            {
                obj = ObjectPactory.instance.monsters[i];
                break;
            }
        }
        return obj;
    }
    public List<GameObject> FindMonsters(List<string> names)
    {
        List<GameObject> objs = new List<GameObject>();

        for (int a = 0; a < names.Count; a++)
            for (int i = 0; i < ObjectPactory.instance.monsters.Length; i++)
                if (ObjectPactory.instance.monsters[i].GetComponent<IName>().Name == names[a])
                    objs.Add(ObjectPactory.instance.monsters[i]);
        return objs;
    }

    public static string Decrypt(string textToDecrypt, string key)
    {//복호화 메소드
        RijndaelManaged rijndaelCipher = new RijndaelManaged();
        rijndaelCipher.Mode = CipherMode.CBC;
        rijndaelCipher.Padding = PaddingMode.PKCS7;


        rijndaelCipher.KeySize = 128;
        rijndaelCipher.BlockSize = 128;
        byte[] encryptedData = Convert.FromBase64String(textToDecrypt);
        byte[] pwdBytes = Encoding.UTF8.GetBytes(key);
        byte[] keyBytes = new byte[16];
        int len = pwdBytes.Length;
        if (len > keyBytes.Length)
        {
            len = keyBytes.Length;
        }

        Array.Copy(pwdBytes, keyBytes, len);
        rijndaelCipher.Key = keyBytes;
        rijndaelCipher.IV = keyBytes;
        byte[] plainText = rijndaelCipher.CreateDecryptor().TransformFinalBlock(encryptedData, 0, encryptedData.Length);
        return Encoding.UTF8.GetString(plainText);
    }



    public static string Encrypt(string textToEncrypt, string key)
    {//암호화 메소드
        RijndaelManaged rijndaelCipher = new RijndaelManaged();
        rijndaelCipher.Mode = CipherMode.CBC;
        rijndaelCipher.Padding = PaddingMode.PKCS7;

        rijndaelCipher.KeySize = 128;
        rijndaelCipher.BlockSize = 128;
        byte[] pwdBytes = Encoding.UTF8.GetBytes(key);
        byte[] keyBytes = new byte[16];
        int len = pwdBytes.Length;
        if (len > keyBytes.Length)
        {
            len = keyBytes.Length;
        }
        Array.Copy(pwdBytes, keyBytes, len);
        rijndaelCipher.Key = keyBytes;
        rijndaelCipher.IV = keyBytes;
        ICryptoTransform transform = rijndaelCipher.CreateEncryptor();
        byte[] plainText = Encoding.UTF8.GetBytes(textToEncrypt);
        //return textToEncrypt;
        return Convert.ToBase64String(transform.TransformFinalBlock(plainText, 0, plainText.Length));
    }
}
