﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using GoogleMobileAds;
using GoogleMobileAds.Api;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;

public class MyGoogleAdsMobs : MonoBehaviour
{
	public static MyGoogleAdsMobs instance;
	public bool isNoAds;
	RewardedAd HeartAD;
	RewardedAd saveMapAD;
	[SerializeField] string unitId;
	[SerializeField] bool isTest;
	private void Awake()
	{
		if (instance == null)
			instance = this;
		else
		{
			Debug.LogWarning("Scene에 2개 이상의 광고 매니저가 존재합니다!");
			Destroy(gameObject);
		}
	}
	void Start()
	{
		if (isTest)
			unitId = "ca-app-pub-3940256099942544/5224354917"; //테스트 유닛 ID
		CreateAndLoadRewardedAd_gem100AD();
		CreateAndLoadRewardedAd_saveMapAD();
	}
    void CreateAndLoadRewardedAd_gem100AD()
    {
		HeartAD = new RewardedAd(unitId);

		HeartAD.OnAdLoaded += OnAdLoaded;
		HeartAD.OnAdFailedToLoad += OnAdFailedToLoad;
		HeartAD.OnAdOpening += OnAdOpening;
		HeartAD.OnAdFailedToShow += OnAdFailedToShow;
		HeartAD.OnUserEarnedReward += OnAdRewardedHeart;
		HeartAD.OnAdClosed += On1HeartClosed;

		AdRequest request = new AdRequest.Builder().Build();
		HeartAD.LoadAd(request);
	}
	void CreateAndLoadRewardedAd_saveMapAD()
	{
		saveMapAD = new RewardedAd(unitId);

		saveMapAD.OnAdLoaded += OnAdLoaded;
		saveMapAD.OnAdFailedToLoad += OnAdFailedToLoad;
		saveMapAD.OnAdOpening += OnAdOpening;
		saveMapAD.OnAdFailedToShow += OnAdFailedToShow;
		saveMapAD.OnUserEarnedReward += OnAdRewardedSaveMap;
		saveMapAD.OnAdClosed += OnLoadsaveMapAdClosed;

		AdRequest request = new AdRequest.Builder().Build();
		saveMapAD.LoadAd(request);
	}
	//private void OnDestroy()
	//{
	//	if (gem100AD != null)
	//	{
	//		gem100AD.OnAdLoaded -= OnAdLoaded;
	//		gem100AD.OnAdFailedToLoad -= OnAdFailedToLoad;
	//		gem100AD.OnAdOpening -= OnAdOpening;
	//		gem100AD.OnAdFailedToShow -= OnAdFailedToShow;
	//		gem100AD.OnUserEarnedReward -= OnAdRewardedHeart;
	//		gem100AD.OnAdClosed -= OnGem100AdClosed;
	//	}
	//	if (saveMapAD != null)
 //       {
	//		saveMapAD.OnAdLoaded -= OnAdLoaded;
	//		saveMapAD.OnAdFailedToLoad -= OnAdFailedToLoad;
	//		saveMapAD.OnAdOpening -= OnAdOpening;
	//		saveMapAD.OnAdFailedToShow -= OnAdFailedToShow;
	//		saveMapAD.OnUserEarnedReward -= OnAdRewardedSaveMap;
	//		saveMapAD.OnAdClosed -= OnLoadsaveMapAdClosed;
	//	}
	//}

	public void OnView1HeartAdClicked()
	{
		if (HeartAD.IsLoaded())
		{
			Debug.Log("View Ad");
			HeartAD.Show();
		}
		else
		{
			Debug.Log("Ad is Not Loaded");
			//LoadGem100Ad();
		}
	}

	public void OnViewSaveMapAdClicked()
	{
		if (saveMapAD.IsLoaded())
		{
			Debug.Log("View Ad");
			saveMapAD.Show();
        }
        else
        {
            Debug.Log("Ad is Not Loaded");
            //if (MapDataManager.instance.saveCoroutine != null)
            //{
            //    StopCoroutine(MapDataManager.instance.saveCoroutine);
            //    MapDataManager.instance.saveCoroutine = null;
            //}
            //LoadSaveMapAd();
        }
	}

	void OnAdLoaded(object sender, EventArgs args) { Debug.Log("OnAdLoaded");}
	void OnAdFailedToLoad(object sender, AdErrorEventArgs e) { Debug.Log("OnAdFailedToLoad");}
	void OnAdOpening(object sender, EventArgs e) { Debug.Log("OnAdOpening");}
	void OnAdFailedToShow(object sender, EventArgs e) { Debug.Log("OnAdFailedToShow");}

	void OnAdRewardedHeart(object sender, Reward e)
	{
		Debug.Log("OnAdRewarded 하트");
		DataManager.instance.HeartRewardStack+=3;
	}
	void OnAdRewardedSaveMap(object sender, Reward e)
	{
		Debug.Log("OnAdRewarded 맵");
		MapDataManager.instance.doSave = true;
		PopUpBoxManager.instance.PopConfirmBox(LocalizationManager.instance.GetMessage(1));
	}
	void On1HeartClosed(object sender, EventArgs e)
	{
		Debug.Log("OnAdClosed");
		CreateAndLoadRewardedAd_gem100AD();
		//LoadGem100Ad();
	}
	void OnLoadsaveMapAdClosed(object sender, EventArgs e)
	{
		Debug.Log("OnAdClosed");
		//if (MapDataManager.instance.saveCoroutine != null)
		//{
		//	StopCoroutine(MapDataManager.instance.saveCoroutine);
		//	MapDataManager.instance.saveCoroutine = null;
		//}
		//LoadSaveMapAd();
		CreateAndLoadRewardedAd_saveMapAD();
	}
	public void Buy5Heart()
	{
		DataManager.instance.HeartRewardStack += 5;
	}
	public void BuyNoADs()
	{
		MyGoogleAdsMobs.instance.isNoAds = true;
		SoundManager.instance.audioSourceSFX.PlayOneShot(SoundManager.instance.buyNoADsSound);
	}
	public void FailedPay()
	{
		SoundManager.instance.audioSourceSFX.PlayOneShot(SoundManager.instance.failedPay);
	}

	public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
	{
		bool validPurchase = true; // Presume valid for platforms with no R.V.

		// Unity IAP's validation logic is only included on these platforms.
#if UNITY_ANDROID || UNITY_IOS || UNITY_STANDALONE_OSX
		// Prepare the validator with the secrets we prepared in the Editor
		// obfuscation window.
		var validator = new CrossPlatformValidator(GooglePlayTangle.Data(),
			AppleTangle.Data(), Application.identifier);

		try
		{
			// On Google Play, result has a single product ID.
			// On Apple stores, receipts contain multiple products.
			var result = validator.Validate(e.purchasedProduct.receipt);
			// For informational purposes, we list the receipt(s)
			Debug.Log("Receipt is valid. Contents:");
			foreach (IPurchaseReceipt productReceipt in result)
			{
				Debug.Log(productReceipt.productID);
				Debug.Log(productReceipt.purchaseDate);
				Debug.Log(productReceipt.transactionID);
			}
		}
		catch (IAPSecurityException)
		{
			Debug.Log("Invalid receipt, not unlocking content");
			validPurchase = false;
		}
#endif

		if (validPurchase)
		{
			// Unlock the appropriate content here.
		}

		return PurchaseProcessingResult.Complete;
	}
}
