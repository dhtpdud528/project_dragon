﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInfoPackage : MonoBehaviour
{
    public string stageName;
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        StartCoroutine(SendDataUpdae());
    }
    IEnumerator SendDataUpdae()
    {
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        while (true)
        {
            if (GameManager.instance != null)
            {
                GameManager.instance.stageName = stageName;
                GameManager.instance.stageNameText.text = stageName;
                break;
            }
            yield return new WaitForEndOfFrame();
        }
        GameManager.instance.IsGamePlay = true;
        Destroy(gameObject);
    }
}
