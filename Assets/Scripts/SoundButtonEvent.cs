﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundButtonEvent : MonoBehaviour
{
    public Image buttonImageSFX;
    public Image buttonImageBGM;
    Button button;
    public enum EventType { SFX, BGM }
    public EventType type;
    // Start is called before the first frame update
    void Start()
    {
        button = GetComponent<Button>();

        SoundManager.instance.IconUpdate(buttonImageSFX, buttonImageBGM);

        if (type == EventType.SFX)
            button.onClick.AddListener(() => SoundManager.instance.SwitchSound(buttonImageSFX));
        if (type == EventType.BGM)
            button.onClick.AddListener(() => SoundManager.instance.SwitchBGM(buttonImageBGM));
    }

    // Update is called once per frame
    void Update()
    {

    }
}
