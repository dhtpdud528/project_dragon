﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseScript : MonoBehaviour
{
    private void Start()
    {
        GameManager.instance.Pause();
    }
    private void OnDisable()
    {
        if(GameManager.instance != null)
            GameManager.instance.Resume();
    }
}
