﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonLayoutInitRegister : MonoBehaviour
{
    Button button;
    // Start is called before the first frame update
    void Start()
    {
        button = GetComponent<Button>();

        button.onClick.AddListener(() => UIMainManager.instance.LayoutInit());

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
