﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonEventLacalization : MonoBehaviour
{
    Button someButton;
    public SystemLanguage language;
    private void Awake()
    {
        someButton = GetComponent<Button>();
        someButton.onClick.AddListener(() => LocalizationManager.instance.ChangeLanguage(language));
    }
}
