﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonSoundRegister : MonoBehaviour
{
    Button button;
    public AudioClip clickSound;
    // Start is called before the first frame update
    void Start()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(() => SoundManager.instance.PlayOneShot_SFX(clickSound));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
