﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonURL : MonoBehaviour
{
    public InputField urlText;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void DoOpenURL()
    {
        Application.OpenURL(urlText.text);
    }
    public void DoOpenURL(string url)
    {
        Application.OpenURL(url);
    }
}
