﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonSaveMapADRegister : MonoBehaviour
{
    Button button;
    // Start is called before the first frame update
    void Start()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(() => MyGoogleAdsMobs.instance.OnViewSaveMapAdClicked());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
