﻿using LitJson;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using UnityEngine;
[Serializable]
public class BaseProfile
{

}
[Serializable]
public class HaveNameProfile : BaseProfile
{
    public string name;

    public HaveNameProfile(string name)
    {
        this.name = name;
    }
}
[Serializable]
public class TrapProfile : HaveNameProfile
{
    public long deBurfDamage;

    public TrapProfile(string name, long deBurfDamage) : base(name)
    {
        this.deBurfDamage = deBurfDamage;
    }
}
[Serializable]
public class DeadableProfile : HaveNameProfile
{
    public int maxHP;

    public DeadableProfile(string name, int maxHP) : base(name)
    {
        this.maxHP = maxHP;
    }
}
[Serializable]
public class MoveableProfile : DeadableProfile
{
    public double moveSpeed;
    public double mass;

    public MoveableProfile(double moveSpeed, double mass, string name, int maxHP) : base(name, maxHP)
    {
        this.moveSpeed = moveSpeed;
        this.mass = mass;
    }
}
[Serializable]
public class AttackableProfile : MoveableProfile
{
    public int attackPower;
    public double attackSpeed;
    public double attackRange;
    public bool isDestroyAI;

    public AttackableProfile(int attackPower, double attackSpeed, double attackRange, double moveSpeed, double mass, string name, int maxHP, bool isDestroyAI) : base(moveSpeed, mass, name, maxHP)
    {
        this.attackPower = attackPower;
        this.attackSpeed = attackSpeed;
        this.attackRange = attackRange;
        this.isDestroyAI = isDestroyAI;
    }
}
public class ObjectPactory : MonoBehaviour
{
    public static ObjectPactory instance;
    //public string key = "대박나라얍";

    public GameObject[] monsters;
    public GameObject[] blocks;
    public GameObject breathController;

    public List<BaseProfile> blanaceProfiles = new List<BaseProfile>();

    public string balancePath;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Debug.LogWarning("Scene에 2개 이상의 밸런스 매니저가 존재합니다!");
            Destroy(gameObject);
        }
        this.balancePath = Application.persistentDataPath + "/BalanceData/";
        DirectoryInfo di = new DirectoryInfo(balancePath);
        if (di.Exists == false)
            di.Create();
    }
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Init());
    }
    IEnumerator Init()
    {
        yield return new WaitForEndOfFrame();
        InitLoad();
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void InitLoad()
    {
        string Jsonstring = "";
        var textData = Resources.Load("BalanceData") as TextAsset;
        Jsonstring = textData.ToString();
        JSONObject objectData = null; ;
        try
        {
            objectData = new JSONObject(Jsonstring);
            JsonData mapDataLitJson = JsonMapper.ToObject(Jsonstring);
        }
        catch (JsonException e)
        {
            Jsonstring = MapDataManager.Decrypt(Jsonstring, MapDataManager.instance.MapKey);
            objectData = new JSONObject(Jsonstring);
        }

        Debug.Log(Jsonstring);
        Load(objectData);
    }
    public void Load(JSONObject objectData)
    {
        //GameManager.instance.textLog.text += "밸런스파일 불러옴"
        //if (!File.Exists(balancePath + "BalanceData.json")) return;


        //string Jsonstring = "";
        //if (MapDesigner.instance != null)
        //    Jsonstring = File.ReadAllText(balancePath + "BalanceData.json");
        //else
        //{
            //var textData = Resources.Load("BalanceData") as TextAsset;
            //Jsonstring = textData.ToString();
        //}
        //JSONObject objectData = null; ;
        //try
        //{
        //    objectData = new JSONObject(Jsonstring);
        //    JsonData mapDataLitJson = JsonMapper.ToObject(Jsonstring);
        //}
        //catch (JsonException e)
        //{
        //    Jsonstring = MapDataManager.Decrypt(Jsonstring, MapDataManager.instance.MapKey);
        //    objectData = new JSONObject(Jsonstring);
        //}

        //Debug.Log(Jsonstring);


        //JSONObject objectData = new JSONObject(Jsonstring);

        if(objectData == null || objectData.list.Count <= 0)
        {
            string Jsonstring = "";
            var textData = Resources.Load("BalanceData") as TextAsset;
            Jsonstring = textData.ToString();

            try
            {
                objectData = new JSONObject(Jsonstring);
                JsonData mapDataLitJson = JsonMapper.ToObject(Jsonstring);
            }
            catch (JsonException e)
            {
                Jsonstring = MapDataManager.Decrypt(Jsonstring, MapDataManager.instance.MapKey);
                objectData = new JSONObject(Jsonstring);
            }
        }
        
        for (int i = 0; i < monsters.Length; i++)
        {
            if (monsters[i].GetComponent<AI_Attackable>())
            {
                var monster = monsters[i].GetComponent<AI_Attackable>();
                var loadData = objectData.list.Find(val => val["name"].str.Equals(monster.Name));

                monster.maxHP = (int)loadData["maxHP"].i;
                //monster.mobProfile = 
                //    new AI_Attackable.AI_AttackableProfile(
                //    (int)loadData["attackPower"].i,
                //    loadData["attackSpeed"].f,
                //    loadData["attackRange"].f,
                //    loadData["moveSpeed"].f,
                //    monster.transform.localScale
                //    );
                monster.mobProfile.attackPower = (int)loadData["attackPower"].i;
                monster.mobProfile.attackSpeed = loadData["attackSpeed"].f;
                monster.mobProfile.attackRange = loadData["attackRange"].f;
                monster.mobProfile.moveSpeed = loadData["moveSpeed"].f;
                monster.mobProfile.Mass = loadData["mass"].f;
                monster.mobProfile.isDestroyAI = loadData["isDestroyAI"].b;
            }
            else if (monsters[i].GetComponent<AI_Burfable>())
            {
                var monster = monsters[i].GetComponent<AI_Burfable>();
                var loadData = objectData.list.Find(val => val["name"].str.Equals(monster.Name));

                monster.maxHP = (int)loadData["maxHP"].i;
                //monster.mobProfile =
                //    new AI_Burfable.AI_BurfalbeProfile(
                //    monster.mobProfile.type,
                //    loadData["moveSpeed"].f,
                //    monster.transform.localScale
                //    );
                monster.mobProfile.moveSpeed = loadData["moveSpeed"].f;
                monster.mobProfile.Mass = loadData["mass"].f;
            }
        }
        for (int i = 0; i < blocks.Length; i++)
        {
            if (blocks[i].GetComponent<Object_Deadable>())
            {
                var deadable = blocks[i].GetComponent<Object_Deadable>();
                var loadData = objectData.list.Find(val => val["name"].str.Equals(deadable.Name));
                deadable.maxHP = (int)loadData["maxHP"].i;
            }
            else if (blocks[i].GetComponent<Floor_Trap>())
            {
                var trap = blocks[i].GetComponent<Floor_Trap>();
                var loadData = objectData.list.Find(val => val["name"].str.Equals(trap.Name));
                trap.profile.deBurfDamage = loadData["deBurfDamage"].i;

            }
        }
        var breathData = objectData.list.Find(val => val["circleMaxSize"] != null);
        float maxSize = 100;
        try
        {
            breathController.GetComponent<BreathController>().profile.breathDamage = (int)breathData["breathDamage"].i;
            breathController.GetComponent<BreathController>().profile.breathPushPower = breathData["breathPushPower"].f;
            breathController.GetComponent<BreathController>().profile.circleMaxSize = maxSize = breathData["circleMaxSize"].f;
            breathController.GetComponent<BreathController>().profile.maxBreath = breathController.GetComponent<BreathController>().curBreath = breathData["maxBreath"].f;
            breathController.GetComponent<BreathController>().profile.recoverBreath = breathData["recoverBreath"].f;
            breathController.GetComponent<BreathController>().profile.consumptionRate = breathData["consumptionRate"].f;
            breathController.GetComponent<BreathController>().profile.exitTime = breathData["exitTime"].f;
            breathController.GetComponent<BreathController>().profile.itemRecoverValue = breathData["itemRecoverValue"].f;
            breathController.GetComponent<BreathController>().breathCircle.transform.localScale = new Vector2(maxSize / 100, maxSize / 100);
            breathController.GetComponent<BreathController>().profile.fireTickDamage = (int)breathData["fireTickDamage"].i;
            breathController.GetComponent<BreathController>().profile.collisionDamage = (int)breathData["collisionDamage"].i;
            breathController.GetComponent<BreathController>().profile.breathChargingTime = breathData["breathChargingTime"].f;
        }
        catch(NullReferenceException e)
        {

        }
    }
    public void Save()
    {
        blanaceProfiles = new List<BaseProfile>();
        for (int i = 0; i < monsters.Length; i++)
        {
            if (monsters[i].GetComponent<AI_Attackable>())
            {
                var monster = monsters[i].GetComponent<AI_Attackable>();
                blanaceProfiles.Add(new AttackableProfile(monster.mobProfile.attackPower, monster.mobProfile.attackSpeed, monster.mobProfile.attackRange, monster.mobProfile.moveSpeed, monster.mobProfile.Mass, monster.Name, monster.maxHP, monster.mobProfile.isDestroyAI));
            }
            else if (monsters[i].GetComponent<AI_Burfable>())
            {
                var monster = monsters[i].GetComponent<AI_Burfable>();
                blanaceProfiles.Add(new MoveableProfile(monster.mobProfile.moveSpeed, monster.mobProfile.Mass, monster.Name, monster.maxHP));
            }
        }
        for (int i = 0; i < blocks.Length; i++)
        {
            if (blocks[i].GetComponent<Object_Deadable>())
            {
                var deadAble = blocks[i].GetComponent<Object_Deadable>();
                blanaceProfiles.Add(new DeadableProfile(deadAble.Name, deadAble.maxHP));
            }
            else if (blocks[i].GetComponent<Floor_Trap>())
                blanaceProfiles.Add(new TrapProfile(blocks[i].GetComponent<Floor_Trap>().Name, blocks[i].GetComponent<Floor_Trap>().profile.deBurfDamage));
        }
        blanaceProfiles.Add(breathController.GetComponent<BreathController>().profile);
    }
}
