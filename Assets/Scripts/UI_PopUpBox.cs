﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class UI_PopUpBox : MonoBehaviour
{
    public Text textBox;
    public AudioClip popupSound;
    public AudioClip disableSound;
    // Start is called before the first frame update
    void Start()
    {
        
    }
    private void OnEnable()
    {
        SoundManager.instance.PlayOneShot_SFX(popupSound);
    }
    private void OnDisable()
    {
        SoundManager.instance.PlayOneShot_SFX(disableSound);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
