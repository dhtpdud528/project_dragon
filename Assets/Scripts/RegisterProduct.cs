﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RegisterProduct : MonoBehaviour
{
    Button button;
    public int productId;

    // Start is called before the first frame update
    void Start()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(() => IAPManager.instance.OnBtnPurchaseClicked(productId));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
