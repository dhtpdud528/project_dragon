﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SortingLayerInMeshRenderer : MonoBehaviour
{
    MeshRenderer mesh;
    Renderer parentRenderer;
    void Start()
    {
        mesh = GetComponent<MeshRenderer>();
        parentRenderer = transform.parent.GetComponent<Renderer>();

        mesh.sortingLayerName = parentRenderer.sortingLayerName;
    }
    public void Update()
    {
        parentRenderer.sortingOrder = Mathf.RoundToInt(transform.parent.position.y * 100f)*-1;
        mesh.sortingOrder = parentRenderer.sortingOrder + 1;
    }
}
