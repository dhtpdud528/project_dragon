﻿using System;
using System.Collections;
using UnityEngine;
using Spine.Unity;
//using Boo.Lang;

public class Object_Deadable : MonoBehaviour, IName
{
    //public struct DeBurfProfile
    //{
    //    ObjectState state;
    //    int damage;

    //    public DeBurfProfile(ObjectState state, int damage)
    //    {
    //        this.state = state;
    //        this.damage = damage;
    //    }
    //}
    //List<DeBurfProfile> deBurfProfiles = new List<DeBurfProfile>();
    [SerializeField]
    private int deburfDamage = 0;
    public enum ObjectState
    {
        None = 0,
        Burn = 1 << 0,
        Slow = 1 << 1,
        Paralysis = 1 << 2,
        All = int.MaxValue
    }
    ObjectState state;
    public ObjectState State
    {
        set
        {
            state = value;
            if ((state & ObjectState.Burn) != 0 && burnCoroutine == null)
                StartCoroutine(burnCoroutine = BurnCoroutine());
            if ((state & ObjectState.Slow) != 0 && slowCoroutine == null)
                StartCoroutine(slowCoroutine = SlowCoroutine());
            if ((state & ObjectState.Paralysis) != 0 && paralysisCoroutine == null)
                StartCoroutine(paralysisCoroutine = ParalysisCoroutine());
        }
        get
        {
            return state;
        }
    }
    public string m_name;
    public string Name
    {
        get
        {
            return m_name;
        }
        set
        {
            m_name = value;
        }
    }
    protected Animator anim;
    public AnimatorStateInfo animInfo;
    public SpriteRenderer spriteHP;
    public int maxHP;
    protected int hp;
    public int HP
    {
        get
        {
            return hp;
        }
        set
        {
            hp = value;
            if (hp > maxHP)
                hp = maxHP;
            if (spriteHP != null)
                spriteHP.transform.localScale = new Vector3((float)(hp) / (float)maxHP, 1, 1);
        }
    }

    int id;
    public int ID
    {
        get
        {
            return id;
        }
        set
        {
            id = value;
        }

    }

    private IEnumerator disableCoroutine;
    private IEnumerator burnCoroutine;
    private IEnumerator slowCoroutine;
    private IEnumerator paralysisCoroutine;
    // Start is called before the first frame update
    protected virtual void Awake()
    {
        hp = maxHP;
        anim = GetComponent<Animator>();

    }
    protected virtual void Start()
    {
        if (id == 0)
            do
            {
                id = UnityEngine.Random.Range(1, int.MaxValue);
            }
            while (GameManager.instance.ids.Contains(id));
        GameManager.instance.ids.Add(id);
        if (Name.Contains("Egg"))
        {
            GameManager.instance.eggs.Add(this);
        }
        if (!GetComponent<AI_Moveable>() && !MapDataManager.instance.spawnedBlocks.Contains(gameObject))
            MapDataManager.instance.spawnedBlocks.Add(gameObject);
    }
    private void OnDisable()
    {
        if (!GetComponent<AI_Moveable>())
            MapDataManager.instance.spawnedBlocks.Remove(gameObject);
        if (Name.Contains("Egg"))
            GameManager.instance.eggs.Remove(this);
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        animInfo = anim.GetCurrentAnimatorStateInfo(0);
    }
    public virtual void LateUpdate()
    {
        GetComponent<Renderer>().sortingOrder = Mathf.RoundToInt(transform.position.y * 100f) * -1;
    }
    public void AddDeburf(GameManager.BreathType type)
    {
        switch (type)
        {
            case GameManager.BreathType.fire:
                State |= ObjectState.Burn;
                break;
            case GameManager.BreathType.ice:
                State |= ObjectState.Slow;
                break;
            case GameManager.BreathType.lightning:
                State |= ObjectState.Paralysis;
                break;
        }
    }
    public void AddDeburf(GameManager.BreathType type, int damage)
    {
        deburfDamage = damage;
        AddDeburf(type);
    }
    public IEnumerator BurnCoroutine()
    {
        GameObject fireParticle = Instantiate(GameManager.instance.fireDeburfParticle[UnityEngine.Random.Range(0, GameManager.instance.fireDeburfParticle.Length)], transform.position + transform.up, Quaternion.identity);
        fireParticle.transform.SetParent(transform);
        for (int deburfCount = 0; deburfCount < 3; deburfCount++)
        {
            yield return new WaitForSeconds(1);
            TakeDamage(deburfDamage);
        }
        deburfDamage = 0;
        Destroy(fireParticle);
        state &= ~ObjectState.Burn;
        burnCoroutine = null;
    }
    public IEnumerator SlowCoroutine()
    {
        if (gameObject.GetComponent<AI_Attackable>() != null)
        {
            AI_Attackable ai = gameObject.GetComponent<AI_Attackable>();
            float moveSlowValue = ai.mobProfile.moveSpeed / 1.2f;
            float attackSlowValue = ai.mobProfile.attackSpeed / 1.2f;
            ai.mobProfile.moveSpeed -= moveSlowValue;
            ai.mobProfile.attackSpeed -= attackSlowValue;
            yield return new WaitForSeconds(10);
            ai.mobProfile.moveSpeed += moveSlowValue;
            ai.mobProfile.attackSpeed += attackSlowValue;
        }
        state &= ~ObjectState.Slow;
        slowCoroutine = null;
    }
    public IEnumerator ParalysisCoroutine()
    {
        GameObject lightningParticle = Instantiate(GameManager.instance.electricDeburfParticle[UnityEngine.Random.Range(0, GameManager.instance.electricDeburfParticle.Length)], transform.position + transform.up, Quaternion.identity);
        lightningParticle.transform.SetParent(transform);
        if (gameObject.GetComponent<AI_Attackable>() != null)
        {
            AI_Attackable ai = gameObject.GetComponent<AI_Attackable>();
            float value = ai.mobProfile.moveSpeed;
            ai.mobProfile.moveSpeed -= value;
            ai.mobProfile.attackSpeed -= value;
            ai.m_rigidbody.isKinematic = true;
            ai.m_rigidbody.constraints = RigidbodyConstraints2D.FreezeAll;
            yield return new WaitForSeconds(10);
            ai.mobProfile.moveSpeed += value;
            ai.mobProfile.attackSpeed += value;
            ai.m_rigidbody.isKinematic = false;
            ai.m_rigidbody.constraints = RigidbodyConstraints2D.FreezeRotation;
        }
        Destroy(lightningParticle);
        state &= ~ObjectState.Paralysis;
        paralysisCoroutine = null;
    }
    public virtual void TakeDamage(int damage)
    {
        if (HP <= 0) return;
        PlayHitSound();
        HP -= damage;
        if (HP <= 0)
        {
            spriteHP.transform.localScale = new Vector3(0, 1, 1);
            Dead();
            return;
        }
        DisableHPbar();
        anim.CrossFadeInFixedTime("hit", 0);
    }
    public virtual void Dead()
    {
        //PlayDeadSound();
        anim.SetBool("isDead", true);
        StartCoroutine(DeadCoroutine());
    }
    protected virtual IEnumerator DeadCoroutine()
    {
        yield return new WaitForSeconds(1);
        GetComponent<Collider2D>().enabled = false;
        if (CompareTag("Obstacle"))
            GameManager.instance.AstarScan();
        if (MapDesigner.instance == null && Name.Contains("Egg"))
        {
            int eggsCount = GameManager.instance.getAliveEggsCount();
            if (eggsCount <= 0)
            {
                GameManager.instance.result.SetActive(true);
                GameManager.instance.resultSpine.SetInteger("Eggs", eggsCount);
                GameManager.instance.dragonSpine.SetTrigger("isLose");
                SoundManager.instance.PlayOneShot_SFX(SoundManager.instance.loseSound);
                GameManager.instance.nextStageButton.interactable = false;
            }
        }
        //Destroy(gameObject);
    }
    public void DisableHPbar()
    {
        if (spriteHP == null) return;
        if (disableCoroutine != null)
            StopCoroutine(disableCoroutine);
        StartCoroutine(disableCoroutine = DisableHPbarCoroutine());
    }


    IEnumerator DisableHPbarCoroutine()
    {
        spriteHP.transform.parent.gameObject.SetActive(true);
        yield return new WaitForSeconds(3);
        spriteHP.transform.parent.gameObject.SetActive(false);
        yield break;
    }
    public void Heal(int amount)
    {
        anim.CrossFadeInFixedTime("heal", 0);
        HP += amount;
        DisableHPbar();
    }
    public void PlayHitSound()
    {
        if (gameObject.GetComponent<AI_Attackable>()) return;
        if (Name.Contains("Egg"))
            SoundManager.instance.PlayOneShot_SFX(SoundManager.instance.eggHit);
        else if (Name.Contains("Log"))
            SoundManager.instance.PlayOneShot_SFX(SoundManager.instance.LogHit);
    }
    public void PlayDeadSound()
    {
        if (gameObject.GetComponent<AI_Attackable>()) return;
        if (Name.Contains("Egg"))
            SoundManager.instance.PlayOneShot_SFX(SoundManager.instance.eggBreak);
        else if(Name.Contains("Log"))
            SoundManager.instance.PlayOneShot_SFX(SoundManager.instance.LogBreak);
    }
}
