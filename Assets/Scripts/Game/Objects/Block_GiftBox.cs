﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block_GiftBox : Object_Deadable
{
    protected override void Awake()
    {
        base.Awake();
    }
    protected override void Start()
    {
        base.Start();
    }
    public void drop_random_item()
    {
        Instantiate(GameManager.instance.item, transform.position, Quaternion.identity).GetComponent<EntitySkillStone>().Type = (GameManager.BreathType)UnityEngine.Random.Range(0, 5);
    }
}
