﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using System;


public class AI_Moveable : Object_Deadable
{
    [Serializable]
    public class AIProfile
    {
        public AI_Moveable m_body;
        public float moveSpeed;
        public Vector3 size;
        [SerializeField]
        float mass;
        public float Mass
        {
            get
            {
                return mass;
            }
            set
            {
                mass = value;
                if(m_body != null)
                    m_body.m_rigidbody.mass = mass;
            }
        }

        public AIProfile(float moveSpeed, Vector3 size)
        {
            this.moveSpeed = moveSpeed;
            this.size = size;
        }
    }
    public float maxVelocity;

    public Seeker seeker;
    public Path path;

    public int currentWaypoint;
    Transform destinationTarget;
    public Transform DestinationTarget
    {
        get
        {
            return destinationTarget;
        }
        set
        {
            destinationTarget = value;
            if(destinationTarget != null)
                realPosTarget = (Vector2)DestinationTarget.position + DestinationTarget.GetComponent<Collider2D>().offset * DestinationTarget.localScale.y;
        }
    }

    public Rigidbody2D m_rigidbody;
    public bool flip;

    public float nextWaypointDistance;
    public enum MoveType { Ground, Fly }
    public MoveType moveType;

    public int turnDistance;

    public string asset;
    public string spine;
    public enum Polygon { SmallWall, LargeWall }
    public Polygon polygon;
    public int gaugePosition;
    public int waistScaleWidth;
    public int waistScaleHeight;
    public bool reachedEndOfPath;

    public float flipTick;
    protected Vector2 realPos;
    protected Vector2 realPosTarget;

    // Start is called before the first frame update
    protected override void Awake()
    {
        base.Awake();
        m_rigidbody = GetComponent<Rigidbody2D>();
        seeker = GetComponent<Seeker>();
    }
    protected override void Start()
    {
        base.Start();
        InvokeRepeating("UpdatePath", 0, .5f);
    }
    public void OnPathComplete(Path p)
    {
        if (!p.error)
        {
            path = p;
            currentWaypoint = 0;
        }
    }
    public void UpdatePath()
    {
        if (seeker.IsDone() && DestinationTarget != null)
            seeker.StartPath(realPos, realPosTarget, OnPathComplete);
    }
    public float DistanceTarget(Transform target, bool debug)
    {
        Vector2 realPos = (Vector2)m_rigidbody.position + GetComponent<Collider2D>().offset * transform.localScale.y;

        Vector2 realPosTarget = (Vector2)target.position + target.GetComponent<Collider2D>().offset * target.localScale.y;

        Vector2 dirToTarget = (realPosTarget - realPos).normalized;

        Vector2 handPos = realPos + (dirToTarget * (GetComponent<CircleCollider2D>().radius * transform.localScale.y));

        Vector2 TouchableTargetPos = realPosTarget + (-dirToTarget * (target.GetComponent<CircleCollider2D>().radius * target.localScale.y));

        if (debug)
        {
            Debug.DrawRay(realPos, (dirToTarget * (GetComponent<CircleCollider2D>().radius * transform.localScale.y)), Color.red);
            Debug.DrawRay(realPosTarget, (-dirToTarget * (target.GetComponent<CircleCollider2D>().radius * target.localScale.y)), Color.yellow);
        }

        return Vector2.Distance(handPos, TouchableTargetPos);
    }
    // Update is called once per frame
    public virtual void FixedUpdate()
    {
        realPos = (Vector2)transform.position + GetComponent<Collider2D>().offset * transform.localScale.y;
        if (path == null)
            return;

        if (currentWaypoint >= path.vectorPath.Count)
        {
            reachedEndOfPath = true;
            return;
        }
        else
        {
            reachedEndOfPath = false;
        }

        float distance = Vector2.Distance(realPos, path.vectorPath[currentWaypoint]);

        if (distance < nextWaypointDistance)
        {
            currentWaypoint++;
        }

        if (Mathf.Abs(m_rigidbody.velocity.x) > maxVelocity)
        {
            float val = maxVelocity;
            if (m_rigidbody.velocity.x < 0)
                val = -maxVelocity;
            m_rigidbody.velocity = new Vector2(val, m_rigidbody.velocity.y);
        }
        if (Mathf.Abs(m_rigidbody.velocity.y) > maxVelocity)
        {
            float val = maxVelocity;
            if (m_rigidbody.velocity.y < 0)
                val = -maxVelocity;
            m_rigidbody.velocity = new Vector2(m_rigidbody.velocity.x, val);
        }
    }
    public virtual bool flipAnimation(bool value)
    {

        if (flip != value)
        {
            if(IsMove())
                anim.CrossFadeInFixedTime("flip", 0);
            flip = value;
            //spriteHP.transform.localScale = new Vector2(-spriteHP.transform.localScale.x, spriteHP.transform.localScale.y);
            //spriteHP.transform.parent.localScale = new Vector2(-spriteHP.transform.parent.localScale.x, spriteHP.transform.parent.localScale.y);
            return true;
        }
        return false;
    }
    public bool IsMove()
    {
        return animInfo.IsName("run");
    }
}
