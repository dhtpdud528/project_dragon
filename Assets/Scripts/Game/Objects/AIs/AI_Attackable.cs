﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using System;

public class AI_Attackable : AI_Moveable
{
    [Serializable]
    public class AI_AttackableProfile : AIProfile
    {
        public int attackPower;
        public float attackSpeed;
        public float attackRange;
        public bool isDestroyAI;

        public AI_AttackableProfile(int attackPower, float attackSpeed, float attackRange, float moveSpeed, Vector3 size, bool isDestroyAI) : base(moveSpeed, size)
        {
            this.attackPower = attackPower;
            this.attackSpeed = attackSpeed;
            this.attackRange = attackRange;
            this.isDestroyAI = isDestroyAI;
        }
    }
    [HideInInspector]
    public AI_AttackableProfile originProfile;
    public AI_AttackableProfile mobProfile;
    public SplashAttackCollider attackCollider;

    public Object_Deadable attackTarget;
    private IEnumerator confusing;
    public bool isUpdateDestination;

    float hpBarSize;
    float hpBarPos;
    

    // Start is called before the first frame update

    protected override void Awake()
    {
        base.Awake();
        //seeker.
        hpBarSize = spriteHP.transform.parent.localScale.x;
        hpBarPos = spriteHP.transform.parent.localPosition.x;
        mobProfile.m_body = this;
        m_rigidbody.mass = mobProfile.Mass;
        originProfile = new AI_AttackableProfile(mobProfile.attackPower, mobProfile.attackSpeed, mobProfile.attackRange, mobProfile.moveSpeed, transform.localScale, mobProfile.isDestroyAI);

        if (mobProfile.isDestroyAI)
            seeker.graphMask = GraphMask.FromGraphName("Destroyer Graph");
        else
            seeker.graphMask = GraphMask.FromGraphName("Runner Graph");

    }
    protected override void Start()
    {
        //base.Start();

        StartCoroutine(UpdateCloestTarget());
    }
    IEnumerator UpdateCloestTarget()
    {
        while (true)
        {
            if(isUpdateDestination)
                DestinationTarget = SearchTarget();
            seeker.StartPath(realPos, realPosTarget, OnPathComplete);
            yield return new WaitUntil(() => seeker.IsDone());
            yield return new WaitForSeconds(0.1f);

        }
    }
    public Transform SearchTarget()
    {
        float closet = float.MaxValue;

        Transform target = null;

        for (int i = 0; i < GameManager.instance.eggs.Count; i++)
        {
            var distance = DistanceTarget(GameManager.instance.eggs[i].transform, false);
            if (GameManager.instance.eggs[i].HP > 0 && closet > distance)
            {
                closet = distance;
                target = GameManager.instance.eggs[i].transform;
            }
        }
        return target;
    }
    public void OnDisable()
    {
        GameManager.instance.monsters.Remove(this);
    }
    protected override void Update()
    {
        base.Update();
        anim.SetFloat("AttackSpeed", mobProfile.attackSpeed);
        if (mobProfile.moveSpeed <= 0 || originProfile.moveSpeed <= 0)
            anim.SetFloat("MoveSpeed", 0);
        else
            anim.SetFloat("MoveSpeed", mobProfile.moveSpeed / originProfile.moveSpeed);
    }
    // Update is called once per frame
    public override void FixedUpdate()
    {
        base.FixedUpdate();
        //if (destination == null || destination.GetComponent<Object_Deadable>() && destination.GetComponent<Object_Deadable>().HP <= 0)
        //{
        //    destination = SearchTarget();
        //    return;
        //}
        if (path == null || DestinationTarget == null)
            return;
        

        if (currentWaypoint >= path.vectorPath.Count)
        {
            reachedEndOfPath = true;
            return;
        }
        else
        {
            reachedEndOfPath = false;
        }
        Vector2 direction = ((Vector2)path.vectorPath[currentWaypoint] - realPos).normalized;
        Vector2 force = direction * mobProfile.moveSpeed * Time.deltaTime;
        
        if (IsConfuse())
        {
            //m_rigidbody.velocity *= 0;
            gameObject.layer = 12;
            m_rigidbody.collisionDetectionMode = CollisionDetectionMode2D.Continuous;
            m_rigidbody.interpolation = RigidbodyInterpolation2D.Interpolate;
        }
        else
        {
            m_rigidbody.velocity += force;
            gameObject.layer = 11;
            m_rigidbody.collisionDetectionMode = CollisionDetectionMode2D.Discrete;
            m_rigidbody.interpolation = RigidbodyInterpolation2D.None;
        }
        if (IsDestinationInRange())
        {
            if(DestinationTarget.GetComponent<Object_Deadable>())
                attackTarget = DestinationTarget.GetComponent<Object_Deadable>();
            anim.SetBool("Attack", true);
            if (!IsAttacking() && !IsConfuse())
                anim.CrossFadeInFixedTime("attack", 0);
        }
        else
        {
            //attackTarget = null;
            anim.SetBool("Attack", false);
        }

        if (m_rigidbody.velocity.x >= 0.1f)
        {
            if (flipAnimation(true))
                if (animInfo.IsName("hit_up"))
                {
                    transform.localScale = originProfile.size;
                    spriteHP.transform.parent.localPosition = new Vector3(hpBarPos, spriteHP.transform.parent.localPosition.y, spriteHP.transform.parent.localPosition.z);
                    spriteHP.transform.parent.localScale = new Vector3(hpBarSize, hpBarSize, 1);
                }
                else
                {
                    transform.localScale = new Vector3(-originProfile.size.x, originProfile.size.y, originProfile.size.z);
                    spriteHP.transform.parent.localPosition = new Vector3(-hpBarPos, spriteHP.transform.parent.localPosition.y, spriteHP.transform.parent.localPosition.z);
                    spriteHP.transform.parent.localScale = new Vector3(-hpBarSize, hpBarSize, 1);
                }

        }
        else if (m_rigidbody.velocity.x <= -0.1f)
        {
            if (flipAnimation(false))
                if (animInfo.IsName("hit_up"))
                {
                    transform.localScale = new Vector3(-originProfile.size.x, originProfile.size.y, originProfile.size.z);
                    spriteHP.transform.parent.localPosition = new Vector3(-hpBarPos, spriteHP.transform.parent.localPosition.y, spriteHP.transform.parent.localPosition.z);
                    spriteHP.transform.parent.localScale = new Vector3(-hpBarSize, hpBarSize, 1);
                }
                else
                {
                    transform.localScale = originProfile.size;
                    spriteHP.transform.parent.localPosition = new Vector3(hpBarPos, spriteHP.transform.parent.localPosition.y, spriteHP.transform.parent.localPosition.z);
                    spriteHP.transform.parent.localScale = new Vector3(hpBarSize, hpBarSize, 1);
                }
        }
    }
    
    public override bool flipAnimation(bool value)
    {
        if (IsConfuse()) return false;
        else
            return base.flipAnimation(value);

    }
    public override void TakeDamage(int damage)
    {
        if (HP <= 0) return;
        //anim.SetBool("Confused", false);
        HP -= damage;
        if (HP <= 0)
        {
            spriteHP.transform.localScale = new Vector3(0, 1, 1);
            Dead();
            return;
        }
        DisableHPbar();
    }
    public void TakeDamage(int damage, Vector2 dir)
    {
        if(HP <= 0) return;
        //anim.SetBool("Confused", false);
        //PlayHitSound();
        HP -= damage;
        if (HP <= 0)
        {
            spriteHP.transform.localScale = new Vector3(0, 1, 1);
            Dead();
            return;
        }
        if(damage > 0)
            DisableHPbar();
        if (dir.y > 0f || dir.x > 0f)
            anim.CrossFadeInFixedTime("hit_up", 0);
        else if (dir.y < 0f || dir.x < 0f)
            anim.CrossFadeInFixedTime("hit_down", 0);
        if (confusing != null)
            StopCoroutine(confusing);
        StartCoroutine(confusing = Confuse());
    }
    public override void Dead()
    {
        //PlayDeadSound();
        anim.SetBool("isDead", true);
        GameManager.instance.monsters.Remove(this);
        StartCoroutine(DeadCoroutine());
    }
    protected override IEnumerator DeadCoroutine()
    {
        if (MapDesigner.instance == null && GameManager.instance.isAllWavesEnd && GameManager.instance.IsAllMonsterDead())
        {
            yield return new WaitForSeconds(2);
            GameManager.instance.StageClear();
        }
        Destroy(gameObject, 1);
    }
    public void DeadHole()
    {
        SoundManager.instance.PlayOneShot_SFX(SoundManager.instance.deadHoleSound);
        HP = 0;
        anim.CrossFadeInFixedTime("dead_hole", 0);
        GameManager.instance.monsters.Remove(this);
        StartCoroutine(DeadHoleCoroutine());
    }
    protected IEnumerator DeadHoleCoroutine()
    {
        if (MapDesigner.instance == null && GameManager.instance.isAllWavesEnd && GameManager.instance.IsAllMonsterDead())
        {
            yield return new WaitForSeconds(2);
            GameManager.instance.StageClear();
        }
        Destroy(gameObject, 1);
    }
    IEnumerator Confuse()
    {
        anim.SetBool("Confused", true);
        yield return new WaitForSeconds(3);
        anim.SetBool("Confused", false);
    }
    //public override IEnumerator AddDeburfCoroutine(GameManager.BreathType type)
    //{
    //    yield return base.AddDeburfCoroutine(type);
    //    if(type == GameManager.BreathType.lightning)
    //    {

    //    }
    //}
    public bool IsConfuse()
    {
        return animInfo.IsName("hit_up") || animInfo.IsName("hit_down") || animInfo.IsName("dead") || animInfo.IsName("dead_hole") || anim.GetBool("Confused");
    }
    public bool IsAttacking()
    {
        return animInfo.IsName("attack");
    }
    public bool IsDestinationInRange()
    {
        return
            //Vector2.Distance(DestinationTarget.transform.position, (Vector2)transform.position + GetComponent<Collider2D>().offset * transform.localScale.y) 
            DistanceTarget(DestinationTarget.transform, true)
            <= mobProfile.attackRange;
    }
    public bool IsTargetInRange()
    {
        if (attackTarget != null)
            return
                //Vector2.Distance((Vector2)attackTarget.transform.position + attackTarget.GetComponent<Collider2D>().offset* attackTarget.transform.localScale.y, (Vector2)transform.position + GetComponent<Collider2D>().offset* transform.localScale.y) 
                DistanceTarget(attackTarget.transform, true)
                <= mobProfile.attackRange;
        else
            return false;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!collision.collider.CompareTag("Monster") && IsConfuse() && m_rigidbody.velocity.magnitude > 0.5f)
        {
            PlayBoundSound();
            TakeDamage(GameManager.instance.breathController.profile.collisionDamage);
        }
    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        if (attackTarget != DestinationTarget.GetComponent<Object_Deadable>() && collision.collider.CompareTag("Obstacle"))
        {
            attackTarget = collision.collider.GetComponent<Object_Deadable>();
            anim.SetBool("Attack", true);
            if (!IsAttacking() && !IsConfuse())
                anim.CrossFadeInFixedTime("attack", 0);
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (attackTarget != null && attackTarget != DestinationTarget.GetComponent<Object_Deadable>() && collision.collider.CompareTag("Obstacle"))
        {
            //attackTarget = null;
            anim.SetBool("Attack", false);
        }
    }
    public void DoAttack()
    {
        //SoundManager.instance.PlayOneShot_SFX(attackSound);
        if (attackCollider != null)
        {
            //StartCoroutine(DoSplashAttack());
            attackCollider.damage = mobProfile.attackPower;
            attackCollider.m_collider.enabled = true;
        }
        if (IsTargetInRange())
            attackTarget.TakeDamage(mobProfile.attackPower);
    }
    public void PlayBoundSound()
    {
        SoundManager.instance.PlayOneShot_SFX(SoundManager.instance.boundSound);
    }
    //IEnumerator DoSplashAttack()
    //{
        
    //    attackCollider.damage = mobProfile.attackPower;
    //    attackCollider.m_collider.enabled = true;
    //    yield return new WaitForFixedUpdate();
    //    attackCollider.m_collider.enabled = false;
    //}
}
