﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening;

public class AI_Burfable : AI_Moveable
{
    [Serializable]
    public class AI_BurfalbeProfile : AIProfile
    {
        public enum BurfType { Heal, Aggro, Gift}
        public BurfType type;

        public AI_BurfalbeProfile(BurfType type, float moveSpeed, Vector3 size) : base(moveSpeed, size)
        {
            this.type = type;
        }
    }
    [HideInInspector]
    public AI_BurfalbeProfile originProfile;
    public AI_BurfalbeProfile mobProfile;
    public GameObject speeachBox;
    public GameObject speeachText;
    public bool isBurfOn;
    private float speeachTextSize;

    protected override void Awake()
    {
        base.Awake();
        originProfile = new AI_BurfalbeProfile(mobProfile.type, mobProfile.moveSpeed, transform.localScale);
        originProfile.m_body = this;
        m_rigidbody.mass = originProfile.Mass;
    }
    protected override void Start()
    {
        base.Start();
        speeachTextSize = speeachText.transform.localScale.x;
        StartCoroutine(ClickMeCoroutine());
        StartCoroutine(UpdateCloestTarget());
    }
    IEnumerator ClickMeCoroutine()
    {
        yield return new WaitForSeconds(5);
        if(!isBurfOn)
        {
            speeachBox.SetActive(true);
            SoundManager.instance.PlayRandomAudio_SFX(SoundManager.instance.allysClickMeSound);
            speeachBox.transform.DOPunchScale(new Vector3(speeachBox.transform.localScale.x, 20, 20), 0.5f, 2);
        }
    }
    IEnumerator UpdateCloestTarget()
    {
        while (true)
        {
            DestinationTarget = DestinationTarget;
            seeker.StartPath(realPos, realPosTarget, OnPathComplete);
            yield return new WaitUntil(() => seeker.IsDone());
            yield return new WaitForSeconds(0.1f);

        }
    }
    protected override void Update()
    {
        base.Update();
        if (mobProfile.moveSpeed <= 0 || originProfile.moveSpeed <= 0)
            anim.SetFloat("MoveSpeed", 0);
        else
            anim.SetFloat("MoveSpeed", mobProfile.moveSpeed / originProfile.moveSpeed);
        if (IsDestinationInRange())
            Destroy(gameObject);
    }
    public void OnMouseDown()
    {
        if (!IsBurf())
            anim.CrossFadeInFixedTime("touch", 0);
        GameManager.instance.hoverObject = gameObject;
    }
    public void OnMouseExit()
    {
        GameManager.instance.hoverObject = null;
    }
    public virtual void sound_ally_active()
    {
        isBurfOn = true;
        speeachBox.SetActive(false);
        GameManager.instance.hoverObject = null;
        switch (mobProfile.type)
        {
            case AI_BurfalbeProfile.BurfType.Heal:
                for (int i = 0; i < GameManager.instance.eggs.Count; i++)
                    if (GameManager.instance.eggs[i].HP > 0)
                        GameManager.instance.eggs[i].Heal(GameManager.instance.eggs[i].maxHP);
                break;

            case AI_BurfalbeProfile.BurfType.Gift:
                break;

            case AI_BurfalbeProfile.BurfType.Aggro:
                break;
        }
    }
    public void drop_random_item()
    {
        Instantiate(GameManager.instance.item, transform.position, Quaternion.identity).GetComponent<EntitySkillStone>().Type = (GameManager.BreathType)UnityEngine.Random.Range(0, 5);
    }
    public override void FixedUpdate()
    {
        base.FixedUpdate();
        if (path == null || DestinationTarget == null)
            return;

        if (currentWaypoint >= path.vectorPath.Count)
        {
            reachedEndOfPath = true;
            return;
        }
        else
        {
            reachedEndOfPath = false;
        }
        Vector2 direction = ((Vector2)path.vectorPath[currentWaypoint] - (realPos)).normalized;
        Vector2 force = direction * mobProfile.moveSpeed * Time.deltaTime;
        m_rigidbody.velocity += force;
        if (IsBurf())
            m_rigidbody.velocity *= 0;

        if (m_rigidbody.velocity.x >= 0.1f)
        {
            if (flipAnimation(true))
            {
                transform.localScale = new Vector3(-originProfile.size.x, originProfile.size.y, originProfile.size.z);
                speeachText.transform.localScale = new Vector3(-speeachTextSize, speeachTextSize, speeachTextSize);
            }
        }
        else if (m_rigidbody.velocity.x <= -0.1f)
        {
            if (flipAnimation(false))
            {
                transform.localScale = originProfile.size;
                speeachText.transform.localScale = new Vector3(speeachTextSize, speeachTextSize, speeachTextSize);
            }
        }
    }
    public void End_Burf()
    {
        if (mobProfile.type == AI_BurfalbeProfile.BurfType.Aggro)
            for (int i = 0; i < GameManager.instance.monsters.Count; i++)
                GameManager.instance.monsters[i].isUpdateDestination = true;
        Destroy(gameObject);
    }

    public bool IsBurf()
    {
        return animInfo.IsName("touch");
    }

    public bool IsDestinationInRange()
    {
        return Vector2.Distance(DestinationTarget.transform.position, (Vector2)transform.position + GetComponent<Collider2D>().offset * transform.localScale.y) <= 0.1f;
    }
}
