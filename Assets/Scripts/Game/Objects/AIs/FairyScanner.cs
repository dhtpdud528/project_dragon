﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FairyScanner : MonoBehaviour
{
    public AI_Burfable body;

    
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (body.isBurfOn && collision.GetComponent<AI_Attackable>() && collision.GetComponent<AI_Attackable>().DestinationTarget != transform)
        {
            collision.GetComponent<AI_Attackable>().isUpdateDestination = false;
            collision.GetComponent<AI_Attackable>().DestinationTarget = transform;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.GetComponent<AI_Attackable>() && collision.GetComponent<AI_Attackable>().DestinationTarget != transform)
        {
            collision.GetComponent<AI_Attackable>().isUpdateDestination = true;
        }
    }

}
