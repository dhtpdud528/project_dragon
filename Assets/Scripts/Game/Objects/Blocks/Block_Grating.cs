﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block_Grating : Object_Static
{
    Collider2D m_collider;
    [SerializeField]
    bool on;
    [HideInInspector]
    public Block_Spawner.Wave linkedWave;

    public bool On
    {
        set
        {
            //if (on == value) return;
            on = value;
            if (!on)
            {
                anim.CrossFadeInFixedTime("hiding", 0);
                m_collider.enabled = false;
                GameManager.instance.AstarScan();
            }
            else
            {
                anim.CrossFadeInFixedTime("showing", 0);
                m_collider.enabled = true;
                GameManager.instance.AstarScan();
            }
        }
        get
        {
            return on;
        }
    }
    protected override void Awake()
    {
        base.Awake();
        m_collider = GetComponentInChildren<Collider2D>();
    }
    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        On = on;
        Debug.Log(linkedWave.name);
    }
    public void TurnSwitch()
    {
        On = !On;
    }
    protected override void OnDisable()
    {
        base.OnDisable();
        if (linkedWave == null) return;
        Debug.Log(linkedWave.name);
        linkedWave.onWaveStart.Remove(linkedWave.onWaveStart.Find((val) => val.target.Equals(gameObject)));
        linkedWave.onWaveEnd.Remove(linkedWave.onWaveStart.Find((val) => val.target.Equals(gameObject)));
    }
}
