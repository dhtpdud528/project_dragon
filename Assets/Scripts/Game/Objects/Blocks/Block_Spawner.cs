﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Events;
using System;
using System.Runtime.Serialization;

[Serializable]
public class EventMessage
{
    //[IgnoreDataMember]
    public GameObject target;
    public int targetID;
    public string message;

    public EventMessage(GameObject target, string message)
    {
        this.target = target;
        this.message = message;
    }
    public EventMessage(int targetID, string message)
    {
        this.targetID = targetID;
        this.message = message;
    }
}
public class Block_Spawner : Object_Static
{
    public enum SpawnState { SPAWNING,WAITING,COUNTING,END}

    [Serializable]
    public class Wave
    {
        public string name;
        public List<EventMessage> onWaveStart = new List<EventMessage>();
        public List<GameObject> spawnTargetsOrigin = new List<GameObject>();
        public int spawnCount;
        public float rate;
        public float timeBetweenWaves = 10;
        public List<EventMessage> onWaveEnd = new List<EventMessage>();

        public Wave(string name, List<EventMessage> onWaveStart, List<GameObject> spawnTargets, int spawnCount, float rate, float timeBetweenWaves, List<EventMessage> onWaveEnd)
        {
            this.name = name;
            if (onWaveStart != null)
                this.onWaveStart = onWaveStart;
            this.spawnTargetsOrigin = spawnTargets;
            this.spawnCount = spawnCount;
            this.rate = rate;
            this.timeBetweenWaves = timeBetweenWaves;
            if (onWaveEnd != null)
                this.onWaveEnd = onWaveEnd;
        }
    }
    public List<Wave> waves = new List<Wave>();
    List<GameObject> currentSpawnTargets = new List<GameObject>();

    int nextWave = 0;
    float waveCountdown;

    public SpawnState state = SpawnState.COUNTING;

    bool init;
    public new void OnDisable()
    {
        GameManager.instance.spawners.Remove(this);
    }
    protected override void Start()
    {
        base.Start();
        if (!GameManager.instance.IsGamePlay)
            state = SpawnState.END;
        //ID초기화가 같은 start에 되어있어 한프레임 뒤로 미룸
        StartCoroutine(EventInit());
    }
    IEnumerator EventInit()
    {
        yield return new WaitForEndOfFrame();
        yield return new WaitUntil(() => waves.Count > 0);
        waveCountdown = waves[0].timeBetweenWaves;
        for (int waveIndex = 0; waveIndex < waves.Count; waveIndex++)
        {
            for (int eventIndex = 0; eventIndex < waves[waveIndex].onWaveStart.Count; eventIndex++)
            {
                if (waves[waveIndex].onWaveStart[eventIndex].target == null)
                    waves[waveIndex].onWaveStart[eventIndex].target = MapDataManager.instance.FindSpawnedBlock(waves[waveIndex].onWaveStart[eventIndex].targetID);
                else
                    waves[waveIndex].onWaveStart[eventIndex].targetID = waves[waveIndex].onWaveStart[eventIndex].target.GetComponent<IName>().ID;
                waves[waveIndex].onWaveStart[eventIndex].target.GetComponent<Block_Grating>().linkedWave = waves[waveIndex];
            }

            for (int eventIndex = 0; eventIndex < waves[waveIndex].onWaveEnd.Count; eventIndex++)
            {
                if (waves[waveIndex].onWaveEnd[eventIndex].target == null)
                    waves[waveIndex].onWaveEnd[eventIndex].target = MapDataManager.instance.FindSpawnedBlock(waves[waveIndex].onWaveEnd[eventIndex].targetID);
                else
                    waves[waveIndex].onWaveEnd[eventIndex].targetID = waves[waveIndex].onWaveEnd[eventIndex].target.GetComponent<IName>().ID;
                waves[waveIndex].onWaveEnd[eventIndex].target.GetComponent<Block_Grating>().linkedWave = waves[waveIndex];
            }


        }
        if (Name.Contains("Spawner"))
            GameManager.instance.spawners.Add(this);
        init = true;
    }
    protected override void Update()
    {
        base.Update();
        if (!init) return;
        if(state == SpawnState.WAITING)
            WaveComplete();
        if (state == SpawnState.END) return;
        if (waveCountdown <= 0)
        {
            if (state != SpawnState.SPAWNING)
                StartCoroutine(SpawnWave(waves[nextWave]));
        }
        else
        {
            waveCountdown -= Time.deltaTime;
            if(!IsCounting() && waveCountdown <= 5)
                anim.CrossFadeInFixedTime("count",0);
        }
    }
    public void LoadWaves(SpawnerInfo.Wave[] loadedWaves)
    {
        this.waves.Clear();
        this.waves = new List<Wave>();
        for (int i = 0; i < loadedWaves.Length; i++)
            this.waves.Add(new Wave(loadedWaves[i].name, loadedWaves[i].onWaveStart, MapDataManager.instance.FindMonsters(loadedWaves[i].spawnTargetNames), loadedWaves[i].spawnCount, (float)loadedWaves[i].rate, (float)loadedWaves[i].timeBetweenWaves, loadedWaves[i].onWaveEnd));
    }
    private void WaveComplete()
    {
        state = SpawnState.COUNTING;


        if (nextWave + 1 > waves.Count - 1)
        {
            state = SpawnState.END;
            GameManager.instance.isAllWavesEnd = GameManager.instance.IsAllWavesEnd();
            return;
        }
        nextWave++;
        waveCountdown = waves[nextWave].timeBetweenWaves;
    }
    

    IEnumerator SpawnWave(Wave wave)
    {
        currentSpawnTargets = new List<GameObject>(wave.spawnTargetsOrigin);
        state = SpawnState.SPAWNING;
        for (int i = 0; i < wave.onWaveStart.Count; i++)
            if (wave.onWaveStart[i].targetID != 0)
            {
                wave.onWaveStart[i].target.SendMessage(wave.onWaveStart[i].message);
            }
        for (int i = 0; i < wave.spawnCount; i++)
        {
            Spawn(currentSpawnTargets);
            yield return new WaitForSeconds(wave.rate);
        }
        state = SpawnState.WAITING;
        for (int i = 0; i < wave.onWaveEnd.Count; i++)
            if (wave.onWaveEnd[i].targetID != 0)
                wave.onWaveEnd[i].target.SendMessage(wave.onWaveEnd[i].message);
        yield break;
    }

    public virtual void Spawn(List<GameObject> targets)
    {
        anim.CrossFadeInFixedTime("spot",0);
        var spawndobj = Instantiate(targets[0], transform.position, Quaternion.identity);
        var temp = targets[0];
        targets.Remove(targets[0]);
        targets.Add(temp);
        GameManager.instance.monsters.Add(spawndobj.GetComponent<AI_Attackable>());
    }
    bool IsCounting()
    {
        return animInfo.IsName("count");
    }
}
