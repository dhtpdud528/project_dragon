﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block_Rainbow : Block_Spawner
{
    public Transform EndRainbow;
    public override void Spawn(List<GameObject> targets)
    {
        //base.Spawn(target);
        anim.CrossFadeInFixedTime("spot", 0);
        AI_Moveable obj = Instantiate(targets[0], transform.position, Quaternion.identity).GetComponent< AI_Moveable>();
        var temp = targets[0];
        targets.Remove(targets[0]);
        targets.Add(temp);
        obj.DestinationTarget = EndRainbow;
    }
}
