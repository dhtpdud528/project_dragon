﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Spine.Unity;
using System;

public class Floor_Trap : Object_Static
{
    
    public TrapProfile profile;
    [Flags]
    public enum TrapType { Hole, Lava, Mud, Water}
    public TrapType type;
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!collision.GetComponent<Object_Deadable>()) return;
        Object_Deadable obj = collision.GetComponent<Object_Deadable>();
        if (obj.HP <= 0) return;
        switch (type)
        {
            case TrapType.Hole:
                if (collision.GetComponent<AI_Attackable>() && collision.GetComponent<AI_Attackable>().IsConfuse())
                {
                    collision.GetComponent<AI_Attackable>().DeadHole();
                    collision.GetComponent<AI_Attackable>().GetComponent<Collider2D>().enabled = false;
                    collision.GetComponent<AI_Attackable>().m_rigidbody.DOMove(transform.position, 1f);
                    StartCoroutine(PullAI(collision.GetComponent<AI_Attackable>()));
                    anim.CrossFadeInFixedTime("drop", 0);
                }
                break;
            case TrapType.Water:
                obj.AddDeburf(GameManager.BreathType.lightning);
                break;
        }
        
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (!collision.GetComponent<Object_Deadable>()) return;
        Object_Deadable obj = collision.GetComponent<Object_Deadable>();
        if (obj.HP <= 0) return;
        switch (type)
        {
            case TrapType.Lava:
                obj.AddDeburf(GameManager.BreathType.fire, (int)profile.deBurfDamage);
                break;
            case TrapType.Mud:
                obj.AddDeburf(GameManager.BreathType.ice);
                break;
        }
    }
    IEnumerator PullAI(AI_Attackable ai)
    {
        while(ai != null && ai.GetComponent<Rigidbody>() != null)
        {
            ai.GetComponent<Rigidbody>().velocity /= 1.01f;
            ai.transform.position = Vector2.Lerp(ai.transform.position, transform.position, Time.deltaTime);
            yield return new WaitForEndOfFrame();
        }
    }
}
