﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Object_Static : MonoBehaviour, IName
{
    public string m_name;
    public string Name
    {
        get
        {
            return m_name;
        }
        set
        {
            m_name = value;
        }
    }
    [SerializeField]
    int id;
    public int ID { 
        get
        {
            return id;
        }
        set 
        {
            id = value;
        }

    }

    public Transform graphic;
    public Animator anim;
    public AnimatorStateInfo animInfo;
    // Start is called before the first frame update
    protected virtual void Awake()
    {
        if (graphic != null && graphic.GetComponent<Animator>())
            anim = graphic.GetComponent<Animator>();

    }
    protected virtual void Start()
    {
        graphic.GetComponent<Renderer>().sortingOrder = Mathf.RoundToInt(transform.position.y * 100f) * -1;
        if (id == 0)
            do
            {
                id = Random.Range(1, int.MaxValue);
            }
            while (GameManager.instance.ids.Contains(id));
        GameManager.instance.ids.Add(id);

        if (!MapDataManager.instance.spawnedBlocks.Contains(gameObject))
            MapDataManager.instance.spawnedBlocks.Add(gameObject);
    }
    protected virtual void OnDisable()
    {
        if (!GetComponent<AI_Moveable>())
            MapDataManager.instance.spawnedBlocks.Remove(gameObject);
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        if (anim != null)
            animInfo = anim.GetCurrentAnimatorStateInfo(0);
    }
}
