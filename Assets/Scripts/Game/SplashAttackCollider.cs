﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashAttackCollider : MonoBehaviour
{
    public int damage;
    public Collider2D m_collider;
    private void Awake()
    {
        m_collider = GetComponent<Collider2D>();
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        Debug.Log(collision.name);
        if (collision.CompareTag("Obstacle"))
        {
            var attackTarget = collision.GetComponent<Object_Deadable>();
            attackTarget.TakeDamage(damage);
        }
        m_collider.enabled = false;
    }
}
