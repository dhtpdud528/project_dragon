﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using DG.Tweening;
using UnityEngine.EventSystems;

[Serializable]
public class DragonBreathProfile : BaseProfile
{
    public double circleMaxSize;
    public double maxBreath;
    public double recoverBreath;
    public double consumptionRate;
    public double exitTime;
    public int breathDamage = 1;
    public double breathChargingTime = 1.2f;
    public double breathPushPower = 40;
    public double itemRecoverValue;
    public int fireTickDamage;
    public int collisionDamage;
}
public class BreathController : MonoBehaviour
{
    public GameManager gm;

    public GameObject breathCircle;
    Animator circleAnim;
    public bool canAttack;

    public GameObject breathCollider;

    [SerializeField]
    private Vector2 circleSize;

    public DragonBreathProfile profile;
    public float curBreath;
    public float needBreath;
    Tween breathTween;

    // Start is called before the first frame update
    void Start()
    {
        gm = GameManager.instance;
        gm.breathController = this;
        //curBreath = (float)profile.maxBreath;
    }

    // Update is called once per frame
    void Update()
    {
        if (gm.hoverObject != null || EventSystem.current.currentSelectedGameObject != null || Input.touchCount > 1)
        { 
            if(canAttack)
                DoFire();
            return; 
        }
        if (Input.GetMouseButtonDown(0)&& !GameManager.IsPointerOverGameObject() && curBreath > 0)
        {
            canAttack = true;
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            circleAnim = Instantiate(breathCircle, transform.position, Quaternion.identity).GetComponent<Animator>();
            circleSize = Vector2.zero;
            circleAnim.transform.position = mousePos;
            circleAnim.GetComponent<Renderer>().enabled = false;
        }
        else if (canAttack)
            if (Input.GetMouseButton(0))
            {
                Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                circleAnim.GetComponent<Renderer>().enabled = true;
                if (!circleAnim.GetCurrentAnimatorStateInfo(0).IsName("meteo_ready"))
                    circleAnim.CrossFadeInFixedTime("meteo_ready", 0);
                transform.position = mousePos;
                circleAnim.transform.position = mousePos;

                if (circleSize.x >= profile.circleMaxSize)
                {
                    circleSize = new Vector2((float)profile.circleMaxSize, (float)profile.circleMaxSize);
                    //Debug.Break();
                }
                else if (needBreath < curBreath)
                {
                    circleSize += new Vector2((float)profile.circleMaxSize, (float)profile.circleMaxSize) * Time.deltaTime * (float)profile.breathChargingTime / 2.5f;
                    needBreath = circleSize.x / (float)profile.circleMaxSize * (float)profile.consumptionRate;
                    circleAnim.speed = (float)profile.breathChargingTime;
                }
                else
                {
                    circleAnim.speed = 0;
                }
            }
            else if (Input.GetMouseButtonUp(0))
            {
                DoFire();
            }
    }
    public void DoFire()
    {
        if (gm.dragonController.recoverCoroutine != null)
        {
            StopCoroutine(gm.dragonController.recoverCoroutine);
            gm.dragonController.recoverCoroutine = null;
        }
        canAttack = false;
        curBreath -= needBreath;
        if (curBreath < 0)
            curBreath = 0;
        if (breathTween != null && breathTween.IsPlaying())
            breathTween.Kill(false);
        else
            breathTween = gm.breathGauge.DOFillAmount(curBreath / (float)profile.maxBreath, 1);
        needBreath = 0;
        circleAnim.speed = 0;
        gm.dragonController.breathProfiles.Enqueue(new BreathProfile(transform.position, circleSize.x, circleAnim));

        //gm.dragonController.MoveAndBreath();


        //GameObject breath = Instantiate(breathParticle, transform.position, Quaternion.identity);
        //DoBreath(CircleSize, circleAnim);
        circleAnim = null;
    }
    public IEnumerator DoRecoverBreath()
    {
        while (gm.dragonController.state == DragonController.DragonState.Sleep)
        {
            if (curBreath < profile.maxBreath)
                curBreath += (float)profile.recoverBreath * Time.deltaTime;
            else
                curBreath = (float)profile.maxBreath;
            gm.breathGauge.fillAmount = curBreath / (float)profile.maxBreath;
            yield return new WaitForEndOfFrame();
        }
    }
    public void DoBreath(BreathProfile profile)
    {
        StartCoroutine(DoBreathCoroutine(profile));
    }
    IEnumerator DoBreathCoroutine(BreathProfile profile)
    {
        yield return new WaitForSeconds(1f);
        string breathName = "normal";
        GameObject[] particles = new GameObject[30];
        float size = profile.size / 100 * 8;
        switch (gm.dragonController.type)
        {
            case GameManager.BreathType.fire:
                breathName = "fire";
                for (int i = 0; i < 30; i++)
                {
                    Vector2 pos1 = (Vector2)profile.position +
                        new Vector2(UnityEngine.Random.Range(-size / 2.5f, size / 2.5f), UnityEngine.Random.Range(-size / 2.5f, size / 2.5f));
                    particles[i] = Instantiate(GameManager.instance.firePlaceParticle[UnityEngine.Random.Range(0, GameManager.instance.firePlaceParticle.Length)], pos1, Quaternion.identity);
                }
                break;
            case GameManager.BreathType.ice:
                breathName = "ice";
                Vector2 pos2 = (Vector2)profile.position;
                particles[0] = Instantiate(GameManager.instance.icePlaceParticle[UnityEngine.Random.Range(0, GameManager.instance.icePlaceParticle.Length)], pos2, Quaternion.identity);
                particles[0].transform.localScale = new Vector2(size, size);
                break;
            case GameManager.BreathType.lightning:
                breathName = "lightning";
                Vector2 pos3 = (Vector2)profile.position;
                particles[0] = Instantiate(GameManager.instance.electricPlaceParticle[UnityEngine.Random.Range(0, GameManager.instance.electricPlaceParticle.Length)], pos3, Quaternion.identity);
                particles[0].transform.localScale = new Vector2(size, size);
                break;
            case GameManager.BreathType.normal: breathName = "normal"; break;
            case GameManager.BreathType.wind:
                breathName = "wind";
                Vector2 pos4 = (Vector2)profile.position;
                particles[0] = Instantiate(GameManager.instance.windPlaceParticle[UnityEngine.Random.Range(0, GameManager.instance.windPlaceParticle.Length)], pos4, Quaternion.identity);
                particles[0].transform.localScale = new Vector2(size, size);
                break;
        }
        GameObject breath = Instantiate(breathCollider, profile.position, Quaternion.identity);
        breath.transform.GetChild(0).GetComponent<SkeletonAnimation>().AnimationState.SetAnimation(0, breathName, false);
        breath.transform.localScale = new Vector2(profile.size / 100, profile.size / 100);
        breath.GetComponent<BreathCollider>().type = gm.dragonController.type;


        yield return new WaitForSeconds(0.1f);
        Destroy(profile.circle.gameObject);
        if (particles[0] != null)
        {
            yield return new WaitForSeconds(10f);
            for (int i = 0; i < particles.Length; i++)
                if (particles[i] != null)
                    Destroy(particles[i]);
        }

    }
}
