﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Spine.Unity;
using System;

public class DragonController : MonoBehaviour
{
    GameManager gm;
    public Image faceImage;
    public Image guageImage;
    public List<Sprite> faceSprites = new List<Sprite>();
    public List<Sprite> guageSprites = new List<Sprite>();
    public GameManager.BreathType type;
    public GameManager.BreathType Type
    {
        get
        {
            return type;
        }
        set
        {
            if (type != value)
            {
                if (value != GameManager.BreathType.normal)
                {
                    if (changedCoroutine != null)
                        StopCoroutine(changedCoroutine);
                    StartCoroutine(changedCoroutine = ChangedCoroutine());
                }
                type = value;
                switch(type)
                {
                    case GameManager.BreathType.fire:
                        faceImage.sprite = faceSprites.Find((val)=> val.name.Contains("fire"));
                        guageImage.sprite = guageSprites.Find((val) => val.name.Contains("fire"));
                        break;
                    case GameManager.BreathType.ice:
                        faceImage.sprite = faceSprites.Find((val) => val.name.Contains("ice"));
                        guageImage.sprite = guageSprites.Find((val) => val.name.Contains("ice"));
                        break;
                    case GameManager.BreathType.lightning:
                        faceImage.sprite = faceSprites.Find((val) => val.name.Contains("light"));
                        guageImage.sprite = guageSprites.Find((val) => val.name.Contains("light"));
                        break;
                    case GameManager.BreathType.normal:
                        faceImage.sprite = faceSprites.Find((val) => val.name.Contains("normal"));
                        guageImage.sprite = guageSprites.Find((val) => val.name.Contains("normal"));
                        break;
                    case GameManager.BreathType.wind:
                        faceImage.sprite = faceSprites.Find((val) => val.name.Contains("wind"));
                        guageImage.sprite = guageSprites.Find((val) => val.name.Contains("wind"));
                        break;
                }
                UpdateFormType();
            }
        }
    }
    public enum DragonState {Attack, Sleep }
    public DragonState state = DragonState.Sleep;
    Animator anim;
    SkeletonAnimator skeletonAnim;
    public Transform breathPosition;
    public GameObject breath;
    public GameObject changeFormParticle;

    public Queue<BreathProfile> breathProfiles = new Queue<BreathProfile>();
    public BreathProfile currentBreath;
    private Vector2 lastPosition;
    private Vector3 sizeOrigin;
    Tween moveTween;
    IEnumerator sleep;

    public float side;
    public float height;
    public IEnumerator recoverCoroutine;
    public IEnumerator changedCoroutine;



    // Start is called before the first frame update
    void Start()
    {
        gm = GameManager.instance;
        sizeOrigin = transform.localScale;
        gm.dragonController = this;
        anim = GetComponent<Animator>();
        skeletonAnim = GetComponent<SkeletonAnimator>();
    }

    // Update is called once per frame
    void Update()
    {
        if(moveTween != null && moveTween.IsPlaying())
        {
            if (((Vector2)transform.position - (currentBreath != null ? currentBreath.position : lastPosition)).x < 0)
                transform.localScale = new Vector3(-sizeOrigin.x, sizeOrigin.y, sizeOrigin.z);
            else
                transform.localScale = sizeOrigin;
        }
        if (Vector2.Distance(lastPosition, (Vector2)transform.position) < 0.01f)
            moveTween = null;

        if (!IsAttacking())
        {
            if (breathProfiles.Count > 0)
            {
                MoveAndBreath();
            }
            else if (moveTween == null && state == DragonState.Attack)
            {
                state = DragonState.Sleep;
                StartCoroutine(sleep = GoSleep());
            }
            
        }
    }
    IEnumerator GoSleep()
    {
        lastPosition = randomExit();
        yield return new WaitForSeconds((float)gm.breathController.profile.exitTime);
        StartCoroutine(recoverCoroutine = gm.breathController.DoRecoverBreath());
        moveTween = transform.DOMove(lastPosition, 4, false);
    }
    public Vector2 randomExit()
    {
        Vector2 pos;
        do
        {
            pos = new Vector2(UnityEngine.Random.Range(-15, 15), UnityEngine.Random.Range(-20, 20));
        }
        while (Mathf.Abs(pos.x) < 11.5 || Mathf.Abs(pos.y) < 17.5);
        return pos;
    }
    public void UpdateFormType()
    {
        PlayTransfromSound();
        skeletonAnim.Skeleton.SetSkin(Type.ToString());

        SkeletonAnimation particle = Instantiate(changeFormParticle, transform.position,Quaternion.identity).GetComponent<SkeletonAnimation>();
        particle.AnimationState.SetAnimation(0, Type.ToString(), false);
        particle.transform.SetParent(transform);

        skeletonAnim.Skeleton.SetSlotsToSetupPose();
        skeletonAnim.LateUpdate();
    }
    public void ResetFormType()
    {
        Type = GameManager.BreathType.normal;
    }
    IEnumerator ChangedCoroutine()
    {
        yield return new WaitForSeconds(10);
        ResetFormType();
    }
    public void MoveAndBreath()
    {
        if (sleep != null)
            StopCoroutine(sleep);
        state = DragonState.Attack;
        moveTween.Kill(false);
        anim.CrossFadeInFixedTime("attack", 0);
        BreathProfile breath = breathProfiles.Dequeue();
        currentBreath = breath;
        lastPosition = breath.position;
        bool right = lastPosition.x < transform.position.x ? true : false;
        Vector2 posForBreath = new Vector2(lastPosition.x + (right ? side : -side), lastPosition.y + height);
        lastPosition = Vector2.Distance(posForBreath,transform.position) < Vector2.Distance(posForBreath, lastPosition) ? (Vector2)transform.position : posForBreath;
        moveTween = transform.DOMove(lastPosition, 1, false);
        gm.breathController.DoBreath(breath);
    }
    public bool IsAttacking()
    {
        return anim.GetCurrentAnimatorStateInfo(0).IsName("attack");
    }
    public void shot()
    {
        PlayBreathSound();
        string breathName = "breath_normal_attack_a";
        switch (Type)
        {
            case GameManager.BreathType.fire: breathName = "breath_fire_attack_a"; break;
            case GameManager.BreathType.ice: breathName = "breath_ice_attack_a"; break;
            case GameManager.BreathType.lightning: breathName = "breath_lightning_attack_a"; break;
            case GameManager.BreathType.normal: breathName = "breath_normal_attack_a"; break;
            case GameManager.BreathType.wind: breathName = "breath_wind_attack_a"; break;
        }
        Transform breathTrans = Instantiate(breath, breathPosition.position, Quaternion.identity).transform;
        breathTrans.GetComponent<Animator>().CrossFadeInFixedTime(breathName, 0);
        breathTrans.localScale = Vector2.zero;
        int xSizeMultiple = transform.localScale.x < 0 ? -1 : 1;
        breathTrans.DOScale(new Vector2(currentBreath.size / (float)gm.breathController.profile.circleMaxSize* xSizeMultiple, currentBreath.size / (float)gm.breathController.profile.circleMaxSize), 0.5f);
        breathTrans.DOMove(currentBreath.position, 0.5f);
        currentBreath = null;
    }
    public void PlayBreathSound()
    {
        SoundManager.instance.PlayOneShot_SFX(SoundManager.instance.breathSound);
    }
    public void PlayTransfromSound()
    {
        SoundManager.instance.PlayOneShot_SFX(SoundManager.instance.transformSound);
    }
}
