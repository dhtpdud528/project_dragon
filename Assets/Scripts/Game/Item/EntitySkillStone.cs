﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Spine.Unity;
using UnityEngine.UI;

public class EntitySkillStone : EntityItem
{
    public SkeletonAnimator skeletonAnim;
    GameManager.BreathType type;
    public GameManager.BreathType Type
    {
        get
        {
            return type;
        }
        set
        {
            type = value;
            if (Type == GameManager.BreathType.normal)
                skeletonAnim.Skeleton.SetSkin("heart");
            else
                skeletonAnim.Skeleton.SetSkin(Type.ToString());
            skeletonAnim.Skeleton.SetSlotsToSetupPose();
            skeletonAnim.LateUpdate();
        }
    }
    public Vector2 slotPosition;
    private bool readyToUse;
    

    public override void Awake()
    {
        base.Awake();
        //Type = (GameManager.BreathType)Random.Range(0, 5);
    }
    public override void Start()
    {
        base.Start();
        transform.SetParent(GameManager.instance.gameCanvas);
    }
    public override void Update()
    {
        base.Update();
    }
    bool IsCanUse()
    {
        return readyToUse && !stateInfo.IsName("use");// && !GameManager.IsPointerOverGameObject();
    }
    public void OnMouseDown()
    {
        if (IsCanUse())
            UseItem();
    }
    private void OnMouseOver()
    {
        if (IsCanUse())
            GameManager.instance.hoverObject = gameObject;
    }
    private void OnMouseExit()
    {
        GameManager.instance.hoverObject = null;
    }
    public override void UseItem()
    {
        if (!IsCanUse()) return;
        base.UseItem();
        PlayUseItemSound();
        anim.CrossFadeInFixedTime("use", 0);
        float recoverVal = (float)GameManager.instance.breathController.profile.itemRecoverValue;

        GameManager.instance.breathController.curBreath =
            recoverVal + GameManager.instance.breathController.curBreath > (float)GameManager.instance.breathController.profile.maxBreath ?
            (float)GameManager.instance.breathController.profile.maxBreath :
            recoverVal + GameManager.instance.breathController.curBreath;

        if (Type != GameManager.BreathType.normal)
            GameManager.instance.dragonController.Type = Type;
    }
    public void MoveToSlot()
    {
        int index = 0;
        switch (Type)
        {
            case GameManager.BreathType.fire:
                index = 0;
                break;
            case GameManager.BreathType.ice:
                index = 1;
                break;
            case GameManager.BreathType.wind:
                index = 2;
                break;
            case GameManager.BreathType.lightning:
                index = 3;
                break;
            default:
                index = 4;
                break;
        }
        if (index == 4)
        {
            for (int i = 0; i < GameManager.instance.eggs.Count; i++)
                if (GameManager.instance.eggs[i].HP > 0)
                    GameManager.instance.eggs[i].Heal(GameManager.instance.eggs[i].maxHP / 2);
            readyToUse = true;
            UseItem();
            return;
        }
        StartCoroutine(MoveToSlotCoroutine(index));

    }
    IEnumerator MoveToSlotCoroutine(int index)
    {
        slotPosition = GameManager.instance.inventory.slotPositions[index].position;
        transform.DOMove(slotPosition, 1);
        if (GameManager.instance.inventory.slots[index] != null)
        {
            GameManager.instance.inventory.slots[index].Num+=1;
            Destroy(gameObject, 1);
        }
        else
        {
            GameManager.instance.inventory.slots[index] = this;
        }
        yield return new WaitForSeconds(1);
        countText.gameObject.SetActive(true);
        readyToUse = true;
    }
    public void PlayUseItemSound()
    {
        SoundManager.instance.PlayOneShot_SFX(SoundManager.instance.useItem);
    }
    public void PlayGetItemSound()
    {
        SoundManager.instance.audioSourceSFX.clip = SoundManager.instance.rewardSound;
        SoundManager.instance.audioSourceSFX.Play();
    }
}
