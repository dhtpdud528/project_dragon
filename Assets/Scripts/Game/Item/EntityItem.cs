﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class EntityItem : MonoBehaviour
{
    public Animator anim;
    public AnimatorStateInfo stateInfo;
    public TextMeshPro countText;
    public int maxNum;
    [SerializeField ]
    int num;
    public int Num 
    {
        get
        {
            return num;
        }
        set 
        {
            num = value;
            if (num > maxNum)
                num = maxNum;
            else if (num <= 0)
            {
                if (GameManager.instance.hoverObject == gameObject)
                    GameManager.instance.hoverObject = null;
                Destroy(gameObject, 0.5f);
            }
            countText.text = num.ToString();
        } 
    }
    public virtual void Awake()
    {
        countText.gameObject.SetActive(false);
        if (maxNum <= 0)
            maxNum = 1;
        Num = 1;
    }
    // Start is called before the first frame update
    public virtual void Start()
    {
        anim = GetComponent<Animator>();
    }
    public void GetItem()
    {
        anim.CrossFadeInFixedTime("get", 0);
    }
    public virtual void Update()
    {
        stateInfo = anim.GetCurrentAnimatorStateInfo(0);
    }
    public virtual void UseItem()
    {
        Num += -1;
    }
}
