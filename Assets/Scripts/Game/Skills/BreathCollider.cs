﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public class BreathCollider : MonoBehaviour
{
    GameManager gm;
    public GameManager.BreathType type;
    public GameObject breathParticle;
    public List<AI_Attackable> damagedAIs = new List<AI_Attackable>();
    float time;
    float maxLifeTime;
    // Start is called before the first frame update
    void Start()
    {
        gm = GameManager.instance;
        PlayExplotionSound();
        if (type != GameManager.BreathType.normal)
            maxLifeTime = 10;
        else
            maxLifeTime = 1;
        Destroy(gameObject, maxLifeTime);
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (time < 1 && collision.GetComponent<Object_Deadable>())
        {
            Object_Deadable obj = collision.GetComponent<Object_Deadable>();

            int damage = (int)(gm.breathController.profile.breathDamage * (transform.localScale.x / ((float)gm.breathController.profile.circleMaxSize / 100)));
            if (obj.GetComponent<AI_Attackable>())
            {
                AI_Attackable monster = (AI_Attackable)obj;
                if (damagedAIs.Contains(monster)) return;
                damagedAIs.Add(monster);

                Vector2 dir = (monster.transform.position - transform.position).normalized;

                if(type == GameManager.BreathType.lightning)
                    monster.TakeDamage(damage/2, dir);
                else
                {
                    monster.TakeDamage(damage, dir);
                    monster.GetComponent<Rigidbody2D>().AddForce(dir * (float)gm.breathController.profile.breathPushPower, ForceMode2D.Impulse);
                }
                
            }
            else
                obj.TakeDamage(damage);
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.GetComponent<Object_Deadable>())
        {
            Object_Deadable obj = collision.GetComponent<Object_Deadable>();
            if (obj.HP <= 0) return;
            if (type == GameManager.BreathType.fire)
                obj.AddDeburf(type, gm.breathController.profile.fireTickDamage);
            else
                obj.AddDeburf(type);
            if (obj.GetComponent<AI_Attackable>() && type == GameManager.BreathType.wind)
            {
                AI_Attackable monster = (AI_Attackable)obj;
                Vector2 dir = (monster.transform.position - transform.position).normalized;
                monster.TakeDamage(0, dir);
                if(monster.GetComponent<Rigidbody2D>().velocity.magnitude < 10)
                    monster.GetComponent<Rigidbody2D>().AddForce(dir * (float)gm.breathController.profile.breathPushPower, ForceMode2D.Impulse);
            }
        }
    }
    public void PlayExplotionSound()
    {
        SoundManager.instance.PlayOneShot_SFX(SoundManager.instance.explosionNormalSound);
    }
}
