﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneTransformRegister : MonoBehaviour
{
    Button button;
    public string targetSceneName;
    // Start is called before the first frame update
    void Start()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(() => MoveToScene());
    }

    public void MoveToScene()
    {
        if (targetSceneName.Equals("LevelDesigner"))
            if (LocalizationManager.instance.language == SystemLanguage.Korean)
                SceneTransformManager.instance.MoveToScene(targetSceneName + "_KR");
            else
                SceneTransformManager.instance.MoveToScene(targetSceneName + "_EN");
        else
            SceneTransformManager.instance.MoveToScene(targetSceneName);
    }
}
