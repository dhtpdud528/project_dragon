﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegisterGratingController : MonoBehaviour
{
    public enum EventType { Start,End}
    //public Block_Spawner target;
    public EventType eventType;
    public GameObject gratingComponent;

    public Transform nonSelectedGratingsTransform;
    public Transform selectedGratingsTransform;
    // Start is called before the first frame update

    public void OnEnable()
    {
        List<EventMessage> waveEvents = null;
        if (eventType == EventType.Start)
            waveEvents = MapDesigner.instance.infoSpawnerTap.SelectedWave.onWaveStart;
        else if (eventType == EventType.End)
            waveEvents = MapDesigner.instance.infoSpawnerTap.SelectedWave.onWaveEnd;

        List<Block_Grating> registedGratingsList = new List<Block_Grating>();

        for (int i = 0; i < waveEvents.Count; i++)
        {
            var targetGrating = MapDataManager.instance.FindSpawnedBlock(waveEvents[i].targetID);
            if (targetGrating != null)
            {
                var component = Instantiate(gratingComponent, selectedGratingsTransform).GetComponent<GratingComponent>();
                component.target = targetGrating;
                registedGratingsList.Add(component.target.GetComponent<Block_Grating>());
            }

        }

        var gratings = FindObjectsOfType<Block_Grating>();
        
        for (int i = 0; i < gratings.Length; i++)
        {
            if (registedGratingsList.Contains(gratings[i])) continue;

            var component = Instantiate(gratingComponent, nonSelectedGratingsTransform).GetComponent<GratingComponent>();
            component.target = gratings[i].gameObject;
        }
    }
    public void OnDisable()
    {
        int tempCount = nonSelectedGratingsTransform.childCount;
        for (int i=0; i< tempCount; i++)
            DestroyImmediate(nonSelectedGratingsTransform.GetChild(0).gameObject);
        tempCount = selectedGratingsTransform.childCount;
        for (int i = 0; i < tempCount; i++)
            DestroyImmediate(selectedGratingsTransform.GetChild(0).gameObject);
    }
    public void SaveGrating()
    {
        List<EventMessage> waveEvents = null;
        if (eventType == EventType.Start)
            waveEvents = MapDesigner.instance.infoSpawnerTap.SelectedWave.onWaveStart;
        else if (eventType == EventType.End)
            waveEvents = MapDesigner.instance.infoSpawnerTap.SelectedWave.onWaveEnd;

        waveEvents.Clear();
        for (int i = 0; i < selectedGratingsTransform.childCount; i++)
            waveEvents.Add(new EventMessage(selectedGratingsTransform.GetChild(i).GetComponent<GratingComponent>().target.GetComponent<IName>().ID, "TurnSwitch"));

        gameObject.SetActive(false);
    }
    public void MoveGrating(bool toSeleted)
    {
        Transform fromList;
        Transform toList;
        if (toSeleted)
        {
            fromList = nonSelectedGratingsTransform;
            toList = selectedGratingsTransform;
        }
        else
        {
            fromList = selectedGratingsTransform;
            toList = nonSelectedGratingsTransform;
        }

        List<Transform> tempComponentTransforms = new List<Transform>();
        for(int i=0; i< fromList.childCount;i++)
        {
            if (fromList.GetChild(i).GetComponent<GratingComponent>().IsSelected)
                tempComponentTransforms.Add(fromList.GetChild(i));
        }
        for (int i = 0; i < tempComponentTransforms.Count; i++)
            tempComponentTransforms[i].SetParent(toList);
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
