﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwitchImageEvent : MonoBehaviour
{
    public Image Target;
    public Sprite Image1;
    public Sprite Image2;
    public void Awake()
    {
        Target.sprite = Image1;
    }
    public void SwitchImage()
    {
        if (Target.sprite == Image1)
            Target.sprite = Image2;
        else
            Target.sprite = Image1;
    }
}
