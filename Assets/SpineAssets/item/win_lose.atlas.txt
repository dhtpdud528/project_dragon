
win_lose.png
size: 1848,237
format: RGBA8888
filter: Linear,Linear
repeat: none
effect_ston_a_fire
  rotate: false
  xy: 1555, 140
  size: 97, 97
  orig: 97, 97
  offset: 0, 0
  index: -1
effect_ston_a_heart
  rotate: false
  xy: 1575, 42
  size: 97, 97
  orig: 97, 97
  offset: 0, 0
  index: -1
effect_ston_a_ice
  rotate: false
  xy: 1653, 140
  size: 97, 97
  orig: 97, 97
  offset: 0, 0
  index: -1
effect_ston_a_light
  rotate: false
  xy: 1751, 140
  size: 97, 97
  orig: 97, 97
  offset: 0, 0
  index: -1
effect_ston_a_wind
  rotate: false
  xy: 1673, 42
  size: 97, 97
  orig: 97, 97
  offset: 0, 0
  index: -1
effect_ston_b_fire
  rotate: false
  xy: 1127, 62
  size: 24, 24
  orig: 26, 26
  offset: 1, 1
  index: -1
effect_ston_c_fire
  rotate: false
  xy: 1127, 62
  size: 24, 24
  orig: 26, 26
  offset: 1, 1
  index: -1
effect_ston_e_fire
  rotate: false
  xy: 1127, 62
  size: 24, 24
  orig: 26, 26
  offset: 1, 1
  index: -1
effect_ston_b_heart
  rotate: false
  xy: 1127, 37
  size: 24, 24
  orig: 26, 26
  offset: 1, 1
  index: -1
effect_ston_c_heart
  rotate: false
  xy: 1127, 37
  size: 24, 24
  orig: 26, 26
  offset: 1, 1
  index: -1
effect_ston_e_heart
  rotate: false
  xy: 1127, 37
  size: 24, 24
  orig: 26, 26
  offset: 1, 1
  index: -1
effect_ston_b_ice
  rotate: false
  xy: 1127, 12
  size: 24, 24
  orig: 26, 26
  offset: 1, 1
  index: -1
effect_ston_c_ice
  rotate: false
  xy: 1127, 12
  size: 24, 24
  orig: 26, 26
  offset: 1, 1
  index: -1
effect_ston_e_ice
  rotate: false
  xy: 1127, 12
  size: 24, 24
  orig: 26, 26
  offset: 1, 1
  index: -1
effect_ston_b_light
  rotate: false
  xy: 1152, 62
  size: 24, 24
  orig: 26, 26
  offset: 1, 1
  index: -1
effect_ston_c_light
  rotate: false
  xy: 1152, 62
  size: 24, 24
  orig: 26, 26
  offset: 1, 1
  index: -1
effect_ston_e_light
  rotate: false
  xy: 1152, 62
  size: 24, 24
  orig: 26, 26
  offset: 1, 1
  index: -1
effect_ston_b_wind
  rotate: false
  xy: 1152, 37
  size: 24, 24
  orig: 26, 26
  offset: 1, 1
  index: -1
effect_ston_c_wind
  rotate: false
  xy: 1152, 37
  size: 24, 24
  orig: 26, 26
  offset: 1, 1
  index: -1
effect_ston_e_wind
  rotate: false
  xy: 1152, 37
  size: 24, 24
  orig: 26, 26
  offset: 1, 1
  index: -1
effect_ston_d_fire
  rotate: false
  xy: 1152, 20
  size: 16, 16
  orig: 18, 18
  offset: 1, 1
  index: -1
effect_ston_f_fire
  rotate: false
  xy: 1152, 20
  size: 16, 16
  orig: 18, 18
  offset: 1, 1
  index: -1
effect_ston_d_heart
  rotate: false
  xy: 1152, 3
  size: 16, 16
  orig: 18, 18
  offset: 1, 1
  index: -1
effect_ston_f_heart
  rotate: false
  xy: 1152, 3
  size: 16, 16
  orig: 18, 18
  offset: 1, 1
  index: -1
effect_ston_d_ice
  rotate: false
  xy: 1169, 20
  size: 16, 16
  orig: 18, 18
  offset: 1, 1
  index: -1
effect_ston_f_ice
  rotate: false
  xy: 1169, 20
  size: 16, 16
  orig: 18, 18
  offset: 1, 1
  index: -1
effect_ston_d_light
  rotate: false
  xy: 1169, 3
  size: 16, 16
  orig: 18, 18
  offset: 1, 1
  index: -1
effect_ston_f_light
  rotate: false
  xy: 1169, 3
  size: 16, 16
  orig: 18, 18
  offset: 1, 1
  index: -1
effect_ston_d_wind
  rotate: false
  xy: 1177, 70
  size: 16, 16
  orig: 18, 18
  offset: 1, 1
  index: -1
effect_ston_f_wind
  rotate: false
  xy: 1177, 70
  size: 16, 16
  orig: 18, 18
  offset: 1, 1
  index: -1
effect_ston_fire
  rotate: false
  xy: 0, 10
  size: 225, 227
  orig: 225, 227
  offset: 0, 0
  index: -1
effect_ston_heart
  rotate: false
  xy: 226, 10
  size: 225, 227
  orig: 225, 227
  offset: 0, 0
  index: -1
effect_ston_ice
  rotate: false
  xy: 452, 12
  size: 224, 225
  orig: 225, 227
  offset: 1, 0
  index: -1
effect_ston_light
  rotate: false
  xy: 677, 12
  size: 224, 225
  orig: 225, 227
  offset: 1, 0
  index: -1
effect_ston_wind
  rotate: false
  xy: 902, 12
  size: 224, 225
  orig: 225, 227
  offset: 1, 0
  index: -1
ston_fire
  rotate: true
  xy: 1274, 0
  size: 99, 150
  orig: 101, 152
  offset: 1, 1
  index: -1
ston_heart
  rotate: true
  xy: 1274, 100
  size: 137, 128
  orig: 149, 152
  offset: 6, 8
  index: -1
ston_ice
  rotate: true
  xy: 1425, 31
  size: 103, 149
  orig: 105, 151
  offset: 1, 1
  index: -1
ston_light
  rotate: true
  xy: 1403, 135
  size: 102, 151
  orig: 104, 153
  offset: 1, 1
  index: -1
ston_wind
  rotate: true
  xy: 1127, 87
  size: 150, 146
  orig: 150, 146
  offset: 0, 0
  index: -1
