
unicon.png
size: 130,262
format: RGBA8888
filter: Linear,Linear
repeat: none
grating_a
  rotate: false
  xy: 0, 0
  size: 99, 45
  orig: 134, 45
  offset: 18, 0
  index: -1
grating_b
  rotate: false
  xy: 0, 46
  size: 99, 216
  orig: 134, 253
  offset: 18, 18
  index: -1
grating_c
  rotate: true
  xy: 100, 58
  size: 204, 30
  orig: 204, 30
  offset: 0, 0
  index: -1
