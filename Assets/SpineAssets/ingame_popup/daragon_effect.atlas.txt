
daragon_effect.png
size: 874,389
format: RGBA8888
filter: Linear,Linear
repeat: none
board_forest
  rotate: false
  xy: 441, 4
  size: 433, 385
  orig: 435, 387
  offset: 1, 1
  index: -1
board_ice
  rotate: false
  xy: 0, 0
  size: 440, 389
  orig: 442, 391
  offset: 1, 1
  index: -1
