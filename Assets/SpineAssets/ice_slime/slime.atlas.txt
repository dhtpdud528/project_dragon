
slime.png
size: 947,182
format: RGBA8888
filter: Linear,Linear
repeat: none
attack_efect_a
  rotate: false
  xy: 675, 2
  size: 148, 180
  orig: 150, 182
  offset: 1, 1
  index: -1
attack_efect_b
  rotate: true
  xy: 476, 0
  size: 182, 198
  orig: 184, 200
  offset: 1, 1
  index: -1
base_a_ice
  rotate: false
  xy: 0, 3
  size: 246, 179
  orig: 248, 181
  offset: 1, 1
  index: -1
base_b_ice
  rotate: false
  xy: 247, 17
  size: 228, 165
  orig: 230, 167
  offset: 1, 1
  index: -1
kori_ice
  rotate: false
  xy: 843, 112
  size: 104, 70
  orig: 106, 72
  offset: 1, 1
  index: -1
left_eye_ice
  rotate: true
  xy: 886, 12
  size: 18, 42
  orig: 20, 44
  offset: 1, 1
  index: -1
mouse_ice
  rotate: false
  xy: 247, 4
  size: 31, 12
  orig: 33, 14
  offset: 1, 1
  index: -1
right_eye_ice
  rotate: true
  xy: 843, 0
  size: 111, 42
  orig: 113, 44
  offset: 1, 1
  index: -1
slime_style_a_ice
  rotate: true
  xy: 824, 6
  size: 176, 18
  orig: 181, 19
  offset: 3, 1
  index: -1
slime_style_b_ice
  rotate: true
  xy: 886, 31
  size: 80, 45
  orig: 80, 45
  offset: 0, 0
  index: -1
