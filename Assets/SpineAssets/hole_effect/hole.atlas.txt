
hole.png
size: 1814,361
format: RGBA8888
filter: Linear,Linear
repeat: none
effect_hole_fire
  rotate: false
  xy: 121, 124
  size: 118, 237
  orig: 118, 266
  offset: 0, 0
  index: -1
effect_hole_heart
  rotate: false
  xy: 0, 118
  size: 120, 243
  orig: 120, 243
  offset: 0, 0
  index: -1
effect_hole_ice
  rotate: false
  xy: 240, 124
  size: 118, 237
  orig: 118, 266
  offset: 0, 0
  index: -1
effect_hole_light
  rotate: true
  xy: 301, 5
  size: 118, 237
  orig: 118, 266
  offset: 0, 0
  index: -1
effect_hole_wind
  rotate: false
  xy: 359, 124
  size: 118, 237
  orig: 118, 266
  offset: 0, 0
  index: -1
hole_a
  rotate: true
  xy: 1677, 78
  size: 135, 137
  orig: 137, 139
  offset: 1, 1
  index: -1
hole_b
  rotate: true
  xy: 1565, 214
  size: 147, 135
  orig: 149, 137
  offset: 1, 1
  index: -1
hole_c
  rotate: true
  xy: 821, 22
  size: 154, 129
  orig: 156, 131
  offset: 1, 1
  index: -1
hole_d
  rotate: true
  xy: 539, 3
  size: 170, 133
  orig: 172, 135
  offset: 1, 1
  index: -1
hole_e
  rotate: true
  xy: 1247, 0
  size: 179, 133
  orig: 181, 135
  offset: 1, 1
  index: -1
hole_f
  rotate: true
  xy: 1145, 180
  size: 181, 135
  orig: 183, 137
  offset: 1, 1
  index: -1
hole_fire
  rotate: true
  xy: 951, 29
  size: 148, 147
  orig: 150, 149
  offset: 1, 1
  index: -1
hole_g
  rotate: true
  xy: 1281, 180
  size: 181, 135
  orig: 183, 137
  offset: 1, 1
  index: -1
hole_h
  rotate: true
  xy: 1011, 179
  size: 182, 133
  orig: 184, 135
  offset: 1, 1
  index: -1
hole_heart
  rotate: true
  xy: 673, 20
  size: 156, 147
  orig: 159, 158
  offset: 2, 4
  index: -1
hole_i
  rotate: true
  xy: 875, 178
  size: 183, 135
  orig: 185, 137
  offset: 1, 1
  index: -1
hole_ice
  rotate: true
  xy: 1099, 30
  size: 148, 147
  orig: 150, 149
  offset: 1, 1
  index: -1
hole_j
  rotate: true
  xy: 478, 174
  size: 187, 132
  orig: 189, 134
  offset: 1, 1
  index: -1
hole_k
  rotate: true
  xy: 744, 177
  size: 184, 130
  orig: 186, 132
  offset: 1, 1
  index: -1
hole_l
  rotate: true
  xy: 611, 177
  size: 184, 132
  orig: 186, 134
  offset: 1, 1
  index: -1
hole_light
  rotate: true
  xy: 1381, 31
  size: 148, 147
  orig: 150, 149
  offset: 1, 1
  index: -1
hole_m
  rotate: false
  xy: 135, 3
  size: 165, 120
  orig: 167, 122
  offset: 1, 1
  index: -1
hole_n
  rotate: false
  xy: 0, 0
  size: 134, 117
  orig: 136, 119
  offset: 1, 1
  index: -1
hole_normal
  rotate: true
  xy: 1417, 213
  size: 148, 147
  orig: 150, 149
  offset: 1, 1
  index: -1
hole_o
  rotate: false
  xy: 1677, 3
  size: 127, 74
  orig: 129, 76
  offset: 1, 1
  index: -1
hole_p
  rotate: false
  xy: 1529, 0
  size: 118, 63
  orig: 120, 65
  offset: 1, 1
  index: -1
hole_q
  rotate: false
  xy: 1701, 310
  size: 104, 51
  orig: 106, 53
  offset: 1, 1
  index: -1
hole_wind
  rotate: true
  xy: 1529, 64
  size: 148, 147
  orig: 150, 149
  offset: 1, 1
  index: -1
