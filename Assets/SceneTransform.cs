﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTransform : MonoBehaviour
{
    public void MoveToScene(string sceneName)
    {
        SceneTransformManager.instance.loadingOBJ.SetActive(true);
        SceneManager.LoadScene(sceneName);
    }
}
